<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TutorTest extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    //Relationships
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function proficiency_exam(){
        return $this->belongsTo('App\ProficiencyExam');
    }

    public function questions(){
        return $this->belongsToMany('App\Question')
        			->withTimestamps()
                    ->withPivot('deleted_at');
    }

    public function tutor_candidates(){
        return $this->hasMany('App\TutorCandidate');
    }

    //Method
    public function view_solutions(){
        //allow student to view solutions if tutor has paid for test
        return $this->tutor_candidates->filter(function($candidate) {
                            return $candidate->tutor_test_payments->where('paid', 1)->where('refunded',0)->count();})
                    ->count() > 0 ? true : false;
    }
}
