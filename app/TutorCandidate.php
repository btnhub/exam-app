<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TutorCandidate extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at', 'deadline', 'submission_date'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    //Relationships
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function tutor_test_payments(){
        return $this->hasMany('App\TutorTestPayment');
    }

    //Relationships
    public function tutor_test(){
        return $this->belongsTo('App\TutorTest');
    }

    public function questions(){
        return $this->belongsToMany('App\Question')
                    ->withTimestamps()
                    ->withPivot('selected_answer_id', 'question_time', 'deleted_at');
    }

    //Methods
    public function no_correct(){
        $no_correct = 0;

        foreach ($this->questions as $question) {
            if ($question->correct_answer()->id == $question->pivot->selected_answer_id) {
                $no_correct++;
            }
        }

        return $no_correct;
    }
    
    public function grade(){
        $no_questions = $this->questions->count(); //# of questions in test

        return number_format($this->no_correct() / $no_questions, 2) * 100;
    }

    public function paid(){
        if ($this->tutor_test_payments->count()) {
            return $this->tutor_test_payments->where('paid', 1)->where('refunded', 0)->count() ? true: false;
        }
        
        return false;
    }

    public function grant_access(){
        //allow tutor access if deadline hasn't passed and test has been paid for
        if ($this->deadline->isFuture() && $this->paid()) {
            
            //If deadline hasn't passed and the payment wasn't refunded
            return true;
         
        }

        return false;
    }
}
