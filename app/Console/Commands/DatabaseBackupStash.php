<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatabaseBackupStash extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stash:db-backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup Of exam_stash Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');
        $dbname = config('database.connections.mysql.database'); 
    
        //$destination = "db_backups";//test
        $destination = "examstash.com/db_backups"; 
        
        $filename = $dbname."-".date("Ymd").".sql";

        exec("mysqldump -u $username -p$password $dbname > $destination/$filename");  
    }
}
