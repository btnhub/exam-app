<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    //Relationship
    public function course(){
        return $this->belongsTo('App\Course');
    }

    public function questions(){
        return $this->belongsToMany('App\Question');
    }

    public function packages(){
        return $this->belongsToMany('App\Package')
                    ->withPivot('order');
    }
}
