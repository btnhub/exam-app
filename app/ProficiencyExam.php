<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Exam;
use App\ProficiencyExam;

class ProficiencyExam extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    public $min_questions = 20; //min number of questions in a proficiency exam

    //Relationships
    public function courses(){
        return $this->belongsToMany('App\Course')
                    ->withPivot('specific_exam_number');
    }


    //Methods
    public function scopeActive($query)
    {   
        return $query->where('available',1)
                        ->whereHas('courses', function($course){
                            $course->whereHas('exams', function($exam){
                                        $exam->where('reviewed', 1);
                                    });
                        });
    }

    public function questions(){
        return Question::whereHas('exam', function($q_exam){
                $q_exam->where('reviewed', 1)
                        ->whereHas('course', function($q_course){
                               $q_course->whereIn('id', $this->courses->pluck('id'));
                        });
                })
                ->get();
    }

    /**
     * Available Prof Exams = Active Prof Exams with enough questions (as defined by model)
     * @return [type] [description]
     */
    public function available_exams(){
        return ProficiencyExam::active()
                    ->get()
                    ->filter(function($query){
                        return $query->questions()->count() >= (new ProficiencyExam)->min_questions;
                    });
    }
}
