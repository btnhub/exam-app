<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    //Relationships
    public function course(){
        return $this->belongsTo('App\Course');
    }
    
    public function semester(){
        return $this->belongsTo('App\Semester');
    }

    public function questions(){
        return $this->hasMany('App\Question');
    } 

    public function details(){
        return "{$this->course->department->name} - {$this->course->course_name} ({$this->course->course_number}) - Exam #$this->exam_number ".ucfirst($this->semester->semester). " $this->year ";
    }
}
