<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GeneralEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $subject;
    public $email_view;
    public $email_data; //array

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $email_view, $email_data)
    {
        $this->subject = $subject;
        $this->email_view = $email_view;
        $this->email_data = $email_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->email_view)
                    ->subject($this->subject)
                    ->with($this->email_data);
    }
}
