<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\TutorCandidate;
use App\TutorTest;

class EmailTutorCandidate extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $tutor_candidate;
    public $tutor_test;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TutorCandidate $tutor_candidate, TutorTest $tutor_test)
    {
        $this->tutor_candidate = $tutor_candidate;
        $this->tutor_test = $tutor_test;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('btn.emails.notify_tutor_candidates')
                    ->subject("{$this->tutor_test->proficiency_exam->name} Proficiency Analysis Requested By {$this->tutor_test->user->first_name}");
    }
}
