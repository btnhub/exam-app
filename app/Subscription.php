<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;
use App\Role;
use App\Package;
use App\Payment;

class Subscription extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];
    //Relationships
    public function users(){
        return $this->belongsToMany('App\User')
                    ->withTimestamps()
                    ->withPivot('package_id', 'payment_id', 'price', 'start_date', 'end_date', 'free', 'notes', 'deleted_at');
    }

    //Methods
    public function active(){
        //Active subscriptions/packages
        if ($this->pivot->start_date <= Carbon\Carbon::now() 
            && $this->pivot->end_date >= Carbon\Carbon::now()
            && $this->pivot->deleted_at == null
            && $this->tutor_test == 0) {
            return true;
        }
        
        return false;
    }

    public function package(){
        $package = Package::find($this->pivot->package_id);

        return $package;
    }

    public function payment(){
        $payment = Payment::find($this->pivot->payment_id);

        return $payment;
    }
}
