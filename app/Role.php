<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Role;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Relationships
     */
    
    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function subscriber_roles()
    {
    	return Role::where('role', '!=', 'banned')->pluck('role', 'id')->toArray();
    }
}
