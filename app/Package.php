<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;
use App\Role;

class Package extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    //Relationships
    public function topics(){
        return $this->belongsToMany('App\Topic')
                    ->withPivot('order')
                    ->orderBy('order');
    }


    public function scopeActive($query)
    {   
        return $query->where('available',1);
        
    }

    public function active_topics(){
        //Topics with questions
        return $this->topics->filter(function($topic){
                        return $topic->questions->count() > 0;
                    });
    }

    public function questions(){
        $topic_ids = $this->active_topics()->pluck('id')->toArray();    

        $questions = Question::whereHas('topics', function($query) use ($topic_ids)
                {
                    $query->whereIn('topic_id', $topic_ids);
                })
                ->get();

        return $questions;
    }
}
