<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function($user, $ability)
            {
                if($user->hasRole('admin'))
                    { 
                        return true;
                    }
            });

        Gate::define('admin-only', function($user)
        {
            if ($user->hasRole('admin') && !$user->hasRole('banned')) {
                return true;
            }
            
            \Session::flash('error', "You are not authorized to view this page");
            return redirect('/');
        });

        /**
         * User can access own data in the Users Table
         */
        Gate::define('self-access', function($user, $object)
        {
            if ($user->hasRole('banned')) {
                \Session::flash('error', "You are not authorized to view this page");
                return redirect('/');
            }

            return $user->id == $object->id;
        });
        
        Gate::define('access-package', function ($user, $package) {

            return $user->active_subscriptions()->pluck('package_id')->contains($package->id);
            
        });
    }
}
