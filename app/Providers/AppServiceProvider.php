<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //View Composer to load values every time the pricing table layout is loaded
        view()->composer('layouts.education_theme.partials.pricing_table', function($view){
                $view->with('subscriptions', \App\Subscription::where('active',1)->where('tutor_test', 0)->orderBy('duration')->get());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
        $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
    }
}
