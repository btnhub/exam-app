<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Question;
use App\Course;

class Course extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    //Relationships
    public function school(){
        return $this->belongsTo('App\School');
    }

    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function exams(){
        return $this->hasMany('App\Exam');
    }
    
    public function topics(){
        return $this->hasMany('App\Topic');
    }

    public function proficiency_exams(){
        return $this->belongsToMany('App\ProficiencyExam')
                    ->withPivot('specific_exam_number');
    }

    //Methods
    public function questions(){
        $questions = collect([]);
        foreach ($this->exams as $exam) {
            foreach ($exam->questions as $q) {
                $questions->push($q);
            }
        }

        return $questions;
    }

    public function courses_array(){
        $courses_collection = Course::orderBy('department_id')->orderBy('course_name')->get();
        $courses_array[''] = "Select A Course";
        
        foreach ($courses_collection as $course) {
            $courses_array[$course->id] = "$course->course_number - $course->course_name";
        }

        return $courses_array;
    }

    public function similar_course_topics(){
        $similar_courses = Course::where('course_name', 'like',$this->course_name)
                                   ->where('id', '!=',$this->id)
                                   ->get();
        
        $similar_course_topics = collect();
        foreach ($similar_courses as $similar_course) {
            $similar_course_topics->push($similar_course->topics);
        }
        
        return $similar_course_topics->flatten();
    }
}
