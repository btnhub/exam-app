<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at', 'last_login_date'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref_id', 'first_name', 'last_name', 'email', 'phone', 'school', 'city', 'state', 'zip', 'country', 'password',
    ];

    /**
     * Accessor to create full name of user
     * @return string 
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Relationships
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function customer(){
        return $this->hasOne('App\Customer');
    }

    public function payments(){
        return $this->hasMany('App\Payment');
    }

    public function assessments(){
        return $this->hasMany('App\Assessment');
    }

    public function tutor_tests(){
        return $this->hasMany('App\TutorTest');
    }

    public function tutor_candidates(){
        return $this->hasMany('App\TutorCandidate');
    }

    public function questions(){
        //For Staff, questions submitted by user when uploading exams
        return $this->hasMany('App\Question', 'submitted_by');
    }

    public function exams(){
        //For Staff, questions submitted by user when uploading exams
        return $this->hasMany('App\Exam', 'submitted_by');
    }

    //Methods
    public function exams_uploaded(){
        //For Staff
        $exams = $this->exams;
        foreach ($this->questions as $question) {
              if ($question->exam) {
                  $exams->push($question->exam);
              }
          }  

        return $exams->unique();
    }

    public function courses_uploaded(){
        //For Staff-- NOT USED!
        $courses = collect([]);

        foreach ($this->exams as $exam) {
              $courses->push($exam->course);
          }  

        foreach ($this->questions as $question) {
              $courses->push($question->exam->course);
          }  

          return $courses->unique();
    }

    public function subscriptions(){
        return $this->belongsToMany('App\Subscription')
                    ->withTimestamps()
                    ->withPivot('package_id', 'payment_id', 'price', 'start_date', 'end_date', 'free', 'notes', 'deleted_at');
    }

    //Methods
    public function hasRole($roles) 
    { 
        if (is_array($roles)) {
            foreach ($roles as $role) 
            {
                if ($this->roles->contains('role', $role))
                    return true;
            }
        }
        
        return $this->roles->contains('role', $roles);
    }

    public function isAdmin()
    {
        if ($this->hasRole(['banned'])) 
        {
            return false;
        }

        return $this->hasRole(['super-user', 'admin']);
    }

    public function isStaff()
    {
        if ($this->hasRole(['banned'])) 
        {
            return false;
        }

        return $this->hasRole(['staff', 'admin', 'super-user']);
    }

    public function addRole($role) 
    { 
        if (!$this->hasRole($role)) {
            return $this->roles()->save(Role::whereRole($role)->firstOrFail());     
        }  
    }

    public function removeRole($role) 
    { 
        if ($this->hasRole($role)) {
            $role_id = Role::whereRole($role)->firstOrFail()->id;
            return $this->roles()->detach($role_id);     
        }  
    }

    public function active_subscriptions(){
        //***USED IN GATE IN AuthServiceProvider
        //BUILDER
        return $this->subscriptions()
                    ->where('start_date', '<=', Carbon\Carbon::now())
                    ->where('end_date', '>', Carbon\Carbon::now())
                    ->wherePivot('deleted_at', null);
    }
}
