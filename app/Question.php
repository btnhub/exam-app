<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Answer;

class Question extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    //Relationships
    public function exam(){
        return $this->belongsTo('App\Exam');
    }

    public function submitted_by(){
        return $this->belongsTo('App\User', 'submitted_by');   
    }

    public function answers(){
        return $this->hasMany('App\Answer');
    }

    public function correct_answer(){
        return $this->answers->where('correct_answer', 1)->first();
    }

    public function topics(){
        return $this->belongsToMany('App\Topic');
    }

    public function assessments(){
        return $this->belongsToMany('App\Assessment')
                    ->withTimestamps()
                    ->withPivot('selected_answer_id');
    }

    public function tutor_tests(){
        return $this->belongsToMany('App\TutorTest')
                    ->withTimestamps()
                    ->withPivot('deleted_at');
    }
}
