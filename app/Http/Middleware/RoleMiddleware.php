<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        $roles = explode("|", $roles);

        if (($request->user()->hasRole($roles) && !$request->user()->hasRole('banned')) || $request->user()->hasRole('admin'))
        {
           return $next($request);
        }

        \Session::flash('error', 'You are not authorized to view the selected page');
        return redirect()->back();
    }
}
