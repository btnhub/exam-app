<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Mail\GeneralEmail;

use App\User;
use App\Package;
use App\Subscription;
use App\Payment;

use Carbon;
use Cart;

class SubscriptionsController extends Controller
{
    public function selectPackages()
    {
        $packages = Package::has('topics')->active()->get();
        $subscriptions = Subscription::where('active',1)->where('tutor_test', 0)->get();

        $departments = $packages->pluck('department')->unique();
        
        return view('btn.subscriptions.select_packages', compact('packages', 'departments', 'subscriptions'));

    }

    public function reviewSubscriptions()
    {
        return view('btn.subscriptions.purchase_subscription');
    }

    public function purchaseSubscriptions(Request $request)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $user = auth()->user();
        $payment_transaction_id = rand(10000000, 99999999);

        $cart_items = Cart::content();
        $cart_total = $cart_subtotal = Cart::subtotal();

        $payment = Payment::create([
                        "ref_id" => strtoupper(str_random(12)),
                        "user_id" => $user->id,
                        "transaction_id" => $payment_transaction_id,
                        "details" => serialize($cart_items),
                        "amount" => $cart_subtotal,
                        "total_amount" => $cart_total,
                        "captured" => 0,
                        "paid" => 0,
                    ]);

        try {
            $amount_stripe = number_format($cart_total, 2) * 100; //amount in cents
            $charge = null;
            
            $token = $_POST['stripeToken'] ?? null;

            $description = "ExamStash Subscription ($payment->ref_id)";    

            $charge = \Stripe\Charge::create(array(
                        "amount" => $amount_stripe, // amount in cents
                        "currency" => "usd",
                        "source" => $token,
                        "description" => $description,
                        /*"receipt_email" => $user->email,*/
                    ));                     

            if ($charge) {
                $payment->transaction_id = $charge->id;
                //$payment->stripe_customer_id = $customer_id ?? null;
                $payment->captured = $charge->captured;
                $payment->paid = $charge->paid;
                $payment->completed_at = date("Y-m-d H:i:s", $charge->created); 
                $payment->save();   

                //CREATE SUBSCRIPTION
                foreach ($cart_items as $cart_item) {
                    $subscription = Subscription::find($cart_item->id);
                    $package = Package::where('ref_id', $cart_item->options->package_ref)->first();

                    $user->subscriptions()->save($subscription, [
                                            'package_id' => $package->id,
                                            'payment_id' => $payment->id,
                                            'price' => $cart_item->price,
                                            'start_date' => Carbon\Carbon::now(),
                                            'end_date' => Carbon\Carbon::now()->addWeeks($subscription->duration)
                                        ]);
                }

                //Update User Role
                $user->addRole('subscriber');

                //Email Receipt
                
                $purchase_details = array();

                foreach ($cart_items as $cart_item) {
                    array_push($purchase_details, "{$cart_item->options->package_name} ({$cart_item->name})");
                }

                $subject = "Subscription Receipt";
                $email_description = implode(', ', $purchase_details);

                $email_view = 'btn.emails.email_receipt';
                $email_data = ['user' => $user,
                                'ref_id' => $payment->ref_id,
                                'description' => $email_description,
                                'amount' => $cart_total
                                ];

                Mail::to($user)
                        ->bcc(config('mail.from.address'))
                        ->send(new GeneralEmail($subject, $email_view, $email_data));
            }
            
            Cart::destroy();

            \Session::flash('success', 'Subscription Created, click "Start Exam" to begin!');

            return redirect('/home');         

        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return view('btn.subscriptions.payment_failed')->with('error', $error);

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return view('btn.subscriptions.payment_failed')->with('error', $error);

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return view('btn.subscriptions.payment_failed')->with('error', $error);

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return view('btn.subscriptions.payment_failed')->with('error', $error);
        }
    }

    public function index(){
    	$subscriptions = Subscription::all();

    	$free_users_collection = User::whereHas('roles', function($role){
    									$role->where('role', 'free')
    										 ->orWhere('role', 'mogi-student');
    								})->get();
    	
    	$free_users[''] = "Select User";
    	foreach ($free_users_collection as $free_user) {
    		$free_users[$free_user->id] = $free_user->full_name;
    	}

    	$subscribers = User::whereHas('roles', function($role){
    									$role->where('role', 'subscriber');
    								})
    						->orWhereHas('subscriptions')	
    						->get();

 		$packages[''] = "Select Package";
    	$packages += Package::orderBy('title')->pluck('title', 'id')->toArray();
    	
    	return view('btn.subscriptions.index', compact('subscriptions', 'free_users', 'subscribers', 'packages'));
    }

    public function createFreeSubscription(Request $request, $subscription_id){
    	//dd("free.sub");

    	$user = User::find($request->user_id);
    	$package = Package::find($request->package_id);
    	$subscription = Subscription::find($subscription_id);

    	$start_date = $request->start_date ?? Carbon\Carbon::now();

    	$end_date = Carbon\Carbon::parse($start_date)->addWeeks($subscription->duration);

    	$user->subscriptions()->save($subscription, [
    						'package_id' => $package->id,
    						'start_date' => $start_date,
    						'end_date' => $end_date,
    						'free' => 1]);

    	\Session::flash('success', 'Free Subscription Created');

    	return redirect()->back();
    }

    public function deleteSubscription(Package $package, $user_id){

    }
}
