<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Mail\EmailTutorCandidate;
use App\Mail\EmailTutorTestResults;
use App\Mail\GeneralEmail;

use App\ProficiencyExam;
use App\TutorTest;
use App\TutorCandidate;
use App\TutorTestPayment;
use App\Question;
use App\Answer;
use App\Subscription;

use Carbon;

class TutorTestsController extends Controller
{
    public function createTutorTest(Request $request){
    	$user = auth()->user();

    	$proficiency_exams = ProficiencyExam::whereIn('id', $request->proficiency_exam_ids)->get();

    	foreach ($proficiency_exams as $proficiency_exam) {
    		//Create Tutor Test
    		$questions = $proficiency_exam->questions()->shuffle()->take($request->question_count);	
    		
    		$tutor_test = TutorTest::create([
    					'ref_id' => strtoupper(str_random(12)),
    					'user_id' => $user->id,
    					'proficiency_exam_id' => $proficiency_exam->id
    			]);
    		$tutor_test->questions()->attach($questions->pluck('id')->toArray());
    	}
    	
    	\Session::flash('success', 'Exam(s) Created.  Send Them Out!');

    	return redirect()->back();
    }

    public function viewTutorTest($tutor_test_ref_id){
    	$tutor_test = TutorTest::where('ref_id', $tutor_test_ref_id)->first();

    	if (auth()->user()->id != $tutor_test->user_id && !auth()->user()->isAdmin()) {
    		\Session::flash('error', 'You are not authorized to view this page');
    		return redirect()->route('home');
    	}

    	return view('btn.proficiency_exams.view_tutor_test', compact('tutor_test'));
    }

    public function selectTutors($tutor_test_ref_id){
    	//Create Tutor Tests and e-mail tutors
    	$tutor_test = TutorTest::where('ref_id', $tutor_test_ref_id)->first();

    	return view('btn.proficiency_exams.select_tutors', compact('tutor_test'));
    }

    public function emailTutorTest(Request $request, $tutor_test_ref_id){
        $this->validate($request, [
    						'deadline' => 'after:today',
    						/*'first_name.*' => 'required_with:email.*',
    						'email.*' => 'required_with:first_name.*'*/
    						]);
        
    	$tutor_test = TutorTest::where('ref_id', $tutor_test_ref_id)->first();

    	$email_count = 0;

        $deadline = Carbon\Carbon::parse($request->deadline)->addHours(23)->addMinutes(59)->addSeconds(59);

    	foreach ($request->email as $key => $email) {
			if ($request->email[$key] && $request->first_name[$key]) {
                $tutor_candidate = TutorCandidate::create([
                                    'ref_id' => strtoupper(str_random(12)),
                                    'tutor_test_id' => $tutor_test->id,
                                    'first_name' => $request->first_name[$key],
                                    'last_name' => $request->last_name[$key],
                                    'email' => $request->email[$key],
                                    'deadline' => $deadline,
                                    'grade_cutoff' => $request->grade_cutoff
                                    ]);
            
                //E-mail Tutors
                Mail::to($tutor_candidate)
                        ->send(new EmailTutorCandidate($tutor_candidate, $tutor_test));

                $email_count++;
            }
    	}    	

    	\Session::flash('success', "Email Sent To Each Candidate ($email_count)!  Inform each tutor that the e-mail was sent and may be in the Junk/Spam folder.");

    	return redirect()->route('home');
    }

    public function resendTutorEmail(Request $request, $tutor_test_ref_id, $tutor_candidate_ref_id){
        $tutor_test = TutorTest::where('ref_id', $tutor_test_ref_id)->first();
        $tutor_candidate = TutorCandidate::where('ref_id', $tutor_candidate_ref_id)->first();

        Mail::to($tutor_candidate)
                    ->send(new EmailTutorCandidate($tutor_candidate, $tutor_test));    

        \Session::flash('success', "E-mail Resent To $tutor_candidate->full_name.  Remind $tutor_candidate->first_name that the e-mail may be in the Junk/Spam folder.");

        return redirect()->route('home');
    }


    public function purchaseTutorTest(Request $request, $tutor_test_ref_id, $tutor_candidate_ref_id){
    	$user = auth()->user();
        $tutor_test = TutorTest::where('ref_id', $tutor_test_ref_id)->first();
        $tutor_candidate = TutorCandidate::where('ref_id', $tutor_candidate_ref_id)->first();

        $tutor_test_price = number_format(Subscription::where('tutor_test', 1)->max('price'),0);

    	if (!$tutor_candidate) {

    		\Session::flash('error', 'Could Not Find Your Information.  Please make sure your ExamStash e-mail address matches the e-mail account you received the link to the exam.');

    		return redirect()->route('home');
    	}

    	elseif($tutor_candidate->grant_access()){
    		//If already paid for and deadline hasn't passed
    		\Session::flash('warning', 'You already have access to this test, do not submit another payment.');

    		return redirect()->back();
    	}
        
        //Submit Payment
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $payment_transaction_id = rand(10000000, 99999999);

        $payment = TutorTestPayment::create([
                        "ref_id" => strtoupper(str_random(12)),
                        "tutor_candidate_id" => $tutor_candidate->id,
                        "transaction_id" => $payment_transaction_id,
                        "amount" => $tutor_test_price,
                        "captured" => 0,
                        "paid" => 0,
                    ]);

        try {
            $amount_stripe = number_format($tutor_test_price, 2) * 100; //amount in cents
            $charge = null;
            
            $token = $_POST['stripeToken'] ?? null;

            $description = "{$tutor_test->user->first_name}'s {$tutor_test->proficiency_exam->name} Test - {$user->full_name}'s Payment ($payment->ref_id)";    

            $charge = \Stripe\Charge::create(array(
                        "amount" => $amount_stripe, // amount in cents
                        "currency" => "usd",
                        "source" => $token,
                        "description" => $description,
                        /*"receipt_email" => $user->email,*/ //send stripe e-mail
                    ));                     

            if ($charge) {
                $payment->transaction_id = $charge->id;
                //$payment->stripe_customer_id = $customer_id ?? null;
                $payment->captured = $charge->captured;
                $payment->paid = $charge->paid;
                $payment->completed_at = date("Y-m-d H:i:s", $charge->created); 
                $payment->save();   

                //Update User Role
                $user->addRole('candidate');

                //Email Receipt
                $subject = "Proficiency Exam Subscription Receipt";
                $description = "{$tutor_test->user->first_name}'s {$tutor_test->proficiency_exam->name} Proficiency Exam";

                $email_view = 'btn.emails.email_receipt';
                $email_data = ['user' => $user,
                                'ref_id' => $payment->ref_id,
                                'description' => $description,
                                'amount' => $tutor_test_price
                                ];

                Mail::to($user)
                        ->send(new GeneralEmail($subject, $email_view, $email_data));
            }
            
            \Session::flash('success', 'Payment Processed, Good Luck!');         

            return redirect()->route('take.tutor.test', ['tutor_test_ref_id' => $tutor_test->ref_id, 'tutor_candidate_ref_id' => $tutor_candidate->ref_id]);    	

        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return view('btn.subscriptions.payment_failed')->with('error', $error);

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return view('btn.subscriptions.payment_failed')->with('error', $error);

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return view('btn.subscriptions.payment_failed')->with('error', $error);

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return view('btn.subscriptions.payment_failed')->with('error', $error);
        }
    }

    public function takeTutorTest($tutor_test_ref_id, $tutor_candidate_ref_id){
    	$user = auth()->user();
        $tutor_test = TutorTest::where('ref_id', $tutor_test_ref_id)->first();
        $tutor_candidate = TutorCandidate::where('ref_id', $tutor_candidate_ref_id)->first();
        
        $tutor_test_price = number_format(Subscription::where('tutor_test', 1)->max('price'),0);

        
        if (!$tutor_candidate) {
        	\Session::flash('error', 'Test Not Found');

        	return redirect()->route('home');

        }

        //Identify User & Grant/Deny Access
        if ($user->id != $tutor_candidate->user_id && $user->email == $tutor_candidate->email) {
        	//If Tutor: Add User ID
        	$tutor_candidate->user_id = $user->id;
	        $tutor_candidate->save();
        }

        elseif ($user != $tutor_candidate->user && !$user->isAdmin()) {
    		if ($tutor_test->user_id == $user->id) {
    			//If the student attempts to use the link
    			\Session::flash('warning', 'Your tutors will be able to access the test using this link.');	
    		}
    		else {
    			\Session::flash('error', 'You are not authorized to view this page.  Make sure your ExamStash account e-mail address matches the e-mail address you received the exam link.');	
    		}
    		
    		return redirect()->route('home');
    	}
        
	    if ($tutor_candidate->submitted) {
	    	\Session::flash('error', "You've already submitted this test.");
	    		
    		return redirect()->route('home');
	    }

        $questions = $tutor_test->questions;

    	return view('btn.tutors.take_tutor_test', compact('tutor_test', 'tutor_candidate', 'questions', 'tutor_test_price'));
    }

    public function gradeTutorTest(Request $request, $tutor_test_ref_id, $tutor_candidate_ref_id){
    	$tutor_test = TutorTest::where('ref_id', $tutor_test_ref_id)->first();
    	$user = auth()->user();
    	$tutor_candidate = TutorCandidate::where('tutor_test_id', $tutor_test->id)
        								->where('submitted', 0)
        								->where('user_id', $user->id)
        								->whereHas('tutor_test_payments', function ($tutor_payment){
        										$tutor_payment->where('paid', 1);
        								})
        								->first();
        if(!$tutor_candidate)
        {
        	\Session::flash('error', 'Could not grade exam. You may have already submitted this exam.');
        	return redirect()->route('home');
        }

    	$questions = collect();
        foreach ($request->assq as $question_ref_id) {
            $question = Question::where('ref_id', $question_ref_id)->first();
            $questions->push($question);
        }

        //Add To Pivot Table
        $tutor_candidate->questions()->sync($questions->pluck('id')->toArray());
        //Grade Exam
        foreach ($request->answers as $question_id => $answer_id) {
            //Update Assessment-Question Table With Selected Answer
            $tutor_candidate->questions()->updateExistingPivot($question_id, ['selected_answer_id' => $answer_id]);
        }

        $tutor_candidate->submitted = 1;
        $tutor_candidate->submission_date = Carbon\Carbon::now();
        $tutor_candidate->save();

		$tutor_answers = $request->answers;

    	//Email Results To Student
		Mail::to($tutor_test->user)
				->send(new EmailTutorTestResults($tutor_candidate, $tutor_test));

    	return view('btn.tutors.tutor_test_results', compact('tutor_test', 'questions', 'tutor_candidate', 'tutor_answers'));    	
    }

    public function viewTutorResults($tutor_test_ref_id, $tutor_candidate_ref_id){
    	
    	$user = auth()->user();

    	if ($user != auth()->user()  && !$user->isAdmin()) {
    		\Session::flash('error', 'Access Denied.');
    		return redirect()->route('home');
    	}

    	$tutor_test = TutorTest::where('ref_id', $tutor_test_ref_id)->first();
    	$tutor_candidate = TutorCandidate::where('ref_id', $tutor_candidate_ref_id)->first();

    	foreach ($tutor_candidate->questions as $question) {
            $tutor_answers[$question->id] = $question->pivot->selected_answer_id;
        }

        $tutor_answers = array_filter($tutor_answers);
    	
    	return view('btn.proficiency_exams.view_tutor_test', compact('tutor_test', 'tutor_candidate', 'tutor_answers'));
    }
}
