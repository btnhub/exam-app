<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ProficiencyExam;
use App\Course;
use App\TutorTest;
use App\Subscription;

class ProficiencyExamsController extends Controller
{
    public function index(){
    	$proficiency_exams = ProficiencyExam::all();
    	$courses_array = Course::first()->courses_array();

    	return view('btn.proficiency_exams.index', compact('proficiency_exams', 'courses_array'));
    }

    public function store(Request $request){
    	$prof_exam = new ProficiencyExam(['ref_id' => strtoupper(str_random(12))]);
    	$prof_exam->fill($request->except("_token"));
    	$prof_exam->available = 1;

        $prof_exam->save();

    	\Session::flash('success', 'Proficiency Exam Created & Marked As Available');

    	return redirect()->back();
    }

    public function addProficiencyCourses(Request $request, $prof_exam_id){
    	$this->validate($request, [
	        'course_ids' => 'required'
	    ]);

    	$prof_exam = ProficiencyExam::find($prof_exam_id);

    	$prof_exam->courses()->sync($request->course_ids);

    	\Session::flash('success', 'Courses Added');
    	return redirect()->back();
    }

    public function proficiencyExamsInfo(){
        $proficiency_exams = (new ProficiencyExam)->available_exams();

        $departments = $proficiency_exams->pluck('department')->unique();

        $tutor_test_price = number_format(Subscription::where('tutor_test', 1)->where('active', 1)->max('price'),0);

    	return view('btn.proficiency_exams.proficiency_exams_info', compact('proficiency_exams', 'tutor_test_price', 'departments'));
    }

}
