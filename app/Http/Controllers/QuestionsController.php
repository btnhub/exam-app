<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exam;
use App\Question;
use App\Answer;
use App\Topic;
use App\Course;

class QuestionsController extends Controller
{
	public function __construct(){

        $this->levels = ['' => "Select A Level",
                   'beginner' => "Beginner",
                   'intermediate' => "Intermediate",
                   'advanced' => "Advanced"];
    }

    public function createImageName(Question $question){
        $course = $question->exam->course;
        $exam = $question->exam;

        $course_name = str_replace(" ", "", $course->course_number);
        $school = $course->school->school_code;
        $semester = $exam->semester->semester_code;
        $year = substr($exam->year, 2);
        $exam_number = sprintf("%02s", $exam->exam_number);
        $question_number = sprintf("%02s", $question->question_number);

        $image_filename = "$course_name$school$semester$year$exam_number{$question_number}Q";
        
        return $image_filename;
    }

    public function createTopicsArray($course_id, $department_id){
        $course = Course::find($course_id);

        $topics = array();

        $topic_collection = Topic::where('course_id', $course_id)->get();

        //Other Topics in Same Department
        $dept_courses_ids = Course::where('department_id', $department_id)->pluck('id');

        $similar_courses_topics = $course->similar_course_topics()
                                        ->whereNotIn('id', $topic_collection->pluck('id'));


        $other_topics_collection = Topic::whereIN('course_id', $dept_courses_ids)
                                          ->whereNotIn('id', $topic_collection->pluck('id'))
                                          ->whereNotIn('id', $similar_courses_topics->pluck('id'))
                                          ->get(); 

        foreach ($topic_collection as $topic) {
            $topics[$topic->id] = $topic->topic;
        }

        if ($similar_courses_topics->count()) {
            
            $topics ["s"] = "-----SIMILAR COURSE TOPICS--------";
            foreach ($similar_courses_topics as $similar_topic) {
                $topics[$similar_topic->id] = $similar_topic->topic;
            }
        }
        
        if ($other_topics_collection->count()) {
            $topics ["d"] = "--------DEPT TOPICS-----------";
            foreach ($other_topics_collection as $dept_topic) {
                $topics[$dept_topic->id] = $dept_topic->topic;
            }
        }
        
        return $topics;
    }

    public function createQuestion($exam_id)
	{
        $exam = Exam::find($exam_id);
        
        $topics = $this->createTopicsArray($exam->course->id, $exam->course->department->id);
        
        $levels = $this->levels;

        return view('btn.questions.create_question', compact('exam', 'topics', 'levels'));
        
	}

    public function saveQuestion(Request $request, $exam_id)
    {
        //$this->validate($request, ['question_image' => 'image']);

        $exam = Exam::find($exam_id);
        
        $question_exists = Question::where('exam_id', $exam->id)
                                    ->where('question_number', $request->question_number)
                                    ->count();
                                    
        if ($question_exists) {
            \Session::flash('error', "Question Exists");

            return redirect()->back();
        }

        $ref_id = strtoupper(str_random(12));
        $answers = array_filter($request->answer, 'strlen');
        
        $question = new Question ([
                        'ref_id' => $ref_id,
                        'exam_id' => $exam_id,
                        'question' => $request->question
                        ]);
        $question->submitted_by = auth()->user()->id;
        $question->save();

        $question->update($request->except(['_token', 'answer', 'correct_answer', 'topic_ids', 'question_image']));

        if ($request->hasFile('question_image')) {

            $question_image_name = $this->createImageName($question);
            
            $extension = substr(strrchr($request->file('question_image')->getClientOriginalName(),'.'),1);
            
            $question_image_name .= ".$extension";

            $file_move = $request->file('question_image')->move(public_path('btn/exam_images/questions'), $question_image_name);

            $question->question_image = $question_image_name;
            $question->save();  
        }

        //Update Answers
        foreach ($answers as $key => $answer_value) {
            //TO DO: Answer Images?!
            $answer = Answer::create([
                        'question_id' => $question->id,
                        'answer' => $answer_value,
                    ]);

            $answer->correct_answer = $request->correct_answer == $key ? 1 : 0;
            $answer->save();        
        }

        //Add Topics
        if ($request->topic_ids) {
            $question->topics()->sync($request->topic_ids);
        } 

        \Session::flash('success', 'Question Created!');

        return redirect()->route('show-question', $ref_id);
    }

    public function showQuestion($ref_id)
    {
    	$question = Question::where('ref_id', $ref_id)->first();

    	$exam_collection = Exam::all();
    	foreach ($exam_collection as $exam) {
    		$exams[$exam->id] = $exam->details();
    	}

    	asort($exams);

        $topics = $this->createTopicsArray($question->exam->course->id, $question->exam->course->department->id);
        $levels = $this->levels;

    	return view('btn.questions.edit_question', compact('question', 'exams', 'topics', 'levels'));

    }

    public function updateQuestion(Request $request, $ref_id)
    {
    	//$this->validate($request, ['image_file' => 'image']);
        $correct_answer_id = $request->correct;

    	//Update Question Statement & Details
    	$question = Question::where('ref_id', $ref_id)->first();
    	
    	$question->update($request->except(['_token', 'answer', 'correct', 'topic_ids', 'advanced']));
    	
        if ($request->hasFile('question_image')) {

            $question_image_name = $this->createImageName($question);
            
            $extension = substr(strrchr($request->file('question_image')->getClientOriginalName(),'.'),1);
            
            $question_image_name .= ".$extension";

            $file_move = $request->file('question_image')->move(public_path('btn/exam_images/questions'), $question_image_name);

            $question->question_image = $question_image_name;
            $question->save();  
        }
        
    	//Update Answers
    	foreach ($request->answer as $answer_id => $answer_value) {
    		//TO DO: Answer Images?!
    		$answer = Answer::find($answer_id);
    		$answer->answer = $answer_value;
    		$answer->correct_answer = $answer->id == $correct_answer_id ? 1 : 0;
    		$answer->save();
    	}

        //Update Advanced If Unchecked
        $question->advanced = isset($request->advanced) ? 1 : 0;
        $question->save();

    	//Update Topics
    	if ($request->topic_ids) {
    		$question->topics()->sync($request->topic_ids);
    	}

    	\Session::flash('success', 'Question Updated!');
    	
    	return redirect()->route('show-question', $ref_id);

    } 

    public function viewQuestions($course_id)
    {
    	//quickly add topics to all questions in a course
    	$course = Course::find($course_id);

        $topics = $this->createTopicsArray($course->id, $course->department->id);

        $levels = $this->levels;

    	return view('btn.view_questions', compact('course', 'topics', 'levels'));
    }

    public function addTopics(Request $request, $course_id)
    {
        $course = Course::find($course_id);

        //Update Advanced
        if (isset($request->advanced)) {
            $advanced_q_ids_array = array_keys($request->advanced);
            foreach ($request->advanced as $advanced_q_id => $advanced) {
                $question = Question::find($advanced_q_id);
                $question->advanced = 1;
                $question->save();
            }

            //Update any question previously marked as advanced whose option has been deselected
            $non_advanced_questions = $course->questions()->whereNotIn('id', $advanced_q_ids_array)->where('advanced', 1);
            
            foreach ($non_advanced_questions as $non_advanced_question) {
                $non_advanced_question->advanced = 0;
                $non_advanced_question->save();
            }
        }

        else
        {
            //remove Advanced from all questions
           foreach ($course->questions()->where('advanced', 1) as $question) {
                $question->advanced = 0;
                $question->save();
            } 
        }
        

        //Update Level
        if (isset($request->level)) {
            foreach (array_filter($request->level, 'strlen') as $level_q_id => $level) {
                $question = Question::find($level_q_id);
                $question->level = $level;
                $question->save();
            }
        }
        

        //Add Topics
        if (isset($request->topic_ids)) {
            foreach ($request->topic_ids as $q_id => $topics) {
                $question = Question::find($q_id);
                $question->topics()->sync($topics);
            }
        }
    	

    	\Session::flash('success', 'Questions Updated');

    	return redirect()->route('view.questions', $course_id);

    }

    public function deleteQuestion()
    {
    	dd('delete_question');
    }

    public function deleteAnswer()
    {
    	dd('delete_answer');
    }
}
