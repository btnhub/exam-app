<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Course;
use App\Topic;

class CoursesController extends Controller
{
    public function addCourseTopics($course_id){
    	$course = Course::find($course_id);

    	return view('btn.courses.add_topics', compact('course'));
    }

    public function saveCourseTopics(Request $request, $course_id){
    	$count = 0;

    	foreach ($request->topics as $key => $topic) {
    		if ($topic) {
    			Topic::create(['course_id' => $course_id,
    						'topic' => $topic]);
    			$count++;
    		}
    	}

    	if ($count) {
    		\Session::flash('success', "$count Topics Added");
    	}

    	else
    	{
    		\Session::flash('error', 'Add Topic');	
    	}

    	return redirect()->back();
    }
}
