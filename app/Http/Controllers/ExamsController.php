<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Exam;
use App\School;
use App\Semester;
use App\Course;
use App\Question;
use App\Answer;
use App\Package;

class ExamsController extends Controller
{
    public function __construct()
	{
		$this->exam_path = base_path("exams_csv");	
	}

    public function saveExam(Request $request)
    {
        unset($request['_token']);
        //dd($request->all());
        $exam_exists = Exam::where('course_id', $request->course_id)->where('year', $request->year)->where('semester_id', $request->semester_id)->where('exam_number', 'like',$request->exam_number)->count();
        if ($exam_exists) {
            \Session::flash('error', "Exam Exists");

            return redirect()->back();
        }
        $exam = Exam::create($request->all());
        $exam->submitted_by = auth()->user()->id;
        $exam->save();

        return redirect()->route('load-exam', $exam->id);
    }

    public function updateCompletedOrReviewed($exam_id, $option, $value){
        $exam = Exam::find($exam_id);

        switch ($option) {
            case 'completed':
                $exam->completed = $value;

                break;
            case 'reviewed':
                $exam->reviewed = $value;

                break;
            
        }
        
        $exam->save();

        \Session::flash('success', "Exam Updated");

        return redirect()->back();
    }

    public function selectExam()
    {
    	$exams = Exam::orderBy('course_id', 'ASC')
    					->orderBy('semester_id', 'ASC')
    					->orderBy('year', 'DESC')
    					->orderBy('exam_number', 'ASC')
    					->get();
    	
    	$courses = Course::all();

    	$packages = Package::all();

        $courses_array = (new Course())->courses_array();

        $semesters = Semester::pluck('semester', 'id')->toArray();

    	return view('btn.select_exam', compact('exams', 'courses', 'packages', 'courses_array', 'semesters'));
    }

    public function loadExam($exam_id)
    {
    	$exam = Exam::find($exam_id);
    	return view('btn.view_exam', compact('exam'));

    }

    public function takeExam($exam_id)
    {
    	$exam = Exam::find($exam_id);
    	return view('btn.take_exam', compact('exam'));

    }

    public function gradeExam(Request $request, $exam_id)
    {
		$exam = Exam::find($exam_id);

		$no_correct = 0;
		$no_questions = $exam->questions->count();

		foreach ($request->answers as $question_id => $answer_id) {
			$answer_db = Answer::find($answer_id);
			if ($answer_db->correct_answer) {
				$no_correct++;
			}
		}

		$grade = number_format($no_correct / $no_questions, 2) * 100;
		$student_answers = $request->answers;

    	return view('btn.exam_results', compact('exam','grade', 'student_answers'));    	
    }

    public function examUploadProgress()
    {
        //For Staff
        $user = auth()->user();
        $courses_array = (new Course())->courses_array();
        $semesters = Semester::pluck('semester', 'id')->toArray();
        $incomplete_exams = Exam::where('completed', 0)
                                ->whereNotIn('id', $user->exams_uploaded()->pluck('id'))
                                ->orderBy('course_id', 'ASC')
                                ->orderBy('semester_id', 'ASC')
                                ->orderBy('year', 'DESC')
                                ->orderBy('exam_number', 'ASC')
                                ->get();
        
        return view('btn.staff.exam_upload_progress', compact('user', 'courses_array', 'semesters', 'incomplete_exams')); 
    }

    public function deleteExam()
    {
    	dd('delete_exam');
    }
}
