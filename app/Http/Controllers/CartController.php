<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;

use App\Package;
use App\Subscription;

class CartController extends Controller
{
    public function index()
    {
        dd('cart');
    }

    public function addToCart(Request $request, $package_ref_id)
    {
        //dd(Cart::content());
        $package = Package::where('ref_id', key($request->subscription_id))->first();
        $subscription = Subscription::where('ref_id', $request->subscription_id[$package->ref_id])->first();
        
        $subscription_price = $subscription->on_sale ? $subscription->sale_price : $subscription->price;

        $item_exists = 0;
        if (Cart::count()) {
            foreach (Cart::content() as $cart_item) {
                if ($subscription->id == $cart_item->id && $package->ref_id == $cart_item->options->package_ref) {
                    \Session::flash('warning', 'Subscription Already In Cart');
                    return redirect()->back();
                }
            }
        }

        Cart::add($subscription->id, $subscription->name, 1, $subscription_price, [
                        'package_ref' => $package_ref_id,
                        'package_name' => $package->title,
                        'package_description' => $package->description])
                ->associate('App\Subscription');            

        \Session::flash('success', 'Added To Cart');
        return redirect()->back();
    }

    public function removeItem($row_id)
    {
    	Cart::remove($row_id);

        \Session::flash('success', "Shopping Cart Updated");

        return redirect()->back();
    }

    public function emptyCart()
    {
    	dd('empty');
    	Cart::destroy();

        \Session::flash('success', "Cart Emptied");

        return redirect()->back();            
    }
}
