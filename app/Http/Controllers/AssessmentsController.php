<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Gate;

use App\Package;
use App\Question;
use App\Answer;
use App\Topic;
use App\Assessment;
use App\User;

use Auth;

class AssessmentsController extends Controller
{
    public function storeComplaint(User $user, $question_ref_id, $complaint){
        $question = Question::where('ref_id', $question_ref_id)->first();
        $question->problem = 1;

        $complaint = date('m/d/Y')." - $user->full_name ($user->id) -  $complaint";

        $question->complaint .= $question->complaint ? "\n".$complaint : $complaint;

        $question->save();
    }

    public function viewTopics($package_ref_id)
    {
        $package = Package::where('ref_id', $package_ref_id)->first();

        if (!auth()->user()->isAdmin() && Gate::denies('access-package', $package)) {
            \Session::flash('error', 'You are not authorized to view this page');
            return redirect('/home');
        }
        
        if (auth()->user()->isAdmin()) {
            $topic_ids = $package->topics->pluck('id');
        }
        else
        {
            $topic_ids = $package->active_topics()->pluck('id');    
        }
        
        $topics = collect();
        foreach ($topic_ids as $topic_id) {
            $topic = Topic::where('id', $topic_id)->select('id', 'topic')->first();
            $topics->push($topic);
        }
        
        $topic_chunks = $topics->split(4);
        
        return view('btn.students.view_topics', compact('package', 'topics', 'topic_chunks'));
    }

    public function takeAssessment(Request $request, $package_ref_id)
    {
    	$package = Package::where('ref_id', $package_ref_id)->first();

        if ($request->topic_ids) {
            $topic_ids = $request->topic_ids;
            $questions_collection = Question::whereHas('topics', function($query) use ($topic_ids)
                {
                    $query->whereIn('topic_id', $topic_ids);
                })
                ->get();
        }
        else {
            $questions_collection = $package->questions();
        }
    	
        $questions = $questions_collection->shuffle()->take(15);

    	return view('btn.students.take_assessment', compact('questions', 'package', 'assessment_ref_id'));
    }

    public function pauseAssessment(Request $request, $package_ref_id){
        //Store any complaints
        $user = auth()->user();
        foreach ($request->complaint as $question_ref_id => $complaint) {
            if (trim($complaint)) {
                $complaint = trim($complaint);
                $this->storeComplaint($user, $question_ref_id, $complaint);
            }
        }
        
        $package = Package::where('ref_id', $package_ref_id)->first();
        
        $questions = collect();
        foreach ($request->assq as $question_ref_id) {
            $question = Question::where('ref_id', $question_ref_id)->first();
            $questions->push($question);
        }

        if ($request->assid) {
            //Update existing assessment
            $assessment = Assessment::where('ref_id', $request->assid)->first();

            foreach ($request->answers as $question_id => $answer_id) {
                    //Update Assessment-Question Table With Selected Answer
                    $assessment->questions()->updateExistingPivot($question_id, ['selected_answer_id' => $answer_id]);                    
            }
        }

        else{
            //create assessment test
            $assessment = Assessment::create([
                            'ref_id' => strtoupper(str_random(12)),
                            'user_id' => Auth::user()->id,
                            'package_id' => $package->id,
                                ]);
            $assessment->questions()->attach($questions->pluck('id')->toArray());
            //$assessment_ref_id = $assessment->ref_id;
            foreach ($request->answers as $question_id => $answer_id) {
                    //Update Assessment-Question Table With Selected Answer
                    $assessment->questions()->updateExistingPivot($question_id, ['selected_answer_id' => $answer_id]);                    
            }
        }
        

        \Session::flash('success', "Progress Saved");

        return redirect('home');
    }

    public function resumeAssessment($assessment_ref_id){
        $assessment = Assessment::where('ref_id', $assessment_ref_id)->first();
        $package = $assessment->package;

        if (!auth()->user()->isAdmin() && Gate::denies('access-package', $package)) {
            \Session::flash('error', 'You are not authorized to view this page');
            return redirect('/home');
        }

        $questions = $assessment->questions;
        $assessment_ref_id = $assessment->ref_id;

        return view('btn.students.take_assessment', compact('questions', 'package', 'assessment_ref_id'));   

    }

    public function gradeAssessment(Request $request, $package_ref_id)
    {
        //Store any complaints
        $user = auth()->user();
        foreach ($request->complaint as $question_ref_id => $complaint) {
            if (trim($complaint)) {
                $complaint = trim($complaint);
                $this->storeComplaint($user, $question_ref_id, $complaint);
            }
        }

        $package = Package::where('ref_id', $package_ref_id)->first();

    	$questions = collect();
        foreach ($request->assq as $question_ref_id) {
            $question = Question::where('ref_id', $question_ref_id)->first();
            $questions->push($question);
        }

		$no_correct = 0;
		$no_questions = $questions->count();


        if (isset($request['answers'])) {
            $assessment = null;

            if ($request->assid) {
                $assessment = Assessment::where('ref_id', $request->assid)->first();
                $assessment->submitted = 1;
                $assessment->save();
            }
            else{
                //create assessment test
                $assessment = Assessment::create([
                                'ref_id' => strtoupper(str_random(12)),
                                'user_id' => $user->id,
                                'package_id' => $package->id,
                                'submitted' => 1
                                    ]);
                $assessment->questions()->attach($questions->pluck('id')->toArray());
            }

            foreach ($request->answers as $question_id => $answer_id) {
                $answer_db = Answer::find($answer_id);

                if ($answer_db->correct_answer) {
                    $no_correct++;
                }

                //Update Assessment-Question Table With Selected Answer
                $assessment->questions()->updateExistingPivot($question_id, ['selected_answer_id' => $answer_id]);
            }
        }

		$grade = number_format($no_correct / $no_questions, 2) * 100;
		$student_answers = $request->answers;
        $assessment_ref_id = $assessment->ref_id;

    	return view('btn.students.assessment_results', compact('package', 'questions', 'grade', 'student_answers', 'assessment_ref_id'));    	
    }

    public function viewResults($assessment_ref_id){
        $assessment = Assessment::where('ref_id', $assessment_ref_id)->first();
        $package = $assessment->package;

        if (!auth()->user()->isAdmin() && Gate::denies('access-package', $package)) {
            \Session::flash('error', 'You are not authorized to view this page');
            return redirect('/home');
        }

        $questions = $assessment->questions;
        $grade = $assessment->grade();
        
        foreach ($assessment->questions as $question) {
            $student_answers[$question->id] = $question->pivot->selected_answer_id;
        }

        $student_answers = array_filter($student_answers);
        $assessment_ref_id = $assessment->ref_id;

        return view('btn.students.assessment_results', compact('package', 'questions', 'grade', 'student_answers', 'assessment_ref_id')); 
    }

    public function reportIssue(Request $request, $question_ref_id, $assessment_ref_id)
    {
        $user = auth()->user();
        foreach ($request->complaint as $question_ref_id => $complaint) {
            if (trim($complaint)) {
                $complaint = trim($complaint);
                $this->storeComplaint($user, $question_ref_id, $complaint);
            }
        }

        \Session::flash('success', 'Your complaint has been submitted.  We will address it shortly.');

        return redirect()->route('view.results', ['assessment_ref_id' => $assessment_ref_id]);
    }
}
