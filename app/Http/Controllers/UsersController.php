<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\ProficiencyExam;

class UsersController extends Controller
{
    public function index(){
    	$users = User::all();

    	return view('btn.admin.users', compact('users'));
    }

    public function cloneUser($user_id)
    {
        auth()->loginUsingId($user_id);
        $user = auth()->user();
        \Session::flash('success', "Logged In As $user->full_name");
        
        return redirect()->route('home');
    }

    public function myAccount(){
        $user = auth()->user();
        $proficiency_exams = (new ProficiencyExam)->available_exams()
                                                    ->pluck('name', 'id');

        return view('my_account', compact('user', 'proficiency_exams'));
    }

    public function editAccount($user_ref_id){
        $user = User::where('ref_id', $user_ref_id)->first();
        
        $this->authorize('self-access', $user);

        return view('btn.users.edit_account', compact('user'));
    }

    public function updateAccount(Request $request, $user_ref_id){
        $user = User::where('ref_id', $user_ref_id)->first();
        
        $this->authorize('self-access', $user);

        if (trim($request['password']) == '') 
        {
            //remove password if not entered to prevent error with validation rule of min 6 characters
            unset($request['password']);
        }

        $this->validate($request, [
                    'first_name' => 'required|min:2|max:255',
                    'last_name' => 'required|min:2|max:255',
                    'email' => 'required|email|unique:users,email,'.$user->id,
                    'password' => 'sometimes|min:6|confirmed',
                    'phone' => 'sometimes|max:20',
                ]);   
        
        $user->update($request->except('_token', 'password', 'password_confirmation'));

        if (trim($request['password']) != '') {
            $user->password = bcrypt($request['password']); 
            $user->save();
        }

        \Session::flash('success', 'Account Updated!');

        return redirect()->route('home');
    }
    
}
