<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class Assessment extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    //Relationships
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function package(){
        return $this->belongsTo('App\Package');
    }

    public function questions(){
        return $this->belongsToMany('App\Question')
        			->withTimestamps()
                    ->withPivot('selected_answer_id');
    }

    //Methods
    public function no_correct(){
        $no_correct = 0;

        foreach ($this->questions as $question) {
            if ($question->correct_answer()->id == $question->pivot->selected_answer_id) {
                $no_correct++;
            }
        }

        return $no_correct;
    }
    
    public function grade(){
        $no_questions = $this->questions->count(); //# of questions in test

        return number_format($this->no_correct() / $no_questions, 2) * 100;
    }

    public function total_answered(){
        return DB::table('assessment_question')
                    ->where('assessment_id', $this->id)
                    ->whereNotNull('selected_answer_id')
                    ->count();
    }

    public function topics_covered(){
        $questions = $this->questions;

        $topics_covered = array();
        foreach ($questions as $question) {
            foreach ($question->topics as $topic) {
                $topics_covered[$topic->id] = $topic->topic;
            }
        }

        ksort($topics_covered);

        return $topics_covered;
    }

    public function topic_questions($topic_id){
        //Find questions for specific topic in this specific Assessment
        $topic_questions = $this->questions()->whereHas('topics', function($query) use ($topic_id)
                                {
                                    $query->where('topics.id', $topic_id);
                                })->get();
        return $topic_questions;
    }
    
    public function topic_performance($topic_id){
        $no_correct = 0;
        
        $topic_questions = $this->topic_questions($topic_id);
        
        foreach ($topic_questions as $topic_question) {
            if ($topic_question->pivot->selected_answer_id == $topic_question->correct_answer()->id) {
                $no_correct++;
            }
        }

        return number_format($no_correct / $topic_questions->count(), 2) * 100;
    }
}
