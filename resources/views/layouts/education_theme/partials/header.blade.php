<head>
 
    <meta charset="utf-8">  
    
    <title>@yield('page_title', config('app.name'))</title>
    <meta name="description" content="Get Ready For Your Next Exam"> 
    <meta name="author" content="Exam Stash"> <!--insert your name here-->
    <meta name="keywords" content="College Exams, College Tests, University Exams, University Tests, Math Exams, Math Tests, Science Exams, Science Tests, Chemistry Exams, Chemistry Tests, Physics Exams, Physics Tests">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> <!--meta responsive-->
    
    <!--START CSS--> 
    {{--Bootstrap--}}
    <link rel="stylesheet" href="{{asset('css/education_theme/nicdark_style.css')}}"> <!--style-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> <!--style-->

    <!--google fonts-->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    
    <link href="https://fonts.googleapis.com/css?family=Yeseva+One" rel="stylesheet">

    <!--[if lt IE 9]>  
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>  
    <![endif]-->  

    <!--FAVICONS-->
    <link rel="shortcut icon" href="{{asset('img/education_theme/favicon/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('img/education_theme/favicon/apple-touch-icon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/education_theme/favicon/apple-touch-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/education_theme/favicon/apple-touch-icon-114x114.png')}}">
    <!--END FAVICONS-->
    
    @yield('header')
</head> 