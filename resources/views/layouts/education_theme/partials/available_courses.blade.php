<div class="nicdark_section nicdark_bg_greydark">

<!--start nicdark_container-->
<div class="nicdark_container nicdark_clearfix">


    <div class="nicdark_section nicdark_height_50"></div>

    
    <div class="grid grid_12">

        <h1 class="nicdark_font_size_50 nicdark_color_white"><strong>Available Exams</strong></h1>

    </div>


    <div class="row"> 
        <div class="col-md-8">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                      <li data-target="#myCarousel" data-slide-to="1"></li>
                      <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                      {{-- <div class="item active">
                        <img src="{{asset('img/screenshots/chemistry-chem1-01-table_reaction-817x700.png')}}" alt="" style="vertical-align: center">
                      </div> --}}

                      <div class="item active">
                        <img src="{{asset('img/select_topics-740x416.png')}}" alt="">
                      </div>

                      <div class="item">
                        <img src="{{asset('img/screenshots/math-calc2-04-integrals-793x700.png')}}" alt="">
                      </div>

                      {{-- <div class="item">
                        <img src="{{asset('img/screenshots/physics-phys1-01-814x700.png')}}" alt="">
                      </div> --}}

                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right"></span>
                      <span class="sr-only">Next</span>
                    </a>
            </div> 
        </div>

        <div class="col-md-4">
            <div class="row">
                <div class="nicdark_section">
                    <div class="nicdark_float_left nicdark_width_65_percentage nicdark_width_100_percentage_all_iphone">
                    

                        <div class="nicdark_section nicdark_padding_left_20 nicdark_padding_left_0_all_iphone nicdark_box_sizing_border_box">

                            <h2 class="nicdark_color_white nicdark_margin_top_20_all_iphone"><strong>Math</strong></h2>
                            <div class="nicdark_section nicdark_height_10"></div>
                            <p class="nicdark_color_grey">Precalc, Calc 1, Calc 2, Calc 3.</p>
                            <div class="nicdark_section nicdark_height_10"></div>
                            <p class="nicdark_color_grey"><b>Coming Soon: </b> <br>
                            Algebra, Differential Equations/Linear Algebra</p>
                            <div class="nicdark_section nicdark_height_10"></div>
                            {{-- <a class="nicdark_display_inline_block nicdark_color_grey nicdark_color_greydark_hover nicdark_bg_white_hover nicdark_transition_all_08_ease nicdark_border_1_solid_grey_2 nicdark_first_font nicdark_padding_8 nicdark_border_radius_3 nicdark_font_size_13" href="#">KNOW ME</a> --}}
                        </div>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="nicdark_section">
                    
                    <div class="nicdark_float_left nicdark_width_65_percentage nicdark_width_100_percentage_all_iphone">
                    

                        <div class="nicdark_section nicdark_padding_left_20 nicdark_padding_left_0_all_iphone nicdark_box_sizing_border_box">

                            <h2 class="nicdark_color_white nicdark_margin_top_20_all_iphone"><strong>Physics</strong></h2>
                            <div class="nicdark_section nicdark_height_10"></div>
                            <p class="nicdark_color_grey">Physics 1 (Classical Mechanics), Physics 2 (Electricity & Magnetism)</p>
                            
                            {{-- <a class="nicdark_display_inline_block nicdark_color_grey nicdark_color_greydark_hover nicdark_bg_white_hover nicdark_transition_all_08_ease nicdark_border_1_solid_grey_2 nicdark_first_font nicdark_padding_8 nicdark_border_radius_3 nicdark_font_size_13" href="#">KNOW ME</a> --}}
                            
                        </div>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="nicdark_section">
                    
                    <div class="nicdark_float_left nicdark_width_65_percentage nicdark_width_100_percentage_all_iphone">
                    

                        <div class="nicdark_section nicdark_padding_left_20 nicdark_padding_left_0_all_iphone nicdark_box_sizing_border_box">

                            <h2 class="nicdark_color_white nicdark_margin_top_20_all_iphone"><strong>Chemistry</strong></h2>
                            <div class="nicdark_section nicdark_height_10"></div>
                            <p class="nicdark_color_grey">General Chemistry 1-2.</p>
                            <div class="nicdark_section nicdark_height_10"></div>
                            <p class="nicdark_color_grey"><b>Coming Soon:</b><br>
                             Organic Chemistry 1-2.</p>
                            <div class="nicdark_section nicdark_height_10"></div>
                            {{-- <a class="nicdark_display_inline_block nicdark_color_grey nicdark_color_greydark_hover nicdark_bg_white_hover nicdark_transition_all_08_ease nicdark_border_1_solid_grey_2 nicdark_first_font nicdark_padding_8 nicdark_border_radius_3 nicdark_font_size_13" href="#">KNOW ME</a> --}}
                        </div>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="nicdark_section">
                    
                    <div class="nicdark_float_left nicdark_width_65_percentage nicdark_width_100_percentage_all_iphone">
                    

                        <div class="nicdark_section nicdark_padding_left_20 nicdark_padding_left_0_all_iphone nicdark_box_sizing_border_box">

                            <h2 class="nicdark_color_white nicdark_margin_top_20_all_iphone"><strong>More To Come!</strong></h2>
                            <br>

                            <a href="{{route('purchase.select.packages')}}" class="btn btn-lg btn-info"> <b>Get Started </b></a>
                            {{-- <div class="nicdark_section nicdark_height_10"></div>
                            <p class="nicdark_color_grey">General Chemistry 1-2.</p>
                            <div class="nicdark_section nicdark_height_10"></div>
                            <p class="nicdark_color_grey"><b>Coming Soon:</b><br>
                             Organic Chemistry 1-2.</p>
                            <div class="nicdark_section nicdark_height_10"></div> --}}
                            {{-- <a class="nicdark_display_inline_block nicdark_color_grey nicdark_color_greydark_hover nicdark_bg_white_hover nicdark_transition_all_08_ease nicdark_border_1_solid_grey_2 nicdark_first_font nicdark_padding_8 nicdark_border_radius_3 nicdark_font_size_13" href="#">KNOW ME</a> --}}
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>
    


    <div class="nicdark_section nicdark_height_50"></div>
    

</div>
<!--end container-->


</div>

<!--end container-->
