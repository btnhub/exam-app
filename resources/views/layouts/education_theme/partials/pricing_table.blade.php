<?php
    $count = 0;
    $photos = [ 1 => "img/female_student_chalkboard-740x416.png",
                2 => "img/einstein_equation-740x416.png",
                3 => "img/male_student_chalkboard-740x416.png",
                4 => "img/female_student_laptop_corridor-740x416.png",
                5 => "img/apple_books-740x416.png",
                6 => "img/female_student_chalkboard-740x416.png"
                ];
?>
<div class="nicdark_section ">

    <div class="nicdark_container nicdark_clearfix">
        @foreach ($subscriptions as $subscription)
            <?php $count++; ?>

            <div class="grid grid_4 ">
                <!--START price-->
                <div class="nicdark_section nicdark_box_sizing_border_box">
                    
                    <div class="nicdark_section nicdark_position_relative">
                            
                        <img alt="" class="nicdark_section" src="{{asset("$photos[$count]")}}">

                        <div class="nicdark_bg_greydark_alpha_6 nicdark_position_absolute nicdark_left_0 nicdark_height_100_percentage nicdark_width_100_percentage nicdark_box_sizing_border_box">
                            
                            <div class="nicdark_position_absolute nicdark_bottom_30 nicdark_width_100_percentage nicdark_padding_botttom_0 nicdark_padding_50 nicdark_box_sizing_border_box nicdark_text_align_center">
                                

                                <h3 class="nicdark_color_white"><strong>{{$subscription->name}} Plan</strong></h3>

                                <div class="nicdark_section nicdark_height_10"></div>
                                <div class="nicdark_section nicdark_height_10 nicdark_display_none_all_iphone"></div>

                                <div class="nicdark_text_align_center_all_iphone nicdark_width_50_percentage nicdark_width_100_percentage_all_iphone nicdark_float_left nicdark_box_sizing_border_box nicdark_padding_right_10 nicdark_text_align_right">
                                    <h1 class="nicdark_color_white nicdark_font_size_60 nicdark_font_size_40_all_iphone nicdark_line_height_40_all_iphone"><strong><span class="nicdark_font_size_20 nicdark_margin_right_10">$</span>{{number_format($subscription->on_sale ? $subscription->sale_price : $subscription->price, 0)}}</strong></h1>

                                        {{-- <p class="nicdark_color_white "> {{$subscription->duration}} Weeks </p> --}}
                                    
                                </div>

                                <div class="nicdark_display_none_all_iphone  nicdark_width_50_percentage nicdark_float_left nicdark_box_sizing_border_box nicdark_padding_left_10 nicdark_text_align_left">
                                    <div class="nicdark_section nicdark_height_15"></div>
                                    
                                    @if ($subscription->on_sale)
                                        <p class="nicdark_font_size_30 nicdark_line_height_20 nicdark_color_white"><s>${{number_format($subscription->price,0)}}</s> </p>
                                    @else
                                        
                                    @endif
                                    {{-- <p class="nicdark_font_size_15 nicdark_line_height_20 nicdark_color_white">{{$subscription->duration}} Weeks --}}

                                    </p>
                                </div>
                                
                            </div>

                        </div>

                    </div>



                </div>


                <div class="nicdark_section nicdark_border_1_solid_grey">
                    <div class="nicdark_section nicdark_padding_20 nicdark_box_sizing_border_box">
                        

                        <table class="nicdark_section nicdark_text_align_center">
                            <tbody>                             
                                <tr class="nicdark_border_bottom_2_solid_grey">
                                    <td class="nicdark_padding_5 nicdark_padding_top_0"><img alt="" class="nicdark_display_inline_block nicdark_margin_right_10" width="13" src="{{asset('img/education_theme/icons/icon-check-green.svg')}}"><p class="nicdark_display_inline_block" style="color:black">{{$subscription->duration}} Weeks Access</p></td>
                                </tr>
                                <tr class="nicdark_border_bottom_2_solid_grey">
                                    <td class="nicdark_padding_5">
                                        <p class="nicdark_display_inline_block" style="color:black">
                                            @if ($subscription->on_sale)
                                                <b><span style="color:darkred">On Sale Now!</span> <s>${{$subscription->price}}</s>  ${{$subscription->sale_price}}</b> <br>
                                                <img alt="" class="nicdark_display_inline_block nicdark_margin_right_10" width="13" src="{{asset('img/education_theme/icons/icon-check-green.svg')}}"> ~ ${{number_format($subscription->sale_price / $subscription->duration,0)}} / week
                                            @else
                                                
                                                <img alt="" class="nicdark_display_inline_block nicdark_margin_right_10" width="13" src="{{asset('img/education_theme/icons/icon-check-green.svg')}}">
                                                ~ ${{number_format($subscription->price / $subscription->duration,0)}} / week
                                                <br><br>
                                            @endif
                                        </p></td>
                                </tr>
                                <tr class="nicdark_border_bottom_2_solid_grey">
                                    <td class="nicdark_padding_5"><p class="nicdark_display_inline_block"><br></p></td>
                                </tr>
                            </tbody>
                        </table>

                        {{-- <div class="nicdark_section nicdark_height_20"></div>

                        <div class="nicdark_width_100_percentage  nicdark_box_sizing_border_box nicdark_float_left">
                            <a class="nicdark_display_inline_block nicdark_text_align_center nicdark_box_sizing_border_box nicdark_width_100_percentage nicdark_color_white nicdark_bg_greydark nicdark_first_font nicdark_padding_10_20 nicdark_border_radius_3 " href="{{route('purchase.select.packages')}}" style="color:white"><b>Get Started!</b></a>   
                        </div> --}}


                    </div>
                    
                </div>
                <!--END price-->

            </div>
        @endforeach  
                        
    </div>

</div>  