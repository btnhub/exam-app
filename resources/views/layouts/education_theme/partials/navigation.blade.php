<!--START menu responsive-->
<div class="nicdark_navigation_2_sidebar_content nicdark_padding_40 nicdark_box_sizing_border_box nicdark_overflow_hidden nicdark_overflow_y_auto nicdark_transition_all_08_ease nicdark_bg_green nicdark_height_100_percentage nicdark_position_fixed nicdark_width_300 nicdark_right_300_negative nicdark_z_index_9">

    <img alt="" width="25" class="nicdark_close_navigation_2_sidebar_content nicdark_cursor_pointer nicdark_right_20 nicdark_top_20 nicdark_position_absolute" src="{{asset('img/education_theme/icons/icon-close-white.svg')}}">

    <div class="nicdark_navigation_2_sidebar">
       @include('layouts.partials.top_nav_menu')
    </div>

</div>
<!--END menu responsive-->

{{--Top Dark Bar With Social Media & Login--}}
<div class="nicdark_section">

    <div class="nicdark_section nicdark_bg_greydark">

        <!--start nicdark_container-->
        <div class="nicdark_container nicdark_clearfix">

            <div class="grid grid_6 nicdark_padding_botttom_10 nicdark_padding_top_10 nicdark_text_align_center_responsive">
                <div class="nicdark_navigation_top_header_2">
                    {{--Had Social Media Links--}}
                    <ul>
                        <li>
                            
                            <a href="mailto:Contact@ExamStash.com" style="color:white">
                                <img class="nicdark_margin_right_10 nicdark_float_left" width="15" src="{{asset('img/education_theme/icons/icon-email-2-grey.svg')}}">
                                EMAIL
                            </a>

                            {{-- <ul class="nicdark_sub_menu">
                                <li><a href="#">FACEBOOK</a></li>
                                <li><a href="#">TWITTER</a></li>
                                <li><a href="#">INSTAGRAM</a></li>
                            </ul> --}}

                        </li>
                    </ul>
                </div>
            </div>

            <div class="grid grid_6 nicdark_text_align_right nicdark_border_top_1_solid_greydark_responsive nicdark_text_align_center_responsive nicdark_padding_botttom_10 nicdark_padding_top_10">
                <div class="nicdark_navigation_top_header_2">
                    @include('layouts.partials.top_links')
                </div>
            </div>


        </div>
        <!--end container-->

    </div>

</div>

{{--Top Navigation--}}
<div class="nicdark_section nicdark_position_relative ">

    <div class="nicdark_section nicdark_position_absolute">

        <!--start nicdark_container-->
        <div class="nicdark_container nicdark_clearfix nicdark_position_relative">

            <div class="grid grid_12 nicdark_display_none_all_responsive">

                <div class="nicdark_section nicdark_height_10"></div>

                <!--LOGO-->
                {{-- <a href="{{ url('/') }}"><img alt="" class="nicdark_position_absolute nicdark_left_15 nicdark_top_20" width="170" src="{{asset('img/education_theme/logos/logo-foodlab-white.svg')}}"></a> --}}
                <a class="navbar-brand" href="{{ url('/') }}">
                    <span style="font-family: Yeseva One, cursive; font-size: 40px !important">
                        <span style="color:white"><b>Exam</b></span><b><span style="color:darkred">STASH</span></b>
                    </span>
                    
                </a>

                <!--right icons menu-->
                @if (auth()->user()  && Cart::count())
                    <div class="nicdark_float_right nicdark_width_100  nicdark_position_relative nicdark_height_25 nicdark_display_none_all_responsive">
                    
                    <a href="{{route('review.subscriptions')}}">
                        <img alt="" class="nicdark_opacity_05_hover nicdark_transition_all_08_ease nicdark_position_absolute nicdark_top_3_negative nicdark_left_0 nicdark_margin_left_20" width="25" src="{{asset('img/education_theme/icons/icon-cart-white.svg')}}">
                    </a>

                    <a class="nicdark_bg_orange nicdark_color_white nicdark_padding_5 nicdark_border_radius_100_percentage nicdark_font_size_8 nicdark_line_height_5 nicdark_position_absolute nicdark_left_0 nicdark_top_10_negative nicdark_margin_left_40" href="#">{{Cart::count()}}</a>

                    {{-- <a class="nicdark_navigation_2_open_search_content" href="#"><img alt="" class="nicdark_opacity_05_hover nicdark_transition_all_08_ease nicdark_position_absolute nicdark_top_3_negative nicdark_right_0" width="25" src="{{asset('img/education_theme/icons/icon-search-white.svg')}}"></a> --}}

                </div>
                @endif
                
                <!--right icons menu-->

                {{--Top Menu Links--}}
                <div class="nicdark_navigation_2 nicdark_text_align_right nicdark_float_right nicdark_display_none_all_responsive">
                    @include('layouts.partials.top_nav_menu')

                </div> 

                <div class="nicdark_section nicdark_height_10"></div> 
                
            </div>

            <!--RESPONSIVE-->
            <div class="nicdark_width_50_percentage nicdark_text_align_center_all_iphone nicdark_width_100_percentage_all_iphone nicdark_float_left nicdark_display_none nicdark_display_block_responsive">
                <div class="nicdark_section nicdark_height_20"></div>
                {{-- <a href="{{ url('/') }}"><img alt="" width="170" class="" src="{{asset('img/education_theme/logos/logo-foodlab-white.svg')}}"></a>    --}}
                <a class="navbar-brand" href="{{ url('/') }}">
                    <span style="font-family: Yeseva One, cursive; font-size: 40px !important">
                        <span style="color:white">Exam</span><b><span style="color:darkred">STASH</span></b>
                    </span>
                    
                </a>
            </div>
            <div class="nicdark_width_50_percentage nicdark_width_100_percentage_all_iphone nicdark_float_left nicdark_display_none nicdark_display_block_responsive">
                <div class="nicdark_section nicdark_height_20"></div>
                <div class="nicdark_float_right nicdark_width_100_percentage nicdark_text_align_right nicdark_text_align_center_all_iphone">
                    
                    
                    <a class="nicdark_open_navigation_2_sidebar_content" href="#">
                        <img alt="" class="nicdark_margin_right_20" width="25" src="{{asset('img/education_theme/icons/icon-menu-white.svg')}}">
                    </a>

                    {{-- <div class="nicdark_position_relative nicdark_display_inline_block">
                        <a href="{{route('cart.index')}}"><img alt="" width="25" src="{{asset('img/education_theme/icons/icon-cart-white.svg')}}"></a> 
                        <a class="nicdark_bg_orange nicdark_color_white nicdark_padding_5 nicdark_border_radius_100_percentage nicdark_font_size_8 nicdark_line_height_5 nicdark_position_absolute nicdark_left_0 nicdark_top_10_negative nicdark_margin_left_20" href="#">{{Cart::count()}}</a>
                    </div>

                    <img alt="" class="nicdark_margin_left_20 nicdark_navigation_2_open_search_content" width="25" src="{{asset('img/education_theme/icons/icon-search-white.svg')}}">  --}}
                </div>
            </div>
            <!--RESPONSIVE-->
        </div>
        <!--end container-->
    </div>
</div>