<div class="nicdark_section nicdark_background_size_cover nicdark_background_position_center_bottom" style="background-image:url(img/chemistry-1920x1080.jpg);">
    <div class="nicdark_section nicdark_bg_greydark_alpha_gradient_5">

        <!--start nicdark_container-->
        <div class="nicdark_container nicdark_clearfix">


            <div class="nicdark_section nicdark_height_50"></div>


            <div class="grid grid_12">

                    <h1 class="nicdark_font_size_50 nicdark_color_white"><strong>Features</strong></h1>{{-- 
                    <div class="nicdark_section nicdark_height_10"></div>
                    <h3 class=" nicdark_color_white">The Best In Our School</h3> --}}
                </div>

        </div>
        <!--END nicdark_container-->

            <!--start nicdark_container-->
            <div class="nicdark_container nicdark_clearfix nicdark_padding_20  nicdark_padding_botttom_0 nicdark_box_sizing_border_box">

            <div class="nicdark_width_100_percentage nicdark_position_relative nicdark_margin_bottom_105_negative nicdark_bg_grey nicdark_border_1_solid_greydark nicdark_float_left nicdark_padding_20 nicdark_padding_0_all_iphone nicdark_box_sizing_border_box">


                <!--START preview-->
                <div class="nicdark_width_33_percentage nicdark_width_100_percentage_responsive nicdark_float_left">

                    <div class="nicdark_section nicdark_padding_15 nicdark_padding_0_all_iphone nicdark_box_sizing_border_box">
                        
                        <!--start preview-->
                        <div class="nicdark_section nicdark_border_1_solid_grey nicdark_bg_white">
                            
                            <!--image-->
                            <div class="nicdark_section nicdark_position_relative">
                                
                                <img alt="" class="nicdark_section" src="{{asset('img/select_topics-740x416.png')}}">
                                {{-- 
                                <div class="nicdark_bg_greydark_alpha_gradient_2 nicdark_position_absolute nicdark_left_0 nicdark_height_100_percentage nicdark_width_100_percentage nicdark_padding_20 nicdark_box_sizing_border_box">

                                </div> --}}

                            </div>
                            <!--image-->


                            <div class="nicdark_section nicdark_padding_20 nicdark_box_sizing_border_box">
                                <div style="height: 180px">
                                    <h3>Customizable Exams</h3>
                                    <div class="nicdark_section nicdark_height_20"></div> 
                                    <p>Select the topics you'd like to focus on and an exam with 15-20 questions will be created.</p>
                                </div>
                            </div>
                        </div>
                        <!--start preview-->

                    </div> 

                </div>
                <!--END preview-->




                <!--START preview-->
                <div class="nicdark_width_33_percentage nicdark_width_100_percentage_responsive nicdark_float_left">

                    <div class="nicdark_section nicdark_padding_15 nicdark_padding_0_all_iphone nicdark_box_sizing_border_box">
                        
                        <!--start preview-->
                        <div class="nicdark_section nicdark_border_1_solid_grey nicdark_bg_white">
                            
                            <!--image-->
                            <div class="nicdark_section nicdark_position_relative">
                                
                                <img alt="" class="nicdark_section" src="{{asset('img/scale_math02-740x416.png')}}">

                                {{-- <div class="nicdark_bg_greydark_alpha_gradient_2 nicdark_position_absolute nicdark_left_0 nicdark_height_100_percentage nicdark_width_100_percentage nicdark_padding_20 nicdark_box_sizing_border_box">
                                </div> --}}

                            </div>
                            <!--image-->


                            <div class="nicdark_section nicdark_padding_20 nicdark_box_sizing_border_box">
                                
                                <div style="height: 180px">
                                    <h3>Scalable Formulas</h3>
                                    <div class="nicdark_section nicdark_height_20"></div> 
                                    <p>Clear math formulas with the ability to increase the size to improve visibility.</p>    
                                </div>
                                

                            </div>
                        </div>
                        <!--start preview-->

                    </div> 

                </div>
                <!--END preview-->


                <!--START preview-->
                <div class="nicdark_width_33_percentage nicdark_width_100_percentage_responsive nicdark_float_left">

                    <div class="nicdark_section nicdark_padding_15 nicdark_padding_0_all_iphone nicdark_box_sizing_border_box">
                        
                        <!--start preview-->
                        <div class="nicdark_section nicdark_border_1_solid_grey nicdark_bg_white">
                            
                            <!--image-->
                            <div class="nicdark_section nicdark_position_relative">
                                
                                <img alt="" class="nicdark_section" src="{{asset('img/analytics-740x416.png')}}">

                                {{-- <div class="nicdark_bg_greydark_alpha_gradient_2 nicdark_position_absolute nicdark_left_0 nicdark_height_100_percentage nicdark_width_100_percentage nicdark_padding_20 nicdark_box_sizing_border_box">
                                    
                                </div> --}}

                            </div>
                            <!--image-->


                            <div class="nicdark_section nicdark_padding_20 nicdark_box_sizing_border_box">
                                <div style="height: 180px">
                                    <h3>Analyze Performance</h3>
                                    <div class="nicdark_section nicdark_height_20"></div> 
                                    <p>View a breakdown of your performance in each topic to identify areas that need a little more work.
                                        <br>
                                        <small>Under Development</small>
                                    </p>
                                    
                                </div>
                            </div>

                        </div>
                        <!--start preview-->

                    </div> 

                </div>
                <!--END preview-->




            </div>


        </div>
        <!--end container-->

    </div>

</div>