
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
    @include('layouts.education_theme.partials.header')

    <body id="start_nicdark_framework">



        <!--START nicdark_site-->
        <div class="nicdark_site">

            <!--START nicdark_site_fullwidth-->
            <div class="nicdark_site_fullwidth nicdark_site_fullwidth_boxed nicdark_clearfix">

                <!--START search container-->
                @include('layouts.education_theme.partials.search_container')
                <!--END search container-->

                {{--Top Navigation--}}
                @include('layouts.education_theme.partials.navigation')
                       
                <div class="nicdark_section nicdark_background_size_cover nicdark_background_position_center_bottom" style="background-image:url({{asset('img/books01.jpeg')}});">

                    <div class="nicdark_section nicdark_bg_greydark_alpha_gradient">

                        <!--start nicdark_container-->
                        <div class="nicdark_container nicdark_clearfix">

                            <div class="nicdark_section nicdark_height_200"></div>

                            <div class="grid grid_12">

                                <strong class="nicdark_color_white nicdark_font_size_60 nicdark_first_font">@yield('page_title')</strong>

                                <div class="nicdark_section nicdark_height_20"></div>
                            </div>

                        </div>
                        <!--end container-->
                    </div>
                </div>

                <div class="nicdark_section nicdark_bg_grey nicdark_border_bottom_1_solid_grey">
                    <!--start nicdark_container-->
                    <div class="nicdark_container nicdark_clearfix">

                        <div class="grid grid_12">
                            {{-- <a href="#">Home</a>
                            <img alt="" class="nicdark_margin_left_10 nicdark_margin_right_10" width="10" src="img/icons/icon-next-grey.svg">
                            <a href="#">Account</a>
                            <img alt="" class="nicdark_margin_left_10 nicdark_margin_right_10" width="10" src="img/icons/icon-next-grey.svg">
                            <a href="#">Jane</a> --}}
                        </div>
                    </div>
                    <!--end container-->
                </div>

                {{--Display Alerts & Error Flash Messages--}}
                @include('layouts.alerts')
                
                {{-- <div class="nicdark_section nicdark_height_50"></div> --}}

                <div style="min-height:850px">
                    
                    <div class="container">
                        @yield('content')        
                    </div>
                    
                </div>
                

                <div class="nicdark_section nicdark_height_40"></div>   

                @include('layouts.education_theme.partials.footer')
            </div>
        </div>

        <!--js-->
        @yield('scripts')
        <script src="{{asset('js/education_theme/nicdark_plugins.js')}}" type="text/javascript"></script>
        {{--Bootstrap--}}
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
    </body>  
</html>