
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
    @include('layouts.education_theme.partials.header')

    <body id="start_nicdark_framework">

        <!--START nicdark_site-->
        <div class="nicdark_site">

            <!--START nicdark_site_fullwidth-->
            <div class="nicdark_site_fullwidth nicdark_site_fullwidth_boxed nicdark_clearfix">

                <!--START search container-->
                @include('layouts.education_theme.partials.search_container')
                <!--END search container-->


                <!--START menu responsive-->
                @include('layouts.education_theme.partials.navigation')
                <!--END menu responsive-->

                <div class="nicdark_section nicdark_background_size_cover nicdark_background_position_center" style="background-image:url({{asset('img/student_chalkboard_tablet-1170x781.jpg')}});">

                    <div class="nicdark_section nicdark_bg_greydark_alpha_gradient_2">

                        
                        <div class="nicdark_section nicdark_height_570"></div>

                        <!--start nicdark_container-->
                        <div class="nicdark_container nicdark_clearfix nicdark_display_none_all_iphone">


                            <div class="grid grid_12">

                                
                                
                                <strong class="nicdark_color_white nicdark_font_size_60 nicdark_first_font">Get Ready For The Next Test</strong>

                                <!--START typed words-->
                                <div class="nicdark_section ">
                                    

                                    <strong class="nicdark_color_white nicdark_font_size_40 nicdark_first_font"> take </strong>

                                    <div class="nicdark_typed_strings">

                                        <p><strong class="nicdark_color_white nicdark_font_size_40 nicdark_first_font">online self-grading exams</strong></p>
                                        <p><strong class="nicdark_color_white nicdark_font_size_40 nicdark_first_font">customized exams</strong></p>

                                    </div>
                                    <span class="nicdark_typed nicdark_padding_botttom_5" style="white-space:pre;"></span>
                                
                                </div>
                                <!--END typed words-->

                            </div>

                        </div>
                        <!--end container-->


                        <div class="nicdark_section nicdark_height_50"></div>


                    </div>

                </div>

                {{-- <div class="nicdark_section nicdark_height_50"></div> --}}

                {{-- <div class="nicdark_container nicdark_clearfix">
                    <div class="grid grid_12 nicdark_text_align_right nicdark_text_align_center_responsive">
                        <h1 class="nicdark_font_size_40 nicdark_line_height_50 nicdark_padding_10"><strong>
                        <center>Need Practice Exams?
                        <br>
                        Hundreds Of Problems For College Courses.
                        </strong></h1>
                    </div>
                </div> --}}

                <div class="nicdark_section nicdark_height_50"></div>
                <div class="nicdark_container nicdark_clearfix">

                    <div class="grid grid_4 ">
                        <img alt="" width="50" src="{{asset('img/education_theme/icons/icon-course-grey.svg')}}">
                        <div class="nicdark_section nicdark_height_20"></div>
                        <h2><strong>Extensive Exam Bank</strong></h2>
                        <div class="nicdark_section nicdark_height_20"></div>
                        <p>Tons of multipe choice problems for college level courses. </p>
                    </div>

                    <div class="grid grid_4 ">
                        <img alt="" width="50" src="{{asset('img/education_theme/icons/icon-award-color.svg')}}">
                        <div class="nicdark_section nicdark_height_20"></div>
                        <h2><strong>Customizable Exams</strong></h2>
                        <div class="nicdark_section nicdark_height_20"></div>
                        <p>Create exams focused upcoming exam topics or on your weak areas. </p>
                    </div>

                    <div class="grid grid_4 ">
                    
                        <img alt="" width="50" src="{{asset('img/education_theme/icons/icon-graph-color.svg')}}">
                        <div class="nicdark_section nicdark_height_20"></div>
                        <h2><strong>Improved Grades</strong></h2>
                        <div class="nicdark_section nicdark_height_20"></div>
                        <p>Practice makes perfect!  The best way to master a concept is to practice, practice, practice.  </p>
                    </div>              
                </div>

                <div class="nicdark_section nicdark_height_50"></div>

                

                @include('layouts.education_theme.partials.available_courses')

                

                @include('layouts.education_theme.partials.features_parallax')

                <div class="nicdark_section nicdark_height_150"></div>
                {{-- <div class="nicdark_container nicdark_clearfix">
                    <h2 class="nicdark_font_size_50"><b>Subscriptions Packages</b></h2>
                </div>
                @include('layouts.education_theme.partials.pricing_table')
                <div class="nicdark_section nicdark_height_20"></div> --}}

                {{-- <center><div class="nicdark_width_20_percentage  nicdark_box_sizing_border_box nicdark_float_left">
                    <a class="nicdark_display_inline_block nicdark_text_align_center nicdark_box_sizing_border_box nicdark_width_100_percentage nicdark_color_white nicdark_bg_greydark nicdark_first_font nicdark_padding_10_20 nicdark_border_radius_3 " href="{{route('purchase.select.packages')}}" style="color:white"><b>Get Started!</b></a>   
                </div></center> --}}
                <center><a href="{{route('purchase.select.packages')}}" class="btn btn-lg btn-danger nicdark_display_inline_block nicdark_text_align_center nicdark_box_sizing_border_box nicdark_width_50_percentage nicdark_color_white nicdark_bg_greydark nicdark_first_font nicdark_padding_10_20 nicdark_border_radius_3 "> <b>Get Started </b></a></center>
                <div class="nicdark_section nicdark_height_50"></div>

                <div class="nicdark_container nicdark_clearfix">
                    <h2 class="nicdark_font_size_50"><b>Tutor Proficiency Exams</b></h2>
                    <div class="grid grid_6">
                        <img src="{{asset('img/female_tutor_frustrated_student-1170x781.jpg')}}" style="max-width: 100%"> 
                    </div>
                    <div class="grid grid_6">
                        <center><h3 style="font-family: serif"><b>All Tutors Say They're The Best ... Are They?</b></h3></center>
                        

                        <center class="nicdark_font_size_20" style="font-family: serif">Make tutors prove they know their stuff before you meet with them.</center>

                        <center><h3><a href="{{route('proficiency.exams.info')}}" class="btn btn-lg btn-primary" style="font-family: serif"><b>Create Proficiency Exams</b></a></h3></center>

                        <center><h3 style="font-family: serif"><b>Free For Students</b></h3></center>
                        <center><em style="font-family: serif; font-size: 18px">Don't waste your time and money on horrible tutors!</em></center>
                    </div>
                </div>                

                <div class="nicdark_section nicdark_height_50"></div>

                {{--Partners Logo--}}
                <div class="nicdark_section">
                    <div class="nicdark_section nicdark_bg_grey nicdark_border_top_1_solid_grey">

                        <!--start nicdark_container-->
                        <div class="nicdark_container nicdark_clearfix">

                            <div class="nicdark_section nicdark_height_30"></div>
                            {{-- <div class="grid grid_2">
                                <img alt="" class="nicdark_width_100_percentage" src="img/education_theme/partner/logo1.png">  
                            </div>
                            <div class="grid grid_2">
                                <img alt="" class="nicdark_width_100_percentage" src="img/education_theme/partner/logo4.png">  
                            </div>
                            <div class="grid grid_2">
                                <img alt="" class="nicdark_width_100_percentage" src="img/education_theme/partner/logo3.png">  
                            </div>
                            <div class="grid grid_2">
                                <img alt="" class="nicdark_width_100_percentage" src="img/education_theme/partner/logo2.png">  
                            </div>
                            <div class="grid grid_2">
                                <img alt="" class="nicdark_width_100_percentage" src="img/education_theme/partner/logo5.png">  
                            </div>
                            <div class="grid grid_2">
                                <img alt="" class="nicdark_width_100_percentage" src="img/education_theme/partner/logo6.png">  
                            </div>
                             --}}

                            <div class="nicdark_section nicdark_height_30"></div>

                    
                        </div>
                        <!--end container-->

                    </div>
                </div>

                @include('layouts.education_theme.partials.footer')
            </div>
        <!--END nicdark_site_fullwidth-->

        </div>
        <!--END nicdark_site-->
        
        <!--js-->
        @yield('scripts')
        
        <script src="{{asset('js/education_theme/nicdark_plugins.js')}}" type="text/javascript"></script>
        {{--Bootstrap--}}
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
    </body>  
</html>