<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
    @include('layouts.education_theme.partials.header')

    <body id="start_nicdark_framework">

        <!--START nicdark_site-->
        <div class="nicdark_site"  style="background-color: grey">
            <div class="nicdark_section nicdark_height_50"></div>   
            <!--START nicdark_site_fullwidth-->
            <div class="nicdark_site_fullwidth nicdark_site_fullwidth_boxed nicdark_clearfix">

                {{--Display Alerts & Error Flash Messages--}}
                @include('layouts.alerts')
                
                {{-- <div class="nicdark_section nicdark_height_50"></div> --}}

                <div style="min-height:850px">
                    
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="jumbotron"  style="min-height: 830px">
                                    
                                    <a class="navbar-brand pull-right" href="{{ url('/') }}">
                                        <span style="font-family: Yeseva One, cursive; font-size: 40px !important">
                                            <span style="color:gray">Exam</span><b><span style="color:darkred">STASH</span></b>
                                        </span>
                                    </a>
                                    @yield('content')        
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                

                <div class="nicdark_section nicdark_height_40"></div>   

                {{-- @include('layouts.education_theme.partials.footer') --}}
            </div>
        </div>

        <!--js-->
        @yield('scripts')
        <script src="{{asset('js/education_theme/nicdark_plugins.js')}}" type="text/javascript"></script>
        {{--Bootstrap--}}
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
    </body>  
</html>