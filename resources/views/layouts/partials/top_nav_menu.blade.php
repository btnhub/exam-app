<ul>
    {{--
    <li>
        <a href="{{url('/')}}">HOME</a>
    </li>
    <li>
        <a href="#">PRICING</a>
    </li> --}}
    <li>
        <a href="{{route('purchase.select.packages')}}"><b>EXAMS</b></a>
    </li>
    
    <li>
        <a href="{{route('proficiency.exams.info')}}"><b>PROFICIENCY TESTS</b></a>
    </li>

    <li>
        <a href="{{url('/home')}}"><b>MY ACCOUNT</b></a>
    </li>
    @if (Auth::user() && Auth::user()->isAdmin())
        <li>
            <a href="#"><b>Admin</b></a>

            <ul class="nicdark_sub_menu">
                <li><a href="{{route('users.index')}}">Users</a></li>
                <li><a href="{{route('exam.upload.progress')}}">Exam Upload</a></li>
                <li><a href="{{route('select-exam')}}">View Exams</a></li>
                <li><a href="{{route('exam.upload.progress')}}">Upload Exam</a></li>
                <li><a href="{{route('proficiency.exams.list')}}">Proficiency Exams</a></li>
            </ul>

        </li>
    @endif
</ul>