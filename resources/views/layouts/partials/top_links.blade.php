<ul>
    @if (Auth::guest())
        <li>
            <img alt="" class="nicdark_margin_right_10 nicdark_float_left" width="15" src="{{asset('img/education_theme/icons/icon-user-grey.svg')}}">
            <a href="{{ route('login') }}" style="color:white">LOGIN</a>
        </li>
        <li>
            <img alt="" class="nicdark_margin_right_10 nicdark_float_left" width="15" src="{{asset('img/education_theme/icons/icon-login-grey.svg')}}">
            <a href="{{ route('register') }}" style="color:white">REGISTER</a>
        </li>

    @else
        <li><a href="{{url('/home')}}" style="color:white">Hi, {{Auth::user()->first_name}}</a></li>  
        <li>
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();" style="color:white">
                LOGOUT
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>  
    @endif
    
</ul>