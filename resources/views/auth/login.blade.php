@extends('layouts.education_theme.template')

@section('content')
<div class="col-md-6 col-md-offset-3">
    <div class="nicdark_section nicdark_box_sizing_border_box">
        <h2><strong>Login :</strong></h2> 
        <span class="pull-right">(Don't have an account?  <a href="{{route('register')}}">Register Here</a>)</span>
        <div class="nicdark_section nicdark_height_20"></div>
    </div>

    <div class="nicdark_section nicdark_box_sizing_border_box">
        <div class="nicdark_section">
        
            {!! Form::open(['method' => 'POST', 'route' => 'login']) !!}
            
                <div class="nicdark_section">
                    <div class="nicdark_width_100_percentage nicdark_box_sizing_border_box nicdark_float_left nicdark_position_relative">
                        <input class=" nicdark_border_width_2 nicdark_background_none nicdark_border_top_width_0 nicdark_border_right_width_0 nicdark_border_left_width_0" placeholder="Email" id="email" type="email" name="email" value="{{ old('email') }}" autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block" style="color:darkred">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="nicdark_section nicdark_height_20"></div>
                    <div class="nicdark_width_100_percentage nicdark_box_sizing_border_box nicdark_float_left nicdark_position_relative">
                        <input class=" nicdark_border_width_2 nicdark_background_none nicdark_border_top_width_0 nicdark_border_right_width_0 nicdark_border_left_width_0" placeholder="Password" id="password" type="password" name="password">
                        @if ($errors->has('password'))
                            <span class="help-block" style="color:darkred">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    
                </div>
                <div class="nicdark_section nicdark_height_20"></div>    
                <div class="btn-group pull-right">
                    {!! Form::submit("LOGIN", ['class' => "nicdark_display_inline_block nicdark_text_align_center nicdark_box_sizing_border_box nicdark_color_white nicdark_bg_green nicdark_first_font nicdark_padding_10_20 nicdark_border_radius_3 "]) !!}
                </div>
            
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
