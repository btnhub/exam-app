@extends('layouts.education_theme.template')

@section('content')

<div class="col-md-6 col-md-offset-3">
    <div class="nicdark_section nicdark_box_sizing_border_box">
        <h2><strong>Register :</strong></h2> 
        <div class="nicdark_section nicdark_height_20"></div>
    </div>


    <div class="nicdark_section nicdark_box_sizing_border_box">
        <div class="nicdark_section">
        
            {!! Form::open(['method' => 'POST', 'route' => 'register']) !!}
            
                <div class="nicdark_section">

                    <div class="nicdark_width_100_percentage nicdark_box_sizing_border_box nicdark_float_left nicdark_position_relative">
                        <input class=" nicdark_border_width_2 nicdark_background_none nicdark_border_top_width_0 nicdark_border_right_width_0 nicdark_border_left_width_0" type="text" placeholder="First Name*" id="first_name" name="first_name" value="{{ old('first_name') }}" autofocus>
                        <span class="help-block" style="color:darkred">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    </div>
                    
                    <div class="nicdark_section nicdark_height_20"></div>
                    
                    <div class="nicdark_width_100_percentage nicdark_box_sizing_border_box nicdark_float_left nicdark_position_relative">
                        <input class=" nicdark_border_width_2 nicdark_background_none nicdark_border_top_width_0 nicdark_border_right_width_0 nicdark_border_left_width_0" type="text" placeholder="Last Name*" id="last_name" name="last_name" value="{{ old('last_name') }}">
                        <span class="help-block" style="color:darkred">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    </div>
                    
                    <div class="nicdark_section nicdark_height_20"></div>

                    <div class="nicdark_width_100_percentage nicdark_box_sizing_border_box nicdark_float_left nicdark_position_relative">
                        <input class=" nicdark_border_width_2 nicdark_background_none nicdark_border_top_width_0 nicdark_border_right_width_0 nicdark_border_left_width_0" placeholder="Email*" id="email" type="email" name="email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block" style="color:darkred">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="nicdark_section nicdark_height_20"></div>
                    <div class="nicdark_width_100_percentage nicdark_box_sizing_border_box nicdark_float_left nicdark_position_relative">
                        <input class=" nicdark_border_width_2 nicdark_background_none nicdark_border_top_width_0 nicdark_border_right_width_0 nicdark_border_left_width_0" placeholder="Password*" id="password" type="password" name="password">
                        @if ($errors->has('password'))
                            <span class="help-block" style="color:darkred">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="nicdark_section nicdark_height_20"></div>

                    <div class="nicdark_width_100_percentage nicdark_box_sizing_border_box nicdark_float_left nicdark_position_relative">
                        <input class=" nicdark_border_width_2 nicdark_background_none nicdark_border_top_width_0 nicdark_border_right_width_0 nicdark_border_left_width_0" placeholder="Confirm Password" id="password-confirm" type="password" name="password_confirmation" >
                    </div>

                    <div class="nicdark_section nicdark_height_20"></div>
                    
                    <div class="nicdark_width_100_percentage nicdark_box_sizing_border_box nicdark_float_left nicdark_position_relative">
                        <input class=" nicdark_border_width_2 nicdark_background_none nicdark_border_top_width_0 nicdark_border_right_width_0 nicdark_border_left_width_0" type="text" placeholder="School" id="school" name="school" value="{{ old('school') }}">
                        <span class="help-block" style="color:darkred">
                            <strong>{{ $errors->first('school') }}</strong>
                        </span>
                    </div>

                    <div class="nicdark_section nicdark_height_20"></div>
                    
                    <div class="nicdark_width_100_percentage nicdark_box_sizing_border_box nicdark_float_left nicdark_position_relative">
                        <input class=" nicdark_border_width_2 nicdark_background_none nicdark_border_top_width_0 nicdark_border_right_width_0 nicdark_border_left_width_0" type="text" placeholder="City" id="city" name="city" value="{{ old('city') }}">
                        <span class="help-block" style="color:darkred">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                    </div>

                    <div class="nicdark_section nicdark_height_20"></div>
                    
                    <div class="nicdark_width_100_percentage nicdark_box_sizing_border_box nicdark_float_left nicdark_position_relative">
                        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            {{-- <label for="state" class="col-md-4 control-label">State</label> --}}
                            <div class="col-md-6">
                                {!! Form::select('state', array_merge(['' => 'Select A State'], config('codes_states')), null, ['id' => 'state', 'class' => 'form-control col-md-4 control-label']) !!}
                                <small class="text-danger">{{ $errors->first('state') }}</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nicdark_section nicdark_height_20"></div>    
                <div class="btn-group pull-right">
                    {!! Form::submit("REGISTER", ['class' => "nicdark_display_inline_block nicdark_text_align_center nicdark_box_sizing_border_box nicdark_color_white nicdark_bg_green nicdark_first_font nicdark_padding_10_20 nicdark_border_radius_3 "]) !!}
                </div>
            
            {!! Form::close() !!}
            
        </div>
    </div>
</div>
@endsection
