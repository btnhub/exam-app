@extends('layouts.education_theme.template')

@section('content')
<div class="container">
	<div class="row">
        <div class="col-md-12">
            <h2>Content Creation</h2>
            <table class="table">
                <thead>
                    <tr>
                        <th>Course</th>
                        <th>Course Number</th>
                        <th>Exams</th>                          
                        <th>Completed</th>
                        <th>Reviewed</th>
                        <th># Of Questions Submitted</th>
                        <th># Questions Total</th>
                    </tr>
                </thead>
                <tbody>                    
                    @foreach ($user->exams_uploaded() as $exam)
                        <tr>
                            <td>{{$exam->course->course_name}}</td>
                            <td>{{$exam->course->course_number}}</td>
                            <td>
                                @if (!$user->isAdmin() && $exam->reviewed)
                                    {{ucfirst($exam->semester->semester)}} {{$exam->year}} Exam #{{$exam->exam_number}}
                                @else
                                    <a href="{{route('load-exam', $exam->id)}}">{{ucfirst($exam->semester->semester)}} {{$exam->year}} Exam #{{$exam->exam_number}} </a>
                                @endif
                                
                            </td>
                            <td>{!!$exam->completed ? 'Yes' : "<span style='color:darkred'>No</span>"!!}</td>
                            <td>{!!$exam->reviewed ? "<span style='color:darkgreen'>Yes</span>" : "<span style='color:darkred'>No</span>"!!}</td>

                            <td>{{$exam->questions->where('submitted_by', $user->id)->count()}}</td>
                            <td>{{$exam->questions->count()}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>{{$user->questions->count()}} Questions Submitted</b></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>Create A New Exam</h2>
            {!! Form::open(['method' => 'POST', 'route' => 'save.exam', 'class' => 'form-horizontal']) !!}

                @include('btn.exams.form_partial', ['courses' => $courses_array])
                
                
                <div class="pull-left col-md-offset-1 col-sm-offset-1">
                <br>    {!! Form::submit("Create Exam", ['class' => 'btn btn-success']) !!}    
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2>Complete An Existing Exam</h2>
                
                {{count($incomplete_exams)}} Uncompleted Exams.  <br>
            <table class="table striped hover" border="1">
                <thead>
                    <tr>
                        <th>Exam ID</th>
                        <th>Course</th>
                        <th>Exam Details</th>
                        <th># of Questions</th>
                        <th>School</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($incomplete_exams as $exam)
                        <tr>
                            <td>{{$exam->id}}</td>
                            <td>
                                {{$exam->course->course_name}}  ({{$exam->course->course_number}})
                            </td>
                            <td>
                                <a href="{{route('load-exam', $exam->id)}}">
                                    @if ($exam->exam_number)
                                        Exam #{{$exam->exam_number}}
                                    @elseif ($exam->quiz)
                                        Quiz #{{$exam->quiz}}
                                    @elseif ($exam->homework)
                                        Homework #{{$exam->homework}}
                                    @endif
                                    
                                    {{ucfirst($exam->semester->semester)}} {{$exam->year}}
                                </a>
                            </td>
                            <td>{{$exam->questions->count()}}</td>
                            <td>{{$exam->course->school->name}} ({{$exam->course->school->school_code}})</td>           
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
    </div>
</div>

@endsection