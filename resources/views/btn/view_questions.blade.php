@extends('layouts.education_theme.template')
@section('scripts')
    @include('btn.partials.mathjax')
@endsection

@section('content')
    <a href="{{route('select-exam')}}"><-- Back To Exam List</a>
    <h2>{{$course->course_name}}</h2>
    {{count($course->questions())}} Questions

    {!! Form::open(['method' => 'POST', 'route' => ['add.topics', $course->id], 'class' => 'form-horizontal']) !!}
    
        <ol>
            @foreach ($course->questions() as $question)
                <li>
                    {!!$question->question!!}
                    @if ($question->question_image)
                        <br>
                        <img src='{{asset("btn/exam_images/questions/{$question->question_image}")}}'>
                        <br>
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <ol type='A'>
                                @foreach ($question->answers as $ans)
                                    @if ($ans->correct_answer == 1)
                                        <span style='color:red'><li>{{$ans->answer}}</li></span>
                                    @else
                                        <li>{{$ans->answer}}</li>
                                    @endif
                                @endforeach
                            </ol>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="checkbox{{ $errors->has("advanced[$question->id]") ? ' has-error' : '' }}">
                                    <label>
                                        {!! Form::checkbox("advanced[$question->id]",1, $question->advanced, ['id' => "advanced[$question->id]"]) !!} Advanced (e.g. calc-based)
                                    </label>
                                </div>
                                <small class="text-danger">{{ $errors->first("advanced[$question->id]") }}</small>
                            </div>

                            <br>

                            <div class="form-group{{ $errors->has("level[$question->id]") ? ' has-error' : '' }}">
                                {!! Form::label("level[$question->id]", 'Level') !!}
                                {!! Form::select("level[$question->id]",$levels, $question->level, ['id' => "level[$question->id]", 'class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first("level[$question->id]") }}</small>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            @if ($topics)
                                <div class='form-group{{ $errors->has("topic_ids[$question->id][]") ? " has-error" : "" }}'>
                                    {!! Form::label("topic_ids[$question->id][]", 'Topics') !!}
                                    {!! Form::select("topic_ids[$question->id][]",$topics, $question->topics->pluck('id'), ['class' => 'form-control', 'multiple', 'size' => 10]) !!}

                                    @if ($question->topics->count())
                                        <small><b style="color:darkgreen">Topics: {{$question->topics->implode('topic', ', ')}}</b></small>
                                    @else
                                        <small><b style="color:red">No Topic Provided</b></small>
                                    @endif
                                    
                                    <small class="text-danger">{{ $errors->first('topic_ids[]') }}</small>
                                </div>
                            @endif
                        </div>    
                    </div>
                    
                </li>
                <b>{{$question->exam->details()}}, Question {{$question->question_number}}</b>
                <a href="{{route('show-question', ['ref_id' => $question->ref_id])}}"><button type="button" class="btn btn-primary">Edit</button></a>
                <button type="button" class="btn btn-danger">Delete (TO DO)</button>
                <hr>
            @endforeach
        </ol>
    
        <div class="btn-group pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
        </div>
    
    {!! Form::close() !!}

@endsection