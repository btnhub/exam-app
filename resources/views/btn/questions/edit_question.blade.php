@extends('layouts.education_theme.template')
@section('scripts')
    @include('btn.partials.mathjax')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>
        tinymce.init({ 
            selector:'textarea',
            plugins: 'table lists link code hr textcolor image',
             toolbar: "newdocument | undo  redo | removeformat | bold  italic  underline strikethrough | alignleft aligncenter alignright alignjustify | table | image | bullist numlist | outdent  indent | blockquote | subscript  superscript | link | code | hr | forecolor backcolor | styleselect formatselect fontselect fontsizeselect",
             menubar: false
        });
    </script>
@endsection

@section('content')
    <a href="{{route('load-exam', $question->exam->id)}}"><-- Back To Exam</a>

    <h2>Edit Question</h2>
    <h2>{{$question->exam->course->school->school}}</h2>
    <h3>{{$question->exam->details()}}</h3>

    <a href="{{route('create.question', $question->exam->id)}}"><button type="button" class="btn btn-primary pull-right">Add Another Question</button></a>
    <h4>Question Number: {{$question->question_number}} (ID: {{$question->id}})</h4>
    
    {!!$question->question!!}
    @if ($question->question_image)
        <br>
        <img src='{{asset("btn/exam_images/questions/{$question->question_image}")}}'>
        <br>
    @endif

    <ol type='A'>
        @foreach ($question->answers as $ans)
            @if ($ans->correct_answer == 1)
                <span style='color:red'><li>{!!$ans->answer!!}</li></span>
            @else
                <li>{!!$ans->answer!!}</li>
            @endif
        @endforeach
    </ol>

    <hr>

    {!! Form::model($question, ['method' => 'POST', 'route' =>[ 'update.question', $question->ref_id], 'class' => 'form-horizontal', 'files' => true]) !!}
    	
        @include('btn.questions.form_partial')

        
        <div class="btn-group pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
            {!! Form::submit("Update Question", ['class' => 'btn btn-success']) !!}
        </div>
    {!! Form::close() !!}
@endsection