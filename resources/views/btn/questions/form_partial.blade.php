    <h3>Question Details</h3>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group{{ $errors->has('exam_id') ? ' has-error' : '' }}">
                {!! Form::label('exam_id', 'Exam') !!}
                {!! Form::select('exam_id', $exams, null, ['id' => 'exam_id', 'class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('exam_id') }}</small>
            </div>  

            <br>

            <div class="form-group{{ $errors->has('question_number') ? ' has-error' : '' }}">
                {!! Form::label('question_number', 'Question_Number') !!}
                {!! Form::number('question_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('question_number') }}</small>
            </div> 
            
            @if (auth()->user()->isAdmin())
                    <div class="form-group">
                        <div class="checkbox{{ $errors->has("advanced") ? ' has-error' : '' }}">
                            <label>
                                {!! Form::checkbox("advanced",1, $question->advanced, ['id' => "advanced"]) !!} Advanced (e.g. calc-based)
                            </label>
                        </div>
                        <small class="text-danger">{{ $errors->first("advanced") }}</small>
                    </div>

                    <div class="form-group{{ $errors->has("level") ? ' has-error' : '' }}">
                        {!! Form::label("level", 'Level') !!}
                        {!! Form::select("level",$levels, $question->level, ['id' => "level", 'class' => 'form-control']) !!}
                        <small class="text-danger">{{ $errors->first("level") }}</small>
                    </div>  
                </div>

                <div class="col-md-3 col-md-offset-1">
                    <div class="form-group{{ $errors->has('topic_ids[]') ? ' has-error' : '' }}">
                        {!! Form::label('topic_ids[]', 'Topics') !!}
                        {!! Form::select('topic_ids[]',$topics, $question->topics->pluck('id'), ['id' => 'topic_ids[]', 'class' => 'form-control', 'multiple', 'size' => 10]) !!}
                        @if ($question->topics->count())
                            <small><b style="color:red">Topics: {{$question->topics->implode('topic', ', ')}}</b></small>
                        @endif
                        
                        <small class="text-danger">{{ $errors->first('topic_ids[]') }}</small>
                    </div>
                    
                </div>
                <div class="col-md-2">
                    <b>Randomize Answers</b>
                    <div class="radio{{ $errors->has('random_ans') ? ' has-error' : '' }}">
                        <label>
                            {!! Form::radio('random_ans', 1,  null) !!} Yes &nbsp;&nbsp;&nbsp;
                        </label>
                        <label>
                            {!! Form::radio('random_ans', 0,  null) !!} No
                        </label>
                        <small class="text-danger">{{ $errors->first('random_ans') }}</small>
                    </div>

                    <br>

                    <b>Free Preview</b>
                    <div class="radio{{ $errors->has('free_preview') ? ' has-error' : '' }}">
                        <label>
                            {!! Form::radio('free_preview', 1,  null) !!} Yes &nbsp;&nbsp;&nbsp;
                        </label>
                        <label>
                            {!! Form::radio('free_preview', 0,  null) !!} No
                        </label>

                        <small class="text-danger">{{ $errors->first('free_preview') }}</small>
                    </div>

                    <br>

                    <b>Applicant Exam</b>
                    <div class="radio{{ $errors->has('applicant_exam') ? ' has-error' : '' }}">
                        <label>
                            {!! Form::radio('applicant_exam', 1,  null) !!} Yes &nbsp;&nbsp;&nbsp;
                        </label>
                        <label>
                            {!! Form::radio('applicant_exam', 0,  null) !!} No
                        </label>
                        <small class="text-danger">{{ $errors->first('applicant_exam') }}</small>
                    </div>

                    <br>

                    <b>Problem</b>
                    <div class="radio{{ $errors->has('problem') ? ' has-error' : '' }}">
                        <label>
                            {!! Form::radio('problem', 1, null) !!} Yes &nbsp;&nbsp;&nbsp;
                        </label>
                        <label>
                            {!! Form::radio('problem', 0, null) !!} No
                        </label>
                        <small class="text-danger">{{ $errors->first('problem') }}</small>
                    </div>
                </div>
                <div class=" col-md-4">
                    <div class="form-group{{ $errors->has('complaint') ? ' has-error' : '' }}">
                        {!! Form::label('complaint', 'Complaint') !!}
                        {!! Form::textarea('complaint', null, ['class' => 'form-control', 'size' => '100x4']) !!}
                        <small class="text-danger">{{ $errors->first('complaint') }}</small>
                    </div>
                </div>
            @else
                </div>
            @endif
            
    </div>


    {{-- <div class="row">
        <div class="col-md-offset-1 col-md-5">
            <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
                {!! Form::label('notes', 'Notes') !!}
                {!! Form::textarea('notes', null, ['class' => 'form-control', 'size' => '100x4']) !!}
                <small class="text-danger">{{ $errors->first('notes') }}</small>
            </div>    
        </div>
    </div> --}}
    
    
    <hr>
    <a href="http://www.wiris.com/editor/demo/en/mathml-latex" target="_blank">Click here to create math equations</a>.  Copy the "LaTeX" code into the question statement below. If the equation editor does not show up, try opening the link <b>incognito</b>.<br>
    
	<div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
	    {!! Form::label('question', 'Question Statement') !!}<br>
	    {!! Form::textarea('question', null, ['class' => 'form-control', 'size' => '100x4']) !!}
	    <small class="text-danger">{{ $errors->first('question') }}</small>
	</div>

    <div class="form-group{{ $errors->has('question_image') ? ' has-error' : '' }}">
        {!! Form::label('question_image', 'Question Image') !!}
        {!! Form::file('question_image') !!}
        <small class="text-danger">{{ $errors->first('question_image') }}</small>
    </div>

    <b>Answers</b>
    <?php 
        $answer_count = 0;
        $alphabet = range('A','Z');
    ?>

    @foreach ($question->answers as $answer)
        <div class="row">
            <div class='form-group{{ $errors->has("answer[$answer->id]") ? ' has-error' : '' }} col-md-5'>
                {!! Form::text("answer[$answer->id]", $answer->answer, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first("answer[$answer->id]") }}</small>
            </div>
            
            <div class="radio{{ $errors->has('correct') ? ' has-error' : '' }} col-md-3 col-md-offset-1">
                <label>
                    {!! Form::radio('correct', $answer->id, $answer->correct_answer) !!} Correct Answer ({{$alphabet[$answer_count]}})
                </label>
                <small class="text-danger">{{ $errors->first('correct') }}</small>
            </div>
            <?php $answer_count++;?>
        </div>
    @endforeach

