@extends('layouts.education_theme.template')
@section('scripts')
    @include('btn.partials.mathjax')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>

    <script>
        tinymce.init({ 
            selector:'textarea',
            plugins: 'table lists link code hr textcolor image',
             toolbar: "newdocument | undo  redo | removeformat | bold  italic  underline strikethrough | alignleft aligncenter alignright alignjustify | table | image | bullist numlist | outdent  indent | blockquote | subscript  superscript | link | code | hr | forecolor backcolor | styleselect formatselect fontselect fontsizeselect",
             menubar: false
        });
    </script>
@endsection

@section('content')
    <a href="{{route('load-exam', $exam->id)}}"><-- Back To Exam</a>

    <h2>Add Question</h2>
    <h3>{{$exam->details()}}</h3>

    {!! Form::open(['method' => 'POST', 'route' => ['save.question', $exam->id], 'class' => 'form-horizontal', 'files' => true]) !!}
    
            <h3>Question Details</h3>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group{{ $errors->has('question_number') ? ' has-error' : '' }}">
                        {!! Form::label('question_number', 'Question_Number') !!}
                        {!! Form::number('question_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('question_number') }}</small>
                    </div>

                    @if (auth()->user()->isAdmin())
                             <div class="form-group">
                                <div class="checkbox{{ $errors->has("advanced") ? ' has-error' : '' }}">
                                    <label>
                                        {!! Form::checkbox("advanced",1, null, ['id' => "advanced"]) !!} Advanced (e.g. calc-based)
                                    </label>
                                </div>
                                <small class="text-danger">{{ $errors->first("advanced") }}</small>
                            </div>

                            <div class="form-group{{ $errors->has("level") ? ' has-error' : '' }}">
                                {!! Form::label("level", 'Level') !!}
                                {!! Form::select("level",$levels, null, ['id' => "level", 'class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first("level") }}</small>
                            </div>   
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group{{ $errors->has('topic_ids[]') ? ' has-error' : '' }}">
                                {!! Form::label('topic_ids[]', 'Topics') !!}
                                {!! Form::select('topic_ids[]',$topics, null, ['id' => 'topic_ids[]', 'class' => 'form-control', 'multiple', 'size' => 10]) !!}
                                
                                <small class="text-danger">{{ $errors->first('topic_ids[]') }}</small>
                            </div>    
                        </div>
                        <div class="col-md-3 col-md-offset-1">
                            <b>Randomize Answers</b>
                            <div class="radio{{ $errors->has('random_ans') ? ' has-error' : '' }}">
                                <label>
                                    {!! Form::radio('random_ans', 1,  null) !!} Yes &nbsp;&nbsp;&nbsp;
                                </label>
                                <label>
                                    {!! Form::radio('random_ans', 0,  1) !!} No
                                </label>
                                <small class="text-danger">{{ $errors->first('random_ans') }}</small>
                            </div>

                            <br>

                            <b>Free Preview</b>
                            <div class="radio{{ $errors->has('free_preview') ? ' has-error' : '' }}">
                                <label>
                                    {!! Form::radio('free_preview', 1,  null) !!} Yes &nbsp;&nbsp;&nbsp;
                                </label>
                                <label>
                                    {!! Form::radio('free_preview', 0,  1) !!} No
                                </label>

                                <small class="text-danger">{{ $errors->first('free_preview') }}</small>
                            </div>

                            <br>

                            <b>Applicant Exam</b>
                            <div class="radio{{ $errors->has('applicant_exam') ? ' has-error' : '' }}">
                                <label>
                                    {!! Form::radio('applicant_exam', 1,  null) !!} Yes &nbsp;&nbsp;&nbsp;
                                </label>
                                <label>
                                    {!! Form::radio('applicant_exam', 0,  1) !!} No
                                </label>
                                <small class="text-danger">{{ $errors->first('applicant_exam') }}</small>
                            </div>
                        </div>
                    @else
                        </div>
                    @endif 

                    
            </div>
            
            <hr>

            <a href="http://www.wiris.com/editor/demo/en/mathml-latex" target="_blank">Click here to create math equations</a>.  Copy the "LaTeX" code into the question statement below. If the equation editor does not show up, try opening the link <b>incognito</b>.<br>

            <div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
                {!! Form::label('question', 'Question Statement') !!}<br>
                {!! Form::textarea('question', null, ['class' => 'form-control', 'size' => '100x4', 'placeholder' => 'Be sure to surround the code with \(   \)  for inline equations and \[  \] for block (centered) equations.']) !!}
                <small class="text-danger">{{ $errors->first('question') }}</small>
            </div>

            <div class="form-group{{ $errors->has('question_image') ? ' has-error' : '' }}">
                {!! Form::label('question_image', 'Question Image') !!}
                {!! Form::file('question_image') !!}
                <small class="text-danger">{{ $errors->first('question_image') }}</small>
            </div>

            <b>Answers</b>
            <?php 
                $answer_count = 0;
                $alphabet = range('A','Z');
            ?>

            @for ($i = 0; $i < 6; $i++)
                <div class="row">
            
                    <div class="col-md-5">
                        <div class="form-group{{ $errors->has("answer[$i]") ? ' has-error' : '' }}">
                            {!! Form::text("answer[$i]", null, ['class' => 'form-control']) !!}
                            <small class="text-danger">{{ $errors->first("answer[$i]") }}</small>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <div class="radio{{ $errors->has('correct_answer') ? ' has-error' : '' }}">
                            <label>
                                {!! Form::radio('correct_answer',$i,null, ['required']) !!} Correct Answer ({{$alphabet[$answer_count]}})
                            </label>
                            <small class="text-danger">{{ $errors->first('correct_answer') }}</small>
                        </div>
                    </div>
                    <?php $answer_count++;?>
            
                </div>
            @endfor
    
        <div class="btn-group pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
            {!! Form::submit("Add Question", ['class' => 'btn btn-success']) !!}
        </div>
    
    {!! Form::close() !!}
@endsection