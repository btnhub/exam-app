@extends('layouts.education_theme.template')

@section('content')
	<h2>Users</h2>
	<b>{{$users->count()}} Users</b>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Contact</th>
				<th>Role</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $user)
				<tr>
					<td>{{$user->id}}</td>
					<td>{{$user->full_name}}</td>
					<td>{{$user->email}}
						<br> {{$user->phone}}
					</td>
					<td>{{ucwords($user->roles->implode('role', ', '))}}</td>
					<td><a href="{{route('clone.user', $user->id)}}" class="btn btn-primary">Login As {{$user->first_name}}</a></td>
				</tr>
			@endforeach
			
		</tbody>
	</table>
@endsection