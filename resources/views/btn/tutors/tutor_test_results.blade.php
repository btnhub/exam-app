@extends('layouts.education_theme.assessment_template')
@section('content')
    
    <h2 style="font-family:serif"><b>{{$tutor_test->proficiency_exam->name}} Proficiency Exam Results</b></h2>
       
    <div style="background-color: white; padding: 30px; border-radius: 25px; margin: 20px; min-height:500px">
        <div class="row">
            <br><br><br><br>
            
            <div class="col-sm-8 col-sm-offset-2">
                <center>
                    <h4><b>You Scored {{$tutor_candidate->grade()}}%</b></h4>
                    Passing grade: {{$tutor_candidate->grade_cutoff}}%

                    <br><br>
                    
                    We've e-mailed your results to {{$tutor_test->user->first_name}}. Good Luck!
                    
                    <br><br>

                    <a href="{{url('home')}}">Return To My Account</a><br><br>    
                </center>
            </div>
        </div>
    </div>
@endsection
