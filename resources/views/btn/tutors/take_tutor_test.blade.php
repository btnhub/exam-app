@extends('layouts.education_theme.assessment_template')

@section('content')

    <h2 style="font-family:serif"><b>{{$tutor_test->proficiency_exam->name}} Proficiency Exam</b></h2>
    
    @if (!$tutor_candidate->grant_access())
        <div style="background-color: white; padding: 30px; border-radius: 25px; margin: 20px; min-height:500px">

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <center>
                        <h3 style="font-weight: bold">Prove You Know Your Stuff!</h3>
                        {{$tutor_test->user->first_name}} is interested in working with you, but would like you to pass a proficiency exam first.  <br>
                        This service is <b>free to students</b>, however tutors are responsible for the ${{$tutor_test_price}} fee.     
                    </center>
                    @if (!$tutor_candidate->deadline->isFuture())
                        <center>
                            <h4 style="color:darkred;font-weight: bold"><em>Deadline Has Passed</em></h4>
                        </center>
                    @endif
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-6">
                    <h4><b>Self-grading Multiple Choice Exam</b></h4>
                    For years, we have been able to help weed out unqualified tutors.  We provide college & high school level proficiency exams to help students and parents find intelligent tutors.
                    <br><br>

                    <img src="{{asset('img/screenshots/physics-phys1-01-814x700.png')}}" alt="" style="max-width:100%">
                </div>
                <div class="col-sm-6">
                    <h4><b>What Now?</b></h4>
                    {{$tutor_test->user->first_name}} created a proficiency exam with <b>{{$tutor_test->questions->count()}} questions</b> for potential tutors to pass and has set a deadline of <b>{{$tutor_candidate->deadline->format('m/d/Y')}}</b>.  Simply pass this test and we'll e-mail {{$tutor_test->user->first_name}} your results. 
                    <br><br>

                    <center style="color:darkblue; font-weight: bold">
                        You need to score at least {{$tutor_candidate->grade_cutoff}}% to pass.  <br>
                        Do you have what it takes?
                    </center>
                    <br>

                    <hr>

                    <br>
                    {!! Form::open(['method' => 'POST', 'route' => ['purchase.tutor.test', $tutor_test->ref_id, $tutor_candidate->ref_id], 'id' => 'payment-form']) !!}
                    
                        @include('layouts.stripe_form')
                        <div class="form-group">
                            <div class="checkbox{{ $errors->has('nonrefundable') ? ' has-error' : '' }}">
                                <label for="nonrefundable">
                                    {!! Form::checkbox('nonrefundable', '1', null, ['id' => 'nonrefundable', 'required']) !!} I understand this payment is <b>non-refundable</b>.
                                </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('nonrefundable') }}</small>
                        </div>
                        <div class="form-group">
                            <div class="checkbox{{ $errors->has('limited_access') ? ' has-error' : '' }}">
                                <label for="limited_access">
                                    {!! Form::checkbox('limited_access', '1', null, ['id' => 'limited_access', 'required']) !!} I understand I have until <b>{{$tutor_candidate->deadline->format('m/d/Y')}}</b> to access and submit this test.
                                </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('limited_access') }}</small>
                        </div>
                        <br>
                        <div class="btn-group pull-right">
                            @if ($tutor_candidate->deadline->isFuture())
                                {!! Form::submit("Take Test ($$tutor_test_price)", ['class' => 'btn btn-lg btn-warning']) !!}
                            @else
                                <button type="button" class="btn btn-lg btn-warning" disabled>Take Test (${{$tutor_test_price}})</button>
                            @endif
                            
                        </div>
                    
                    {!! Form::close() !!}        
                </div>
            </div> 
        </div>

    @else
        <h4>{{$questions->count()}} Questions</h4>
        To Scale Equations: Right click the equation --> Math Settings --> Scale All Math...
        <hr class="my-4">
        <ul class="nav nav-tabs">
            <?php $i = 1?>
            @foreach ($questions as $question)
                <li @if ($i==1)
                    class="active"
                    @endif >
                    <a data-toggle="tab" href="#menu{{$i}}">{{$i}} 
                        <span id="checked{{$i}}" 
                            @if (!$question->pivot || !$question->pivot->selected_answer_id)
                                hidden
                            @endif>
                            <span class="glyphicon glyphicon-check" aria-hidden="true"></span>    
                        </span>
                        
                    </a>
                </li>
                <?php $i++;?>
            @endforeach
        </ul>

        {!! Form::open(['method' => 'POST', 'route' => ['grade.tutor.test', $tutor_test->ref_id, $tutor_candidate->ref_id], 'display' => 'inline', 'onsubmit' => "return confirm('Are you sure?');"]) !!}
            <div class="tab-content">
                <?php $j = 1;?>
            
                @foreach ($questions as $question)
                    <div id="menu{{$j}}" class="tab-pane fade @if ($j == 1)
                                                                in active
                                                              @endif">

                        <div style="background-color: white; padding: 20px; border-radius: 25px; margin: 20px; min-height:500px">
                            <h3>Question {{$j}}</h3>
                            <center>
                                <span style="font-size: 20px">{!!$question->question!!} </span>
                                @if ($question->question_image)
                                    <br>
                                    <img src='{{asset("btn/exam_images/questions/{$question->question_image}")}}'>
                                    <br>
                                @endif
                            </center>
                            <br>
                            <div class="row">
                                <div class="col-md-offset-2">
                                    <ol type='A'>
                                        @foreach ($question->answers as $ans)
                                            
                                            <div class="row">
                                                <div class="radio{{ $errors->has("answers[$question->id]") ? ' has-error' : '' }}">
                                                    <label>
                                                        {!! Form::radio("answers[{$question->id}]", $ans->id,  $question->pivot ? ($question->pivot->selected_answer_id == $ans->id ? 1 : null) : null, ['onChange' => "markAsChecked($j)"]) !!} <span style="font-size: 18px">
                                                            {!!$ans->answer!!}
                                                        </span>
                                                    </label>
                                                    <small class="text-danger">{{ $errors->first("answers[$question->id]") }}</small>
                                                </div>
                                            </div>
                                        @endforeach
                                    </ol>
                                </div>
                            </div>    
                        </div>
                    </div>

                    <?php $j++?>
                    {!! Form::hidden("assq[]", $question->ref_id) !!}
                @endforeach

                <div style="padding:20px">
                    {!! Form::button("Submit Exam", ['class' => 'btn btn-primary btn-lg pull-right', 'type' => 'submit']) !!}
                </div>           
            </div>      
        {!! Form::close() !!}

        @include('btn.partials.mathjax')
    @endif

    
@endsection

@section('scripts')
    <script type="text/javascript">
        function markAsChecked(question_no){
            var span_id = "checked" + question_no;
            document.getElementById(span_id).style = "display:inline";
        }

        function showHideComplaint(problem_id, ref_id){
            if (document.getElementById(problem_id).checked == true) {
                document.getElementById(ref_id).style = "display:block";
            }
            else
            {
                document.getElementById(ref_id).style = "display:none";   
            }
        }
    </script>
@endsection