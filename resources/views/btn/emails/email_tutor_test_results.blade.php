@extends('layouts.cerberus_email.template')

@section('email_body')
<tr>
    <td bgcolor="#ffffff">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="padding: 40px; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                    <p style="margin: 0 0 10px 0;">
                   		{{$tutor_test->user->first_name}},
						<br><br>
						{{$tutor_candidate->first_name}} just took your {{$tutor_test->proficiency_exam->name}} proficiency exam.

						<div class="row">
							<div class="col-xs-4">
							<h3><b>{{$tutor_candidate->first_name}}'s Results</b></h3>	
								<table class="table table-striped">
									<tbody>
										<tr>
											
											<td style="padding: 2px 5px">
												<b>Course: </b>
											</td>
											<td style="padding: 2px 5px">
												{{$tutor_test->proficiency_exam->name}}
											</td>
										</tr>
										<tr>
											<td style="padding: 2px 5px"><b>Tutor's Grade: </b></td>
											<td style="padding: 2px 5px">
												<span style="color: @if ($tutor_candidate->grade() < $tutor_candidate->grade_cutoff)
														darkred
													@else
														darkgreen
													@endif"><b>
														{{$tutor_candidate->grade()}}%
													</b>
												</span>
											</td>
										</tr>
										<tr>
											<td style="padding: 2px 5px"><b>Passing Grade: </b></td>
											<td style="padding: 2px 5px"> {{$tutor_candidate->grade_cutoff}}%</td>
										</tr>
									</tbody>
								</table>
								<br>
								<!-- Button : BEGIN -->
								<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;">
								    <tr>
								        <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
								            <a href="{{url('/home#tutor_proficiency_results')}}" target="_blank" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 110%; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a">
								                <span style="color:#ffffff;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;View Results&nbsp;&nbsp;&nbsp;&nbsp;</span>
								            </a>
								        </td>
								    </tr>
								</table>
								<!-- Button : END -->
	
							</div>

							<div class="col-xs-8">
								<h3><b>So, What's Next?</b></h3>	
								How to proceed is entirely up to you. The proficiency exam is only one part of the evaluation process.  Here are a few additional tips:
								<ol>
									<li><b>Past Performance:</b> Ask for a transcript demonstrating the tutor's performance in this course.</li>
									<li><b>Past Experience:</b> Ask for details about {{$tutor_candidate->first_name}}'s previous teaching experience and ask if {{$tutor_candidate->first_name}} has ever taught or tutored this course before.</li>
									<li><b>First Meeting: </b>Consider asking for a free informal meet-and-greet (e.g. over coffee) to see if you'd be comfortable working with {{$tutor_candidate->first_name}}.</li>
									<li><b>Be Skeptical!</b> If you don't see a measurable improvement in your performance and confidence, drop {{$tutor_candidate->first_name}} and find a better tutor. </li>
								</ol>
								<center>
									<b>Never waste your time and money on unhelpful tutors. <br>
									Remember, it's not you, it's them!</b>
								</center>
							</div>
						</div>
                    </p>
                </td>
            </tr>
        </table>
    </td>
</tr>
	
	@include('layouts.cerberus_email.partials.footer_proficiency_exams')
@endsection

