@extends('layouts.cerberus_email.template')

@section('email_body')

<!-- 1 Column Text : BEGIN -->
<tr>
    <td bgcolor="#ffffff">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="padding: 40px; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                    <p style="margin: 0 0 10px 0;">
						
						<b>{{$tutor_candidate->first_name}},</b>
						<br><br>
						Great News!  A student, {{$tutor_test->user->first_name}}, is looking for a tutor for {{$tutor_test->proficiency_exam->name}} and is interested in working with you.  {{$tutor_test->user->first_name}} would like you to pass a {{$tutor_test->proficiency_exam->name}} Proficiency Exam to prove that you are knowledgeable in this area.

						
						<table>
							<tbody>
								<tr>
									<td style="padding: 2px 5px"><b>Course: </b></td>
									<td style="padding: 2px 5px"> {{$tutor_test->proficiency_exam->name}}</td>
								</tr>
								<tr>
									<td style="padding: 2px 5px"><b>Deadline: </b></td>
									<td style="padding: 2px 5px">{{$tutor_candidate->deadline->format('m/d/Y h:i A')}} </td>
								</tr>
								<tr>
									<td style="padding: 2px 5px"><b>Passing Grade: </b></td>
									<td style="padding: 2px 5px"> {{$tutor_candidate->grade_cutoff}}%</td>
								</tr>
							</tbody>
						</table>	
						
						<h3>What Next?</h3>	
						Prove you know your stuff!  Simply pass the {{$tutor_test->proficiency_exam->name}} proficiency exam and {{$tutor_test->user->first_name}} will then decide whether or not to work with you.
						<br><br><br>
						<center>
							<!-- Button : BEGIN -->
							<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;">
							    <tr>
							        <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
							            <a href="{{url('/register')}}" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 110%; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a" target="_blank">
							                <center><span style="color:#ffffff;" class="button-link">Create An Account</span></center>
							            </a>
							        </td>
							        <td>&nbsp;&nbsp;&nbsp;&nbsp;Then &nbsp;&nbsp;&nbsp;&nbsp;</td>
							        <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
							            <a href="{{route('take.tutor.test', ['tutor_test_ref_id' => $tutor_test->ref_id, 'tutor_candidate_ref_id' => $tutor_candidate->ref_id])}}" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 110%; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a" target="_blank">
							                <center><span style="color:#ffffff;" class="button-link">Ace The Test!</span></center>
							            </a>
							        </td>
							    </tr>
							</table>
						</center>					
                    </p>
                    <tr>
		                <td style="padding: 0px 0px 20px; text-align: center;">
		                    <h1 style="margin: 0; font-family: sans-serif; font-size: 24px; line-height: 125%; color: #333333; font-weight: normal;"><em>Good Luck!</em></h1>
		                </td>
		            </tr>
                </td>
            </tr>
        </table>
    </td>
</tr>
<!-- 1 Column Text : END -->

@endsection
