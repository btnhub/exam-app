@extends('layouts.cerberus_email.template')

@section('email_body')

<!-- 1 Column Text : BEGIN -->
<tr>
    <td bgcolor="#ffffff">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="padding: 40px; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                    <p style="margin: 0 0 10px 0;">
						
						<b>{{$user->first_name}},</b>
						<br><br>
						Below is your receipt for your recent purchase (ID: {{$ref_id}}):

						<table class="table">
							<thead>
								<tr>
									<th style="padding: 2px 30px">Description</th>
									<th style="padding: 2px 30px">Amount</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="padding: 2px 30px">{{$description}}</td>
									<td style="padding: 2px 30px"><center>${{$amount}}</center></td>
								</tr>
							</tbody>
						</table>
                    </p>
                    <tr>
		                <td style="padding: 0px 0px 20px; text-align: center;">
		                    <h1 style="margin: 0; font-family: sans-serif; font-size: 24px; line-height: 125%; color: #333333; font-weight: normal;"><em>Thank You!</em></h1>
		                </td>
		            </tr>
                </td>
            </tr>
        </table>
    </td>
</tr>
<!-- 1 Column Text : END -->

@endsection
