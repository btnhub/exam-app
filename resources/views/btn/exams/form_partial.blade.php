<div class="col-md-3">
    <div class="form-group{{ $errors->has('course_id') ? ' has-error' : '' }}">
        {!! Form::label('course_id', 'Course') !!}
        {!! Form::select('course_id',$courses, null, ['id' => 'course_id', 'class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('course_id') }}</small>
    </div>    
</div>

<div class="col-md-1">
    <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
        {!! Form::label('year', 'Year') !!}
        {!! Form::number('year', null, ['class' => 'form-control', 'required' => 'required', 'min' => '1990', 'max' => date('Y')]) !!}
        <small class="text-danger">{{ $errors->first('year') }}</small>
    </div>        
</div>

<div class="col-md-1">
    <div class="form-group{{ $errors->has('semester_id') ? ' has-error' : '' }}">
        {!! Form::label('semester_id', 'Semester') !!}
        {!! Form::select('semester_id',$semesters, null, ['id' => 'semester_id', 'class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('semester_id') }}</small>
    </div>

</div>

<div class="col-md-1">
    <div class="form-group{{ $errors->has('exam_number') ? ' has-error' : '' }}">
        {!! Form::label('exam_number', 'Exam #') !!}
        {!! Form::text('exam_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('exam_number') }}</small>
    </div>        
</div>
