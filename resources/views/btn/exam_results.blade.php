@extends('layouts.education_theme.template')
@section('scripts')
    @include('btn.partials.mathjax')
@endsection
@section('content')

    <h2>{{$exam->course->school->name}}</h2>
    <h3>{{$exam->course->course_name}} Exam</h3>
    <h4>You Scored {{$grade}}%</h4>

    <a href="{{route('select-exam')}}">View Exam List</a><br><br>

    Correct answers are in blue. 

        <ol>
            @foreach ($exam->questions as $q)
                <li>
                    {{$q->question}} <a href="#"><button>Report This Question</button></a>
                    @if ($q->question_image)
                        <br>
                        <img src='{{asset("btn/exam_images/questions/{$q->question_image}")}}'>
                        <br>
                    @endif
                    
                    <ol type='A'>
                        @foreach ($q->answers as $ans)
                            @if ($ans->correct_answer == 1)
                                <span style='color:blue'><b><li>{{$ans->answer}}</li></b></span>
                            @elseif (isset($student_answers[$q->id]) 
                                        && $student_answers[$q->id] == $ans->id
                                        && $student_answers[$q->id] != $q->correct_answer()->id)
                                <span style='color:red'><li>{{$ans->answer}}</li></span>
                            @else
                                <li>{{$ans->answer}}</li>
                            @endif
                        @endforeach
                    </ol>
                </li>
                <br><br>
            @endforeach 
        </ol>

    
    <a href="{{route('select-exam')}}"><button type="button" class="btn btn-primary">Exam List</button></a><br><br>
@endsection