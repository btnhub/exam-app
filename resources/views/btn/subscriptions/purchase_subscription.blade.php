@extends('layouts.education_theme.template')

@section('content')
	<h2>Review Subscription</h2>
	<div class="col-md-6">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Course Package</th>
					<th>Subscription</th>
					<th>Price</th>
				</tr>
			</thead>
			<tbody>
				@foreach (Cart::content() as $cart_item)
					<tr>
						<td>
							{{$cart_item->options->package_name}}<br>
						</td>
						<td>
							{{$cart_item->name}} - {{$cart_item->model->duration}} Weeks
						</td>
						<td>${{$cart_item->price}}</td>
					</tr>
				@endforeach
				<tr>
					<td></td>
					<td></td>
					<td><b>${{number_format(Cart::subtotal(),0)}}</b></td>
				</tr>
			</tbody>
		</table>	
	</div>

	<div class="col-md-6">
		{!! Form::open(['method' => 'POST', 'route' => 'purchase.subscriptions', 'class' => 'form-horizontal', 'id' => 'payment-form']) !!}
		
		    @include('layouts.stripe_form')	
		
			<div class="form-group">
			    <div class="checkbox{{ $errors->has('beta') ? ' has-error' : '' }}">
			        <label for="beta">
			            {!! Form::checkbox('beta', '1', null, ['id' => 'beta', 'required']) !!} <span>I understand that this is a <b>Beta version</b> and may have errors.</span>
			        </label>
			    </div>
			    <small class="text-danger">{{ $errors->first('beta') }}</small>
			</div>

			<div class="form-group">
			    <div class="checkbox{{ $errors->has('refund') ? ' has-error' : '' }}">
			        <label for="refund">
			            {!! Form::checkbox('refund', '1', null, ['id' => 'refund', 'required']) !!} <span>I understand this subscription is <b>non-refundable</span>.
			        </label>
			    </div>
			    <small class="text-danger">{{ $errors->first('refund') }}</small>
			</div>


		    <div class="btn-group pull-right">
		        {!! Form::submit("Purchase Subscription", ['class' => 'btn btn-primary']) !!}
		    </div>
		
		{!! Form::close() !!}
	</div>
	
@endsection