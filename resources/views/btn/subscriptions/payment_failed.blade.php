@extends('layouts.education_theme.template')

@section('content')
	<h2>Payment Failed</h2>
	We were unable to process your payment.  Please try again.
@endsection
