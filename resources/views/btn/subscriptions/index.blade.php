@extends('layouts.education_theme.template')

@section('content')
	<h2>Subscriptions</h2>

	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Duration</th>
				<th>Price</th>
				<th></th>				
			</tr>
		</thead>
		<tbody>
			@foreach ($subscriptions as $subscription)
				<tr>
					<td>{{$subscription->id}}</td>
					<td>{{$subscription->name}} 
						@if (!$subscription->active)
							<b style="color:darkred">Not Active</b>
						@endif
						<br>
						{{$subscription->description}}
					</td>
					<td>{{$subscription->duration}}</td>
					<td>${{$subscription->price}}<br>
						${{$subscription->sale_price}} (Sale)
						@if ($subscription->on_sale)
							<br><b style="color:darkred">On Sale</b>
						@endif
					</td>
					<td>
						{!! Form::open(['method' => 'POST', 'route' => ['create.free.subscription', $subscription->id], 'class' => 'form-horizontal', 'id' => "create".$subscription->id]) !!}
						
							<div class="row">
								<div class="col-md-6">
									<div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
								        {!! Form::label('user_id', 'Free Users') !!}
								        {!! Form::select('user_id', $free_users, null, ['id' => 'user_id', 'class' => 'form-control', 'required' => 'required',]) !!}
								        <small class="text-danger">{{ $errors->first('user_id') }}</small>
								    </div>	
								</div>
								<div class="col-md-6">
									<div class="form-group{{ $errors->has('package_id') ? ' has-error' : '' }}">
									    {!! Form::label('package_id', 'Package') !!}
									    {!! Form::select('package_id',$packages, null, ['id' => 'package_id', 'class' => 'form-control', 'required' => 'required']) !!}
									    <small class="text-danger">{{ $errors->first('package_id') }}</small>
									</div>
								</div>
							</div>
						    							
						    <div class="btn-group pull-right">
						        {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
						    </div>
						
						{!! Form::close() !!}
					</td>
				</tr>	
			@endforeach
			
		</tbody>
	</table>

	<hr>
	<h2>Subscribers</h2>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Subscription</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($subscribers as $subscriber)
				<tr>
					<td>{{$subscriber->id}}</td>
					<td>{{$subscriber->full_name}}</td>
					<td>
						@if ($subscriber->subscriptions)
							@foreach ($subscriber->subscriptions as $subscription)
								<div class="col-md-3">
									<b>Name:</b> {{$subscription->name}}<br>
									<b>Start:</b> {{Carbon\Carbon::parse($subscription->pivot->start_date)->format('m/d/Y')}} <br>
									<b>End:</b> {{Carbon\Carbon::parse($subscription->pivot->end_date)->format('m/d/Y')}}<br>
									<b>Package:</b> {{$subscription->package()->title}}

									@if ($subscription->pivot->free)
										<br><b style="color:darkblue">Free Subscription</b>
									@endif	
								</div>
								
							@endforeach

						@endif

					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection