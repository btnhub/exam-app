@extends('layouts.education_theme.template')
@section('page_title', 'Subscriptions')
@section('content')
	<h2>Select Subscription
	@if (Cart::count())
		<div class="pull-right">
			<a href="#cart" class="btn btn-lg btn-primary">
				<img alt="" width="35" src="{{asset('img/education_theme/icons/icon-cart-grey.svg')}}"> View Cart
			</a>
			
		</div>
	@endif
	</h2>
	<div class="row">
		<div class="col-md-12">
			<h4><b>Exams Available In The Following Departments: 
			@foreach ($departments as $department)
				<a href="#{{$department}}">{{$department}}</a>,
			@endforeach		
			 more to come!
			 </b></h4>
		</div>
	</div>
	<br>
	Please note you are purchasing a discounted subscription to a <b>Beta version</b>.  The package & questions may have a few mistakes such as e.g. spelling or formatting errors.  Any feedback helps us improve this service to you and other students and is greatly appreciated!<br><br>
	@include('layouts.education_theme.partials.pricing_table')
	


	@if (auth()->user() && auth()->user()->active_subscriptions()->count())
		You currently have the following subscription(s):
		@foreach (auth()->user()->active_subscriptions()->get() as $active_subscription)
			<li><b>{{$active_subscription->package()->title}}</b> <small>(Ending {{Carbon\Carbon::parse($active_subscription->pivot->end_date)->diffForHumans()}})</small></li>
		@endforeach
	@endif
	<div class="col-md-8">
		@foreach ($departments as $department)
			<h2 id="{{$department}}"><b style="color:darkgreen; font-family: serif">{{strtoupper($department)}}</b></h2>
			<table class="table">
				<tbody>
					@foreach ($packages->where('department', $department)->sortby('order') as $package)
						<tr>
							<td>
								<h3>{{$package->title}}</h3> <br>

								@if ($package->description)
									{{$package->description}}<br><br>
								@endif
								
								<a data-toggle="collapse" data-target="#topics_{{$package->ref_id}}">{{-- {{$package->active_topics()->count()}} Topics, {{$package->questions()->count()}} Questions --}} View Topics</a>
								<br>

								<small>Click for the number of questions for each topic.</small>
								<div id="topics_{{$package->ref_id}}" class="collapse">
									
									@foreach ($package->active_topics() as $topic)
										<li>{{$topic->topic}} ({{$topic->questions->count()}})</li>
									@endforeach
								</div>
							</td>
							<td>
								
								{!! Form::open(['method' => 'POST', 'route' => ['add.to.cart', 'package_ref_id' => $package->ref_id], 'class' => 'form-horizontal', 'id' => $package->ref_id]) !!}
									@foreach ($subscriptions->sortByDesc('duration') as $subscription)
										<div class="radio{{ $errors->has('$subscription_id[$package->ref_id]') ? ' has-error' : '' }}">
										    <label>
										        {!! Form::radio("subscription_id[$package->ref_id]",$subscription->ref_id,  null, ['required', 'class' => 'pull-left']) !!} 
										        <span>{{$subscription->name}} - 

										        	@if ($subscription->on_sale)
		                                                <s>${{number_format($subscription->price,0)}}</s>  ${{number_format($subscription->sale_price,0)}}
		                                            @else
		                                        		${{number_format($subscription->price,0)}}
		                                            @endif
		                                         </span>
										         <br>
										        
										        {{$subscription->duration}} Weeks
										    </label>
										    <small class="text-danger">{{ $errors->first('$subscription_id[$package->ref_id]') }}</small>
										</div>
									@endforeach
									<br>
									{!! Form::submit("Add To Cart", ['class' => 'col-md-4 col-sm-4 btn btn-primary']) !!}
								{!! Form::close() !!}
								<br><br>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		@endforeach
	</div>

	<div class="col-md-4">
		<h2 id="cart">Cart</h2>
		<table class="table">
			@forelse (Cart::content() as $cart_item)
				<tr>
					<td>{!! Form::open(['method' => 'DELETE', 'route' => ['remove.item', 'row_id' => $cart_item->rowId], 'class' => 'delete']) !!}	
							<div class="btn-group pull-right">
						        {!! Form::submit("X", ['class' => 'btn btn-sm btn-danger']) !!}
						    </div>
						{!! Form::close() !!}
					</td>
					<td>{{$cart_item->options->package_name}} - {{$cart_item->name}}<br>
						<small>{{$cart_item->model->duration}} Weeks</small>

					</td>
					<td>${{$cart_item->price}}</td>
				</tr>
			@empty
				<tr><td><em>No Items In Cart</em></td></tr>
			@endforelse
			@if (Cart::count())
				<tr>
					<td></td>
					<td></td>
					<td><b>${{number_format(Cart::subtotal(),0)}}</b><br><br>
						{!! Form::open(['method' => 'GET', 'route' => 'review.subscriptions', 'class' => 'form-horizontal']) !!}
						    <div>
						        {!! Form::submit("Proceed To Checkout", ['class' => 'btn btn-success']) !!}
						    </div>
						
						{!! Form::close() !!}
					</td>
				</tr>
			@endif
		</table>
	</div>
@endsection