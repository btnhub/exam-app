@extends('layouts.education_theme.assessment_template')

@section('content')
{{-- 
	<div class="row">
    	<div class="col-md-12">
        	<div class="jumbotron"  style="min-height: 830px">
				<a class="navbar-brand pull-right" href="{{ url('/') }}">
                    <span style="font-family: Yeseva One, cursive; font-size: 40px !important">
                        <span style="color:gray">Exam</span><b><span style="color:darkred">STASH</span></b>
                    </span>
                </a> --}}

				<h1>{{$package->title}} Exam</h1>
				<h3>Select Topics </h3>
				<p>{{count($topics)}} Topics (number of questions in parenthesis)</p>
				<br>
				<b>Get started by either:</b>
				<li>Selecting topics to focus on, or</li>
				<li>Simply clicking "Start Exam" (questions will be randomly selected from all the topics).</li>

				<br>
				<h4><b>Good Luck!</b></h4>
			

				{!! Form::open(['method' => 'POST', 'route' => ['take.assessment', $package->ref_id], 'class' => 'form-horizontal']) !!}
					
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group pull-right">
						        {!! Form::submit("Start Exam", ['class' => 'btn btn-success']) !!}
						    </div>
						</div>
						
					</div>
				    {{-- @foreach ($topics as $topic_id => $topic_title) --}}
				    @foreach ($topic_chunks as $key => $topic_array)
				    	<div class="col-md-3 col-sm-3">
				    		@foreach ($topic_array as $key => $topic)
					    		
					    		<div class="form-group">
						    	    <div class="checkbox{{ $errors->has('topic_ids[]') ? ' has-error' : '' }}">
						    	        @if ($topic->questions->count())
						    	        	<label>
							    	            {!! Form::checkbox('topic_ids[]',$topic->id, null) !!} <b>{{$topic->topic}} ({{$topic->questions->count()}})</b>
							    	        </label>	
						    	        @else
						    	        	<label>
						    	            {!! Form::checkbox('topic_ids[]',$topic->id, null) !!} {{$topic->topic}} 
						    	        	</label>
						    	        @endif

						    	        
						    	    </div>
						    	    <small class="text-danger">{{ $errors->first('topic_ids[]') }}</small>
						    	</div>
					    	@endforeach	
				    	</div>
				    @endforeach

				<div class="row">
					<div class="col-md-12">
						<div class="btn-group pull-right">
					        {!! Form::submit("Start Exam", ['class' => 'btn btn-success']) !!}
					    </div>
					</div>
					
				</div>
				{!! Form::close() !!}
{{-- 			</div>
		</div>
		
	</div> --}}
@endsection