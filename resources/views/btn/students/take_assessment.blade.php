@extends('layouts.education_theme.assessment_template')

@section('content')

    <h1>{{$package->title}} Exam</h1>
    <h4>{{$questions->count()}} Questions</h4>
    To Scale Equations: Right click the equation --> Math Settings --> Scale All Math...
    <hr class="my-4">
    <ul class="nav nav-tabs">
        <?php $i = 1?>
        @foreach ($questions as $question)
            <li @if ($i==1)
                class="active"
                @endif >
                <a data-toggle="tab" href="#menu{{$i}}">{{$i}} 
                    <span id="checked{{$i}}" 
                        @if (!$question->pivot || !$question->pivot->selected_answer_id)
                            hidden
                        @endif>
                        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>    
                    </span>
                    
                </a>
            </li>
            <?php $i++;?>
        @endforeach
    </ul>

    {!! Form::open(['method' => 'POST', 'route' => ['grade.assessment', $package->ref_id], 'display' => 'inline', 'onsubmit' => "return confirm('Are you sure?');"]) !!}
        <div class="tab-content">
            <?php $j = 1;?>
        
            @foreach ($questions as $question)
                <div id="menu{{$j}}" class="tab-pane fade @if ($j == 1)
                                                            in active
                                                          @endif">

                    <div style="background-color: white; padding: 20px; border-radius: 25px; margin: 20px; min-height:500px">
                        <h3>Question {{$j}}</h3>
                        <center>
                            <span style="font-size: 20px">{!!$question->question!!} </span>
                            @if ($question->question_image)
                                <br>
                                <img src='{{asset("btn/exam_images/questions/{$question->question_image}")}}'>
                                <br>
                            @endif
                        </center>
                        <br>
                        <div class="row">
                            <div class="col-md-offset-2">
                                <ol type='A'>
                                    @foreach ($question->answers as $ans)
                                        
                                        <div class="row">
                                            <div class="radio{{ $errors->has("answers[$question->id]") ? ' has-error' : '' }}">
                                                <label>
                                                    {!! Form::radio("answers[{$question->id}]", $ans->id,  $question->pivot ? ($question->pivot->selected_answer_id == $ans->id ? 1 : null) : null, ['onChange' => "markAsChecked($j)"]) !!} <span style="font-size: 18px">
                                                        {!!$ans->answer!!}
                                                    </span>
                                                </label>
                                                <small class="text-danger">{{ $errors->first("answers[$question->id]") }}</small>
                                            </div>
                                        </div>
                                    @endforeach
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                                <div class="form-group">
                                    <div class="checkbox{{ $errors->has("problem[$question->ref_id]") ? ' has-error' : '' }}">
                                        <label>
                                            {!! Form::checkbox("problem[$question->ref_id]",1, null, ['id' => "problem[$question->ref_id]", 'onclick' => "showHideComplaint(this.id, '$question->ref_id')"]) !!} <span style="color:black">Report An Issue</span>
                                        </label>
                                    </div>
                                    <small class="text-danger">{{ $errors->first("problem[$question->ref_id]") }}</small>
                                </div>

                                <br>
                                <span id="{{$question->ref_id}}" hidden>
                                    <div class="form-group{{ $errors->has("complaint[$question->ref_id]") ? ' has-error' : '' }}">
                                        {!! Form::label("complaint[$question->ref_id]", 'Briefly describe the issue') !!}
                                        <br><small>Your feedback is greatly appreciated!</small>
                                        {!! Form::textarea("complaint[$question->ref_id]", null, ['class' => 'form-control', 'size' => '20x3']) !!}
                                        <small class="text-danger">{{ $errors->first("complaint[$question->ref_id]") }}</small>
                                    </div>    
                                </span>
                                
                            </div>
                        </div>    
                    </div>
                </div>

                <?php $j++?>
                {!! Form::hidden("assq[]", $question->ref_id) !!}
            @endforeach

            {!! Form::hidden('assid', $assessment_ref_id ?? null) !!}
            <div style="padding:20px">
                {!! Form::button("Submit Exam", ['class' => 'btn btn-primary btn-lg pull-right', 'type' => 'submit']) !!}
                @if (auth()->user())
                    <button type="submit" class="btn btn-success btn-lg pull-left" formaction="{{route('pause.assessment', $package->ref_id)}}">Save & Exit</button>
                @endif
            </div>           
        </div>      
    {!! Form::close() !!}

    @include('btn.partials.mathjax')
@endsection

@section('scripts')
    <script type="text/javascript">
        function markAsChecked(question_no){
            var span_id = "checked" + question_no;
            document.getElementById(span_id).style = "display:inline";
        }

        function showHideComplaint(problem_id, ref_id){
            if (document.getElementById(problem_id).checked == true) {
                document.getElementById(ref_id).style = "display:block";
            }
            else
            {
                document.getElementById(ref_id).style = "display:none";   
            }
        }
    </script>
@endsection