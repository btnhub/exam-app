@extends('layouts.education_theme.template')
@section('scripts')
    @include('btn.partials.mathjax')
    <script type="text/javascript">
        function showHideComplaint(problem_id, ref_id){
            if (document.getElementById(problem_id).checked == true) {
                document.getElementById(ref_id).style = "display:block";
            }
            else
            {
                document.getElementById(ref_id).style = "display:none";   
            }
        }
    </script>
@endsection

@section('content')
    
    <h2>{{$package->title}} Exam</h2>
    
    <h4><b>You Scored {{$grade}}%</b></h4>

    <a href="{{url('home')}}">Return To My Account</a><br><br>

    Correct answers are in <span style="color:blue">blue</span>. If you believe there is an error in a question, please click "Report An Issue" and briefly describe the problem.  Your feedback is greatly appreciated!
    <br><br>

        <ol>
            @foreach ($questions as $q)
                <li>
                    {!!$q->question!!} 
                    @if ($q->question_image)
                        <br>
                        <img src='{{asset("btn/exam_images/questions/{$q->question_image}")}}'>
                        <br>
                    @endif
                    
                    <ol type='A'>
                        @foreach ($q->answers as $ans)
                            @if ($ans->correct_answer == 1)
                                <span style='color:blue'><b><li>{!!$ans->answer!!}</li></b></span>
                            @elseif (isset($student_answers[$q->id]) 
                                        && $student_answers[$q->id] == $ans->id
                                        && $student_answers[$q->id] != $q->correct_answer()->id)
                                <span style='color:red'><li>{!!$ans->answer!!}</li></span>
                            @else
                                <li>{!!$ans->answer!!}</li>
                            @endif
                        @endforeach
                        @if (!isset($student_answers[$q->id]))
                            <span style='color:red'>No Answer Selected</li></span>
                        @endif

                        {!! Form::open(['method' => 'POST', 'route' => ['report.issue', 'question_ref_id' => $q->ref_id, 'assessment_ref_id' => $assessment_ref_id], 'id'=>"question-$q->ref_id"]) !!}
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="checkbox{{ $errors->has("problem[$q->ref_id]") ? ' has-error' : '' }}">
                                            <label>
                                                {!! Form::checkbox("problem[$q->ref_id]",1, null, ['id' => "problem[$q->ref_id]", 'onclick' => "showHideComplaint(this.id, '$q->ref_id')"]) !!} <span style="color:darkred">Report An Issue</span>
                                            </label>
                                        </div>
                                        <small class="text-danger">{{ $errors->first("problem[$q->ref_id]") }}</small>
                                    </div>

                                    <br>
                                    <span id="{{$q->ref_id}}" hidden>
                                        <div class="form-group{{ $errors->has("complaint[$q->ref_id]") ? ' has-error' : '' }}">
                                            {!! Form::label("complaint[$q->ref_id]", 'Briefly describe the issue') !!}
                                            <br><small>Your feedback is greatly appreciated!</small>
                                            {!! Form::textarea("complaint[$q->ref_id]", null, ['class' => 'form-control', 'size' => '20x3']) !!}
                                            <small class="text-danger">{{ $errors->first("complaint[$q->ref_id]") }}</small>
                                        </div> 
                                        <br><br>   
                                        <div class="pull-right">
                                            {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
                                        </div>
                                    </span>
                                </div>                                
                            </div>
                        {!! Form::close() !!}
                    </ol>
                </li>
                <br><br>
            @endforeach 
        </ol>

    <a href="{{url('home')}}">Return To My Account</a><br><br>
@endsection
