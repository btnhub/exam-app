@extends('layouts.education_theme.template')
@section('scripts')
    @include('btn.partials.mathjax')
@endsection
@section('content')

    <h2>{{$exam->course->school->name}}</h2>
    <h3>{{$exam->course->course_name}} Exam</h3>
    <h4>{{$exam->questions()->count()}} Questions</h4>

    {!! Form::open(['method' => 'POST', 'route' => ['grade-exam', $exam->id]]) !!}

        <ol>
            @foreach ($exam->questions as $q)
                <li>
                    {{$q->question}}
                    @if ($q->question_image)
                        <br>
                        <img src='{{asset("btn/exam_images/questions/{$q->question_image}")}}'>
                        <br>
                    @endif
                    <br>
                    (Ref ID: {{$q->ref_id}})
                    <ol type='A'>
                        @foreach ($q->answers as $ans)
                            
                            <div class="radio{{ $errors->has("answers[$q->id]") ? ' has-error' : '' }}">
                                <label>
                                    {!! Form::radio("answers[$q->id]", $ans->id,  null, ['id' => 'radio_id']) !!} {{$ans->answer}}
                                </label>
                                <small class="text-danger">{{ $errors->first("answers[$q->id]") }}</small>
                            </div>
                        @endforeach
                    </ol>
                </li>
                <br><br>
            @endforeach 
        </ol>

        <div class="btn-group pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
            {!! Form::submit("Submit Exam", ['class' => 'btn btn-success']) !!}
        </div>

    {!! Form::close() !!}
@endsection