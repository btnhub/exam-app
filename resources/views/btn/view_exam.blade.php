@extends('layouts.education_theme.template')
@section('scripts')
    @include('btn.partials.mathjax')
@endsection
@section('content')
    
    @if (auth()->user()->isAdmin())
        <a href="{{route('select-exam')}}"><-- Back To Exam List</a>
    @endif
    
    @if (auth()->user()->isAdmin() || auth()->user()->isStaff())
        <a href="{{route('exam.upload.progress')}}" class="pull-right">Exam Progress Summary</a>
    @endif

    <h2>{{$exam->course->school->name}}</h2>
    <h3>{{$exam->details()}} Exam  <br>
        Exam ID: {{$exam->id}}</h3>
    
    <span class="pull-right"> 
        @if (!$exam->completed)
            <a href="{{route('update.completed.reviewed', [$exam->id, "completed", 1] )}}" class="btn btn-success">Completed</a>
        @else
            <a href="{{route('update.completed.reviewed', [$exam->id, "completed", 0] )}}">Mark As Uncompleted</a>
        @endif
        
        @if (auth()->user()->isAdmin())
                @if (!$exam->reviewed)
                    <a href="{{route('update.completed.reviewed', [$exam->id, "reviewed", 1] )}}" class="btn btn-warning">Reviewed</a>
                @else
                    <a href="{{route('update.completed.reviewed', [$exam->id, "reviewed", 0] )}}">Mark As Unreviewed</a>
                @endif           

                <a href="#" class="btn btn-danger">Delete EXAM (TO DO)</a>
        @endif   

    </span>

    <h4>{{$exam->questions()->count()}} Questions</h4>
    <a href="{{route('create.question', $exam->id)}}"><button type="button" class="btn btn-primary">Add Question</button></a>

    
    
    <ol>
        @foreach ($exam->questions->sortby('question_number') as $q)
            <li>
                {!!$q->question!!} <a href="{{route('show-question', ['ref_id' => $q->ref_id])}}"><button type="button" class="btn btn-primary">Edit This Question</button></a>
                @if ($q->question_image)
                    <br>
                    <img src='{{asset("btn/exam_images/questions/{$q->question_image}")}}'>
                    <br>
                @endif
                
                <ol type='A'>
                    @foreach ($q->answers as $ans)
                        @if ($ans->correct_answer == 1)
                            <span style='color:red'><li>{!!$ans->answer!!}</li></span>
                        @else
                            <li>{!!$ans->answer!!}</li>
                        @endif
                    @endforeach
                </ol>
            </li>
            ({{ucfirst($exam->semester->semester)}} {{$exam->year}} Exam {{$exam->exam_number}} Question {{$q->question_number}}, Ref ID: {{$q->ref_id}})<br><br>
        @endforeach 
    </ol>
@endsection