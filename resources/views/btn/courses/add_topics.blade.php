@extends('layouts.education_theme.template')

@section('content')

	<h2>Add Topics To {{$course->course_name}}</h2>

	<h3>Existing Topics</h3>
		{{count($course->topics)}} Topics
		<div class="row">
			@foreach ($course->topics->split(4) as $key => $topic_array)
		    	<div class="col-md-3 col-sm-3">
		    		@foreach ($topic_array as $key => $topic)
		    			<li>{{$topic->topic}}</li>
		    		@endforeach
		    	</div>
			@endforeach	
		</div>

		<hr>

	<h3>Similar Course Topics</h3>
		<div class="row">
			@foreach ($course->similar_course_topics()->split(4) as $key => $similar_topic_array)
		    	<div class="col-md-3 col-sm-3">
		    		@foreach ($similar_topic_array as $key => $similar_topic)
		    			<li>{{$similar_topic->topic}}</li>
		    		@endforeach
		    	</div>
			@endforeach	
		</div>
	<hr>

	<h3>Add New Topics</h3>

	<div class="row">
		{!! Form::open(['method' => 'POST', 'route' => ['save.course.topics', $course->id], 'class' => 'form-horizontal']) !!}
			
			<div class="row">
				<div class="btn-group pull-right">
			        {!! Form::submit("Add Topics", ['class' => 'btn btn-success']) !!}
			    </div>
			</div>
		    

			@for ($i = 0; $i < 10; $i++)
				<div class="col-md-4 col-md-offset-1">
					<div class="form-group{{ $errors->has('topics[]') ? ' has-error' : '' }}">
					    {!! Form::text('topics[]', null, ['class' => 'form-control']) !!}
					    <small class="text-danger">{{ $errors->first('topics[]') }}</small>
					</div>
				</div>
					
			@endfor
		
		{!! Form::close() !!}
	</div>	
	
	<br><br><br>
@endsection