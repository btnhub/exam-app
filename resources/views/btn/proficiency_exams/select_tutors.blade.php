@extends('layouts.education_theme.template')
@section('page_title', "Contact Tutors")
@section('content')
	<h2>E-mail Your {{$tutor_test->proficiency_exam->name}} Exam</h2>
	Use this form to e-mail your {{$tutor_test->proficiency_exam->name}} exam to prospective tutors.  Make them prove their worth before you start working with them!

	{!! Form::open(['method' => 'POST', 'route' => ['email.tutor.test', $tutor_test->ref_id]]) !!}
		<h3>Exam Details</h3>
		<div class="row">

			<div class="form-group{{ $errors->has('grade_cutoff') ? ' has-error' : '' }} col-xs-3">
			    {!! Form::label('grade_cutoff', 'Passing Grade') !!}
			    <small>We recommend 80%</small>
			    {!! Form::number('grade_cutoff', 80, ['class' => 'form-control', 'required' => 'required', 'min' => 0, 'max' => 100]) !!}
			    
			    <small class="text-danger">{{ $errors->first('grade_cutoff') }}</small>
			</div>

			<div class="form-group{{ $errors->has('deadline') ? ' has-error' : '' }} col-xs-3">
			    {!! Form::label('deadline', 'Exam Submission Deadline') !!}
			    {!! Form::date('deadline',Carbon\Carbon::now()->addDays(3), ['class' => 'form-control', 'required' => 'required']) !!}
			    <small class="text-danger">{{ $errors->first('deadline') }}</small>
			</div>
		</div>

	    <div id="tutor-info">
			<h3>Tutor Info</h3>
			<div class="row">
				<div class="form-group{{ $errors->has('first_name[]') ? ' has-error' : '' }} col-xs-4">
				    {!! Form::label('first_name[]', 'Tutor First Name') !!}
				    {!! Form::text('first_name[]', old('first_name'), ['class' => 'form-control', 'required' => 'required']) !!}
				    <small class="text-danger">{{ $errors->first('first_name[]') }}</small>
				</div>	

				<div class="form-group{{ $errors->has('last_name[]') ? ' has-error' : '' }} col-xs-4">
				    {!! Form::label('last_name[]', 'Tutor Last Name') !!}
				    {!! Form::text('last_name[]', old('last_name'), ['class' => 'form-control']) !!}
				    <small class="text-danger">{{ $errors->first('last_name[]') }}</small>
				</div>
				<div class="form-group{{ $errors->has('email[]') ? ' has-error' : '' }} col-xs-4">
				    {!! Form::label('email[]', 'Email address') !!}
				    {!! Form::email('email[]', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'eg: tutor@email.com']) !!}
				    <small class="text-danger">{{ $errors->first('email[]') }}</small>
				</div>

			
			</div>

		</div>
		<div id="add-tutor" class="row">
			<p><span class="pull-right" onclick="addTutor()"><a>+ Add Tutor</a></span></p>
		</div>

	
	    <div class="btn-group pull-right">
	        {!! Form::submit("Send E-mails", ['class' => 'btn btn-success']) !!}
	    </div>
		
	{!! Form::close() !!}
@endsection

@section('scripts')
	<script type="text/javascript">
		function addTutor(){
			var row = document.createElement("div");
			row.setAttribute('class', 'row');

			//Create First Name Field
			var first_name_div = document.createElement("div");
			first_name_div.setAttribute('class', 'form-group col-sm-4');
				var first_name_label = document.createElement('label');
				first_name_label.value = 'Tutor First Name';
				var first_name_input = document.createElement("input");
				first_name_input.setAttribute('class',"form-control");
				first_name_input.type = "text";
				first_name_input.name = "first_name[]";
				first_name_input.placeholder = "First Name ***Required***"
				first_name_div.appendChild(first_name_label);
				first_name_div.appendChild(first_name_input);
			
			//Create Last Name Field
			var last_name_div = document.createElement("div");
			last_name_div.setAttribute('class', 'form-group col-sm-4');
				var last_name_input = document.createElement("input");
				last_name_input.setAttribute('class',"form-control");
				last_name_input.type = "text";
				last_name_input.name = "last_name[]";
				last_name_input.placeholder = "Last Name";

				last_name_div.appendChild(last_name_input);

			//Create Email Field
			var email_div = document.createElement("div");
			email_div.setAttribute('class', 'form-group col-sm-4');
				var email_input = document.createElement("input");
				email_input.setAttribute('class',"form-control");
				email_input.type = "email";
				email_input.name = "email[]";
				email_input.placeholder = "tutor@email.com ***Required***";
				email_div.appendChild(email_input);

			//Add To Row
			row.appendChild(first_name_div);
			row.appendChild(last_name_div);
			row.appendChild(email_div);

			//Add to HTML
			var element = document.getElementById("tutor-info");
			element.appendChild(row);
		}
	</script>
@endsection