@extends('layouts.education_theme.template')
@section('page_title', "Proficiency Exams List")
@section('content')
	{!! Form::open(['method' => 'POST', 'route' => 'proficiency.store']) !!}
	
		<div class="row">
			<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-sm-6">
		        {!! Form::label('name', 'Proficiency Exam Name') !!}
		        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
		        <small class="text-danger">{{ $errors->first('name') }}</small>
		    </div>
			<div class="form-group{{ $errors->has('department') ? ' has-error' : '' }} col-sm-6">
		        {!! Form::label('department', 'Department') !!}
		        {!! Form::text('department', null, ['class' => 'form-control', 'required' => 'required']) !!}
		        <small class="text-danger">{{ $errors->first('department') }}</small>
		    </div>	

		</div>
	    
		<div class="row">
		    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }} col-sm-6">
		        {!! Form::label('description', 'Short Description') !!}
		        {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
		        <small class="text-danger">{{ $errors->first('description') }}</small>
		    </div>	
		    <div class="pull-right">
	        	<br>
		        {!! Form::submit("Create Prof Exam", ['class' => 'btn btn-success']) !!}
		    </div>
		</div>
	
	{!! Form::close() !!}

	<hr>

	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Description</th>
				<th>Courses</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@forelse ($proficiency_exams as $prof_exam)
				<tr>
					<td>{{$prof_exam->id}} / {{$prof_exam->ref_id}}</td>
					<td>{{$prof_exam->name}}<br>
						<small>{{$prof_exam->questions()->count()}} Available Questions</small>
						@if ($prof_exam->questions()->count() < (new App\ProficiencyExam)->min_questions)
							<p style="color:green">Insufficienct Number Of Questions</p>
						@endif
						@if (!$prof_exam->available)
							<p style="color:darkred">Not Available</p>
						@endif
					</td>
					<td>{{$prof_exam->description}}<br>
						<small>{{$prof_exam->detailed_description}}</small>
					</td>
					<td>{!!implode('<br>', $prof_exam->courses->pluck('course_number')->toArray())!!}</td>
					<td>
						{!! Form::open(['method' => 'POST', 'route' => ['add.proficiency.courses', $prof_exam->id], 'id' => "exam-$prof_exam->id"]) !!}
						
						    <div class="form-group{{ $errors->has('course_ids[]') ? ' has-error' : '' }}">
						        {!! Form::label('course_ids[]', 'Courses') !!}
						        {!! Form::select('course_ids[]',$courses_array, $prof_exam->courses->pluck('id'), ['id' => 'course_ids[]', 'class' => 'form-control', 'required' => 'required', 'multiple', 'size' => 10]) !!}
						        <small class="text-danger">{{ $errors->first('course_ids[]') }}</small>
						    </div>
						
						    <div class="btn-group pull-right">
						        
						        {!! Form::submit("Add Courses", ['class' => 'btn btn-primary']) !!}
						    </div>
						
						{!! Form::close() !!}
					</td>
				</tr>	
			@empty
				<tr><td colspan="5"><center><em>No Proficiency Exams</em></center></td>
			@endforelse
			
		</tbody>
	</table>
	<small>** Available Questions = Questions from exams that have been reviewed.</small>
@endsection