@extends('layouts.education_theme.template')
@section('scripts')
    @include('btn.partials.mathjax')
@endsection
@section('content')
    <h3>Your {{$tutor_test->proficiency_exam->name}} Exam</h3>

    Here is the test prospective tutors must pass.  Each tutor will be given the same questions. <a href="{{route('select.tutors', $tutor_test->ref_id)}}">Email This Test To Tutors</a> <br>

    @if ($tutor_test->view_solutions())
        Correct answers are in <span style="color:blue"><b>blue</b></span>. 
        
        @if (isset($tutor_answers))
            <h4><b>{{$tutor_candidate->first_name}}'s Performance: {{$tutor_candidate->grade()}}%</b></h4>
            @if ($tutor_candidate->grade() < 100)
                {{$tutor_candidate->first_name}}'s incorrect answers are in <span style="color:red"><b>red</b></span>.
            @endif
        @endif
    @else
        You will be able to view the solutions once a prospective tutor pays to take the test.
    @endif


    <hr>
    <b>{{$tutor_test->questions->count()}} Questions</b>
    <br><br>
        <ol>
            @foreach ($tutor_test->questions as $q)
                <li>
                    {!!$q->question!!}
                    @if ($q->question_image)
                        <br>
                        <img src='{{asset("btn/exam_images/questions/{$q->question_image}")}}'>
                        <br>
                    @endif
                    
                    @if ($tutor_test->view_solutions())
                        <ol type='A'>
                            @foreach ($q->answers as $ans)
                                @if ($ans->correct_answer == 1)
                                    <span style='color:blue'><b><li>{!!$ans->answer!!}</li></b></span>
                                @elseif (isset($tutor_answers[$q->id]) 
                                        && $tutor_answers[$q->id] == $ans->id
                                        && $tutor_answers[$q->id] != $q->correct_answer()->id)
                                    <span style='color:red'><b><li>{!!$ans->answer!!}</li></b></span>
                                @else
                                    <li>{!!$ans->answer!!}</li>
                                @endif
                            @endforeach
                            @if (isset($tutor_answers) && !isset($tutor_answers[$q->id]))
                            <span style='color:red'>No Answer Selected</li></span>
                        @endif
                        </ol>
                    @endif
                </li>
                <br><br>
            @endforeach 
        </ol>
@endsection