{!! Form::open(['method' => 'POST', 'route' => 'create.tutor.test']) !!}

    <div class="row">
		<div class="form-group{{ $errors->has('proficiency_exam_ids[]') ? ' has-error' : '' }}">
		    {!! Form::label('proficiency_exam_ids[]', 'Select Course(s)') !!}
		    {!! Form::select('proficiency_exam_ids[]', $proficiency_exams, null, ['id' => 'proficiency_exam_ids[]', 'class' => 'form-control', 'multiple', 'required']) !!}
		    <small class="text-danger">{{ $errors->first('proficiency_exam_ids[]') }}</small>
		</div>
	</div>

	<div class="form-group{{ $errors->has('question_count') ? ' has-error' : '' }}">
	    {!! Form::label('question_count', 'Number Of Questions') !!}

	    {!! Form::number('question_count',20, ['class' => 'form-control', 'required' => 'required', 'min' => 10, 'max' => 20]) !!}
	    <br>
	    <small>Between 10-20</small>
	    <small class="text-danger">{{ $errors->first('question_count') }}</small>
	</div>

    <div class="btn-group pull-left">
        
        {!! Form::submit("Create Exam", ['class' => 'btn btn-success']) !!}
    </div>

{!! Form::close() !!}