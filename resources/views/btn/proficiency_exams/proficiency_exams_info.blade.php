@extends('layouts.education_theme.template')
@section('page_title', "Tutor Proficiency Exams")
@section('content')

	<h2>Is Your Tutor Any Good?</h2>
	<div class="row">
		<div class="col-sm-6">
			<img src="{{asset('img/confused_teacher_student-1170x874.jpg')}}" style="max-width: 100%">		
		</div>
		<div class="col-sm-6">
			<h3>All tutors claim to be the best, make them prove it</h3>		
			<p>One of the biggest challenges in selecting a tutor is determining if they really know the material.  <b style="color:black">Problem Solved!</b></p>

			<hr>

			<h3>How It Works</h3>
			<p>
				<div class="col-sm-4">
					<center><img alt="" width="50" src="{{asset('img/education_theme/icons/icon-pen.svg')}}">
					<br>
					<h4><b>Create An Exam</b></h4>
					<br>
					</center>
				</div>

				<div class="col-sm-4">
					<center><img alt="" width="50" src="{{asset('img/education_theme/icons/icon-mail-color.svg')}}">	
					<br>
					<h4><b>Send The Exam</b></h4>
					<br>
					</center>
				</div>
				
				<div class="col-sm-4">
					<center><img alt="" width="50" src="{{asset('img/education_theme/icons/icon-graph-color.svg')}}">	
					<br>
					<h4><b>Review Tutor Results</b></h4>
					<br>
					</center>
				</div>
			</p>
			<p><b>Price:</b> This service is <b>free for students</b>, tutors are responsible for the ${{$tutor_test_price}}/exam fee.</p>

			<center><a href="{{url('/home#tutor_proficiency_exams')}}" class="btn btn-lg btn-primary">Get Started!</a></center>
			
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<h3><b>Available Courses</b></h3>
			<span style="color:darkblue;"><b>{{$proficiency_exams->count()}} Courses</b></span>
			@foreach ($departments as $department)
				<h4><b>{{$department}}</b></h4>
				<table class="table">
					<tbody>
						@foreach ($proficiency_exams->sortBy('id') as $proficiency_exam)
							@if ($proficiency_exam->department == $department)
								<tr>
									<td><b>{{$proficiency_exam->name}}</b></td>
									<td>{{$proficiency_exam->description}}<br>
										<small>{{$proficiency_exam->questions()->count()}} Questions</small>
									</td>
								</tr>
							@endif
							
						@endforeach
						
					</tbody>
				</table>	
			@endforeach
			...more to come!
		</div>
	</div>
	<br><br>
	<div class="row">
		<center><p><b><em>Don't waste your time and money on horrible & overpriced tutors, make them prove their worth first!</em></b></p>	</center>
		<center><a href="{{url('/home#tutor_proficiency_exams')}}" class="btn btn-lg btn-primary">Create A Proficiency Exam</a></center>
	</div>
@endsection
