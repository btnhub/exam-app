@extends('layouts.education_theme.template')

@section('content')
	<?php 
		$total_questions = 0;
		foreach ($courses as $course) {
			$total_questions += $course->questions()->count();
		}
	?>

	<div class="row">
		<div class="col-md-7">
			<h2>Courses</h2>
			{{count($courses)}} Courses, <b>{{$total_questions}} Questions</b>
			<table border="1" class="table">
				<thead>
					<th>ID</th>
					<th>Dept</th>
					<th>Course</th>
					<th>Course #</th>
					<th>Count</th>
					<th></th>
				</thead>
				<tbody>
					@foreach ($courses->sortBy('department_id') as $course)
						<tr>
							<td>{{$course->id}}</td>
							<td>{{$course->department->name}}</td>
							<td>{{$course->course_name}}</td>
							<td>{{$course->course_number}}</td>
							<td>
								<b>{{$course->exams->count()}} Exams, 
								<a href="{{route('view.questions', $course->id)}}">{{$course->questions()->count()}} Questions</a></b>
								<?php $no_topic_count = 0?>
								@foreach ($course->questions() as $question)
									@if (!$question->topics->count())
										<?php $no_topic_count++;?>
									@endif
								@endforeach
								@if ($no_topic_count)
									<br>
									<small style="color:darkred">{{$no_topic_count}} Questions w/o topics</small>
								@endif
							</td>
							<td><a href="{{route('add.course.topics', $course->id)}}">Add Topics</a></td>
						</tr>	
					@endforeach
					
				</tbody>
			</table>
		</div>
		<div class="col-md-4">
			<h2>Packages</h2>
			<br>
			<table class="table">
				<thead>
					<th>ID</th>
					<th>Title</th>
					<th>Price</th>
					<th></th>
				</thead>
				<tbody>
					@foreach ($packages as $package)
						<tr>
							<td>{{$package->id}}</td>
							<td>{{$package->title}}</td>
							<td>{{$package->price}}</td>
							<td>
								{{-- <a href="{{route('take.assessment', $package->ref_id)}}">Take Test</a><br> --}}
								<a href="{{route('view.topics', $package->ref_id)}}">View Topics</a>

							</td>
						</tr>
					@endforeach
				</tbody>
			</table>

			<br>
			<h3><a href="{{route('subscriptions.index')}}">View Subscriptions</a></h3>

			<h3><a href="{{route('purchase.select.packages')}}">Purchase Subscription</a></h3>

		</div>
	</div>
	
	<hr>

	<h2>Select Exam To View / Edit</h2>

	{{count($exams)}} Exams Total.  <br>
	{!! Form::open(['method' => 'POST', 'route' => 'save.exam', 'class' => 'form-horizontal']) !!}
	
	    @include('btn.exams.form_partial', ['courses' => $courses_array])
		<br>
		&nbsp;&nbsp;&nbsp;&nbsp;
		{!! Form::submit("Create Exam", ['class' => 'btn btn-success']) !!}
	    
	
	{!! Form::close() !!}

	<h3>Uncompleted Exams</h3>
	{{count($exams->where('completed', 0))}} Uncompleted Exams.  <br>
	<table class="table striped hover" border="1">
		<thead>
			<tr>
				<th>Exam ID</th>
				<th>Course</th>
				<th>Exam Details</th>
				<th># of Questions</th>
				<th>School</th>
				<th>Notes</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($exams->where('completed', 0) as $exam)
				<tr>
					<td>{{$exam->id}}</td>
					<td>
						{{$exam->course->course_name}}  ({{$exam->course->course_number}})
						<a href="{{route('update.completed.reviewed', [$exam->id, "completed", 1] )}}" class="btn btn-sm btn-success pull-right">Completed</a>
					</td>
					<td>
						<a href="{{route('load-exam', $exam->id)}}">
							@if ($exam->exam_number)
								Exam #{{$exam->exam_number}}
							@elseif ($exam->quiz)
								Quiz #{{$exam->quiz}}
							@elseif ($exam->homework)
								Homework #{{$exam->homework}}
							@endif
							
							{{ucfirst($exam->semester->semester)}} {{$exam->year}}
						</a>
					</td>
					<td>{{$exam->questions->count()}}</td>
					<td>{{$exam->course->school->name}} ({{$exam->course->school->school_code}})</td>			
					<td>
						@if ($exam->professor)
							Prof. {{$exam->professor}}<br>
						@endif

						{{$exam->notes}}
						<a href="{{route('take-exam', $exam->id)}}">
							Take Test
						</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<hr>

	<h3>Completed & <b>Unreviewed</b> Exams</h3>
	{{count($exams->where('completed', 1)->where('reviewed', 0))}} Completed & Unreviewed Exams.  <br>
	<table class="table striped hover" border="1">
		<thead>
			<tr>
				<th>Exam ID</th>
				<th>Course</th>
				<th>Exam Details</th>
				<th># of Questions</th>
				<th>School</th>
				<th>Notes</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($exams->where('completed', 1)->where('reviewed', 0) as $exam)
				<tr>
					<td>{{$exam->id}}</td>
					<td>
						{{$exam->course->course_name}}  ({{$exam->course->course_number}})  

						<a href="{{route('update.completed.reviewed', [$exam->id, "completed", 0] )}}" class="btn btn-sm btn-warning pull-right">Uncompleted</a>
						<a href="{{route('update.completed.reviewed', [$exam->id, "reviewed", 1] )}}" class="btn btn-sm btn-info pull-right">Reviewed</a>
						
					</td>
					<td>
						<a href="{{route('load-exam', $exam->id)}}">
							@if ($exam->exam_number)
								Exam #{{$exam->exam_number}}
							@elseif ($exam->quiz)
								Quiz #{{$exam->quiz}}
							@elseif ($exam->homework)
								Homework #{{$exam->homework}}
							@endif
							
							{{ucfirst($exam->semester->semester)}} {{$exam->year}}
						</a>
					</td>
					<td>{{$exam->questions->count()}}</td>
					<td>{{$exam->course->school->name}} ({{$exam->course->school->school_code}})</td>			
					<td>
						@if ($exam->professor)
							Prof. {{$exam->professor}}<br>
						@endif

						{{$exam->notes}}
						<a href="{{route('take-exam', $exam->id)}}">
							Take Test
						</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<hr>

	<h3>Completed & <b>Reviewed</b> Exams</h3>
	{{count($exams->where('completed', 1)->where('reviewed', 1))}} Completed & Reviewed Exams.  <br>
	<table class="table striped hover" border="1">
		<thead>
			<tr>
				<th>Exam ID</th>
				<th>Course</th>
				<th>Exam Details</th>
				<th># of Questions</th>
				<th>School</th>
				<th>Notes</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($exams->where('completed', 1)->where('reviewed', 1) as $exam)
				<tr>
					<td>{{$exam->id}}</td>
					<td>
						{{$exam->course->course_name}}  ({{$exam->course->course_number}}) 
						<a href="{{route('update.completed.reviewed', [$exam->id, "reviewed", 0] )}}" class="btn btn-sm btn-warning pull-right">Unreviewed</a>
					</td>
					<td>
						<a href="{{route('load-exam', $exam->id)}}">
							@if ($exam->exam_number)
								Exam #{{$exam->exam_number}}
							@elseif ($exam->quiz)
								Quiz #{{$exam->quiz}}
							@elseif ($exam->homework)
								Homework #{{$exam->homework}}
							@endif
							
							{{ucfirst($exam->semester->semester)}} {{$exam->year}}
						</a>
					</td>
					<td>{{$exam->questions->count()}}</td>
					<td>{{$exam->course->school->name}} ({{$exam->course->school->school_code}})</td>			
					<td>
						@if ($exam->professor)
							Prof. {{$exam->professor}}<br>
						@endif

						{{$exam->notes}}
						<a href="{{route('take-exam', $exam->id)}}">
							Take Test
						</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection