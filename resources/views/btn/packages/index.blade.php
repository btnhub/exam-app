@extends('layouts.education_theme.template')

@section('content')
	<h2>Packages</h2>

	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Title</th>
				
			</tr>
		</thead>
		<tbody>
			@foreach ($packages as $package)
				<tr>
					<td>{{$package->id}}</td>
					<td>{{$package->title}}<br>
						{{$package->description}}
					</td>
				</tr>	
			@endforeach
			
		</tbody>
	</table>
@endsection