@extends('layouts.education_theme.template')
@section('page_title', 'My Account')
@section('content')
<div class="nicdark_container nicdark_clearfix">
    <div class="grid grid_3">

        <div class="nicdark_section nicdark_box_sizing_border_box">
                    
            <!--start preview-->
            <div class="nicdark_section ">
        
                <!--image-->
                {{-- <div class="nicdark_section nicdark_position_relative">
                    
                    <img alt="" class="nicdark_section" src="img/education_theme/avatar/avatar-chef-6.png">

                    <div class="nicdark_bg_greydark_alpha_gradient nicdark_position_absolute nicdark_left_0 nicdark_height_100_percentage nicdark_width_100_percentage nicdark_padding_20 nicdark_box_sizing_border_box">
                        <div class="nicdark_position_absolute nicdark_bottom_20">
                            <h2 class="nicdark_color_white">@jane</h2>
                        </div>

                    </div>

                </div> --}}
                <!--image-->


                <div class="nicdark_section nicdark_box_sizing_border_box">
                
                    <div class="nicdark_section nicdark_bg_greydark">
                        <table class="nicdark_section ">
                            <tbody>
                               
                               <tr class="">
                                    <td class="nicdark_padding_30">  
                                        <h5 class="nicdark_font_size_13 nicdark_text_transform_uppercase nicdark_color_grey"> Name</h5>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <p class="nicdark_color_white nicdark_line_height_16">{{$user->full_name}}</p>
                                    </td>
                                </tr>
                                <tr class="">
                                    {{-- <td class="nicdark_padding_30">  
                                        <h5 class="nicdark_font_size_13 nicdark_text_transform_uppercase nicdark_color_grey"> Name</h5>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <p class="nicdark_color_white nicdark_line_height_16">{{$user->full_name}}</p>
                                    </td> --}}

                                    <td class="nicdark_padding_left_30 nicdark_padding_right_30">
                                        <h5 class="nicdark_font_size_13 nicdark_text_transform_uppercase nicdark_color_grey">Email</h5>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <p class="nicdark_color_white nicdark_line_height_16">{{$user->email}}</p>    
                                        <div class="nicdark_section nicdark_height_30"></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table> 
                    </div>

                    <div class="nicdark_section nicdark_border_1_solid_grey nicdark_padding_20 nicdark_box_sizing_border_box">

                        <table class="nicdark_section">
                            <tbody>
                               
                               <tr class="">
                                    <td class="nicdark_padding_10">  

                                        <div class="nicdark_display_table nicdark_float_left">
                
                                            <div class="nicdark_display_table_cell nicdark_vertical_align_middle">
                                                <img alt="" class="nicdark_margin_right_20" width="35" src="{{asset('img/education_theme/icons/icon-graduation-color.svg')}}">
                                            </div>

                                            <div class="nicdark_display_table_cell nicdark_vertical_align_middle">
                                                <h5 class="nicdark_font_size_13 nicdark_text_transform_uppercase"><strong>School</strong></h5>
                                                <div class="nicdark_section nicdark_height_5"></div>
                                                <p class="">{{ucwords($user->school)}}</p>
                                            </div>

                                        </div>

                                    </td>
                                </tr>
                                {{-- <tr class="">
                                    <td class="nicdark_padding_10">  

                                        <div class="nicdark_display_table nicdark_float_left">
                
                                            <div class="nicdark_display_table_cell nicdark_vertical_align_middle">
                                                <img alt="" class="nicdark_margin_right_20" width="25" src="img/education_theme/icons/icon-link-grey.svg">
                                            </div>

                                            <div class="nicdark_display_table_cell nicdark_vertical_align_middle">
                                                <h5 class="nicdark_font_size_13 nicdark_text_transform_uppercase"><strong>Url</strong></h5>
                                                <div class="nicdark_section nicdark_height_5"></div>
                                                <p class="">www.educationlab.net</p>
                                            </div>

                                        </div>

                                    </td>
                                </tr> --}}

                                {{-- <tr class="">
                                    <td class="nicdark_padding_10">  
                                        <h5 class="nicdark_font_size_13 nicdark_text_transform_uppercase"><strong>About Me</strong></h5>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <p class="nicdark_line_height_25">Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Sed quis rutrum tellus, sit amet viverra felis. Cras sagittis sem sit amet urna.</p>
                                    </td>
                                </tr> --}}

                            </tbody>
                        </table> 
                    </div>
                </div>

                {{--Edit / Logout Buttons--}}
                <div class="nicdark_section nicdark_padding_10 nicdark_box_sizing_border_box nicdark_bg_white ">
                    
                    <div class="nicdark_width_50_percentage nicdark_padding_10 nicdark_box_sizing_border_box nicdark_float_left nicdark_text_align_center">
                        <a class="nicdark_display_inline_block nicdark_color_white nicdark_bg_green nicdark_box_sizing_border_box nicdark_width_100_percentage nicdark_first_font nicdark_padding_8 nicdark_border_radius_3 nicdark_font_size_13" href="{{route('edit.account', $user->ref_id)}}" style="color:white">EDIT PROFILE</a>
                    </div>  

                    <div class="nicdark_width_50_percentage nicdark_padding_10 nicdark_box_sizing_border_box nicdark_float_left nicdark_text_align_center">
                        <a class="nicdark_display_inline_block nicdark_color_white nicdark_bg_red nicdark_box_sizing_border_box nicdark_width_100_percentage nicdark_first_font nicdark_padding_8 nicdark_border_radius_3 nicdark_font_size_13" href="{{route('logout')}}"
                          onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();" style="color:white">LOGOUT</a>
                    </div> 
                </div>                
            </div>
            <!--end preview-->
        </div>

    </div>


    <div class="grid grid_9">

        @if (!$user->hasRole('candidate') || ($user->hasRole('candidate') && $user->subscriptions->count()) || Cart::count())
            <div class="nicdark_section">
                <h3>Subscriptions & Practice Exams</h3>
                @if (Cart::count())
                    <h4>
                        <b>
                        You Have {{Cart::count()}} Item(s) In Your Cart.  <a href="{{route('review.subscriptions')}}"> <img alt="" width="35" src="{{asset('img/education_theme/icons/icon-cart-grey.svg')}}"> View Cart</a></b>
                    </h4>
                @endif
                <!--START tab-->
                <div class="nicdark_tabs">
                    <ul class="nicdark_list_style_none nicdark_margin_0 nicdark_padding_0 nicdark_section nicdark_border_bottom_2_solid_grey">
                        <li class="nicdark_display_inline_block">
                            <h4>
                                <a class="nicdark_outline_0 nicdark_padding_20 nicdark_padding_right_10 nicdark_display_inline_block nicdark_first_font nicdark_color_greydark" href="#tabs-1">Subscriptions</a> 
                                <a class="nicdark_display_inline_block nicdark_bg_grey nicdark_margin_right_20 nicdark_border_1_solid_grey nicdark_first_font nicdark_padding_8 nicdark_border_radius_3 nicdark_font_size_13" href="#">{{$user->subscriptions->count()}}</a>
                            </h4>
                        </li>
                        <li class="nicdark_display_inline_block">
                            <h4>
                                <a class="nicdark_outline_0 nicdark_padding_20 nicdark_padding_right_10 nicdark_display_inline_block nicdark_first_font nicdark_color_greydark" href="#tabs-2">Exams</a> 
                                <a class="nicdark_display_inline_block nicdark_bg_grey nicdark_margin_right_20 nicdark_border_1_solid_grey nicdark_first_font nicdark_padding_8 nicdark_border_radius_3 nicdark_font_size_13" href="#">{{$user->assessments->where('submitted', 1)->count()}}</a>
                            </h4>
                        </li>
                        <li class="nicdark_display_inline_block">
                            <h4>
                                <a class="nicdark_outline_0 nicdark_padding_20 nicdark_padding_right_10 nicdark_display_inline_block nicdark_first_font nicdark_color_greydark" href="#tabs-3">Incomplete</a> 
                                <a class="nicdark_display_inline_block nicdark_bg_grey nicdark_margin_right_20 nicdark_border_1_solid_grey nicdark_first_font nicdark_padding_8 nicdark_border_radius_3 nicdark_font_size_13" href="#">{{$user->assessments->where('submitted', 0)->count()}}</a>
                            </h4>
                        </li>
                        @if ($user->isStaff() || $user->isAdmin())
                            <li class="nicdark_display_inline_block">
                                <h4>
                                    <a class="nicdark_outline_0 nicdark_padding_20 nicdark_padding_right_10 nicdark_display_inline_block nicdark_first_font nicdark_color_greydark" href="#tabs-4">Upload Exams</a> 
                                    <a class="nicdark_display_inline_block nicdark_bg_grey nicdark_margin_right_20 nicdark_border_1_solid_grey nicdark_first_font nicdark_padding_8 nicdark_border_radius_3 nicdark_font_size_13" href="#"></a>
                                </h4>
                            </li>
                        @endif
                    </ul>

                    <div class="nicdark_section nicdark_height_20"></div>

                    <div class="nicdark_section" id="tabs-1">


                        <div class="nicdark_section nicdark_overflow_hidden nicdark_overflow_x_auto nicdark_cursor_move_responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Course Package</th>
                                        <th>Subscription</th>
                                        <th>Start Date</th>
                                        <th>Payment Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($user->subscriptions as $subscription)
                                        <tr>
                                            <td>
                                               <b>{{$subscription->package()->title}}</b>
                                               <br><small>{{$subscription->package()->active_topics()->count()}} Topics, {{$subscription->package()->questions()->count()}} Questions</small>
                                                @if($subscription->active())
                                                    <br>
                                                    <a href="{{route('view.topics', $subscription->package()->ref_id)}}" {{-- target="_blank" --}}>Start Exam</a>
                                                @endif
                                            </td>
                                            <td>
                                                <b>{{$subscription->name}}</b> ({{$subscription->duration}} Weeks)<br>
                                                <small>
                                                    {{$subscription->description}}
                                                </small>
                                            </td>
                                            <td>
                                                <b>Start Date: </b>{{Carbon\Carbon::parse($subscription->pivot->start_date)->format('m/d/Y')}} <br>
                                                <b>End Date: </b> {{Carbon\Carbon::parse($subscription->pivot->end_date)->format('m/d/Y')}}<br>
                                                @if ($subscription->pivot->end_date > Carbon\Carbon::now())
                                                    <small>{{Carbon\Carbon::parse($subscription->pivot->end_date)->diffForHumans()}}</small>
                                                @endif
                                                
                                            </td>
                                            <td>
                                                @if ($subscription->payment())
                                                    Amount: ${{$subscription->pivot->price}} <br>
                                                    ID: {{$subscription->payment()->ref_id}}
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5"><em>No Subscriptions.</em> <b><a href="{{route('purchase.select.packages')}}">Find Exams</a></b></td>
                                        </tr>
                                    @endforelse
                                    
                                </tbody>
                            </table>
                            @if ($user->subscriptions->count())
                                <b>Need Exams For Another Course?  <a href="{{route('purchase.select.packages')}}">Click Here</a></b>
                            @endif
                        </div>
            

                    </div>
                    <div class="nicdark_section" id="tabs-2">
                        <div class="nicdark_section nicdark_overflow_hidden nicdark_overflow_x_auto nicdark_cursor_move_responsive">
                            <b>Submitted & Graded Exams</b><br>
                            Your performance for each topic is shown.<br><br>

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Exam</th>
                                        <th>Topics Covered</th>
                                        <th>Results</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($user->assessments->where('submitted', 1)->sortByDesc('id') as $assessment)
                                        <tr>
                                            <td>
                                                <b>{{$assessment->package->title}}</b> <br>
                                                <small>{{$assessment->created_at->diffForHumans()}}</small>
                                            </td>
                                            <td>
                                                @foreach ($assessment->topics_covered() as $topic_id => $topic)
                                                    {{$topic}} <b>{{$assessment->submitted ? "{$assessment->topic_performance($topic_id)}%" : ""}}</b>
                                                    (<small>{{$assessment->topic_questions($topic_id)->count()}} Questions)
                                                    
                                                    </small>
                                                    <br><br>
                                                @endforeach                                       
                                            </td>
                                            <td>
                                              
                                                <b>{{$assessment->grade()}}% </b><br>
                                                    ({{$assessment->no_correct()}} / {{$assessment->questions->count()}})
                                                @if ($user->active_subscriptions()->pluck('package_id')->contains($assessment->package->id))
                                                    <br>
                                                    <a href="{{route('view.results', $assessment->ref_id)}}">View Questions</a>    
                                                @endif
                                                
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No Recent Exams</td>
                                        </tr>
                                    @endforelse
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="nicdark_section" id="tabs-3">

                        <div class="nicdark_section nicdark_box_sizing_border_box nicdark_overflow_hidden nicdark_overflow_x_auto nicdark_cursor_move_responsive">
                        <b>Saved / Incomplete Exams</b>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Exam</th>
                                        <th>Topics Covered</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($user->assessments->where('submitted', 0)->sortByDesc('id') as $assessment)
                                        <tr>
                                            <td>
                                                <b>{{$assessment->package->title}}</b> <br>
                                                <small>{{$assessment->created_at->diffForHumans()}}</small>
                                            </td>
                                            <td>
                                                @foreach ($assessment->topics_covered() as $topic_id => $topic)
                                                    {{$topic}}<br>
                                                @endforeach                                       
                                            </td>
                                            <td>
                                            
                                                Questions Completed: {{$assessment->total_answered()}}/ {{$assessment->questions->count()}}
                                                
                                                @if ($user->active_subscriptions()->pluck('package_id')->contains($assessment->package->id))
                                                    <br>
                                                    <a href="{{route('resume.assessment', $assessment->ref_id)}}" {{-- target="_blank" --}}>Resume Exam</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No Recent Assessment</td>
                                        </tr>
                                    @endforelse
                                    
                                </tbody>
                            </table>

                        </div>

                    </div>

                    @if ($user->isStaff() || $user->isAdmin())
                        <div class="nicdark_section" id="tabs-4">

                            <div class="nicdark_section nicdark_box_sizing_border_box nicdark_overflow_hidden nicdark_overflow_x_auto nicdark_cursor_move_responsive">

                                <a href="{{route('exam.upload.progress')}}">Create An Exam</a>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Course</th>
                                            <th>Course Number</th>
                                            <th>Exams</th>                          
                                            <th>Completed</th>
                                            <th>Reviewed</th>
                                            <th># Of Questions Submitted</th>
                                            <th># Questions Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>                 
                                        @foreach ($user->exams_uploaded() as $exam)
                                            <tr>
                                                <td>{{$exam->course->course_name}}</td>
                                                <td>{{$exam->course->course_number}}</td>
                                                <td>
                                                    @if (!$user->isAdmin() && $exam->reviewed)
                                                        {{ucfirst($exam->semester->semester)}} {{$exam->year}} Exam #{{$exam->exam_number}}
                                                    @else
                                                        <a href="{{route('load-exam', $exam->id)}}">{{ucfirst($exam->semester->semester)}} {{$exam->year}} Exam #{{$exam->exam_number}} </a>
                                                    @endif
                                                    
                                                </td>
                                                <td>{!!$exam->completed ? 'Yes' : "<span style='color:darkred'>No</span>"!!}</td>
                                                <td>{!!$exam->reviewed ? "<span style='color:darkgreen'>Yes</span>" : "<span style='color:darkred'>No</span>"!!}</td>

                                                <td>{{$exam->questions->where('submitted_by', $user->id)->count()}}</td>
                                                <td>{{$exam->questions->count()}}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><b>{{$user->questions->count()}} Questions Submitted</b></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    @endif
                    

                </div>
                <!--END tab-->


            </div>
            <hr>
        @endif


        <?php 
            $user_candidates = $user->tutor_candidates;
            $likely_candidates = App\TutorCandidate::whereNull('user_id')->where('email', 'like', $user->email)->get();
            foreach ($likely_candidates as $likely_candidate) {
                $user_candidates->push($likely_candidate);
            }
        ?>
        <div class="nicdark_section">          
            @if ($user->hasRole('candidate') || $user_candidates->count() > 0 )
                <div class="grid grid_12">
                    <h3>Requested Proficiency Exams</h3>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Requested By</th>
                                <th>Exam</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($user_candidates as $tutor_candidate)
                                <tr>
                                    <td>{{$tutor_candidate->tutor_test->user->first_name}}</td>
                                    <td>
                                        <b>{{$tutor_candidate->tutor_test->proficiency_exam->name}}</b>
                                        <br>
                                        <small>
                                            {{$tutor_candidate->tutor_test->questions->count()}} Questions <br>
                                            <b>Passing Grade:</b> {{$tutor_candidate->grade_cutoff}}% <br>
                                            <b>Deadline:</b> {{$tutor_candidate->deadline->format('m/d/Y')}}
                                        </small>
                                    </td>
                                    <td>
                                        <center>
                                            @if ($tutor_candidate->submitted)
                                                
                                                <b>Your Grade: </b>{{$tutor_candidate->grade()}}%<br>
                                                <b>Submitted:</b> {{$tutor_candidate->submission_date->format('m/d/Y')}}
                                            @else
                                               @if ($tutor_candidate->deadline->isFuture())
                                                    <a href="{{route('take.tutor.test', [$tutor_candidate->tutor_test->ref_id, $tutor_candidate->ref_id])}}" class="btn btn-sm btn-info pull-right">Take Test</a>
                                                @else
                                                    <button type="button" class="btn btn-sm btn-info pull-right" disabled>Take Test</button>
                                                @endif
                                            @endif
                                        </center>
                                    </td>
                                </tr>    
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
            
            <hr>

            @if (!$user->hasRole('candidate') || ($user->hasRole('candidate') && $user->subscriptions->count()))
                <div class="grid grid_12">
                    <h3>Tutor Proficiency Exams</h3><a name="tutor_proficiency_exams"></a>
                    <b>Is your tutor any good?</b>  Create, e-mail and review tutor proficiency exams. <a href="{{route('proficiency.exams.info')}}">Learn More.</a>
                </div>
                
                <div class="grid grid_3">
                    <h4><b style="color:darkgreen"><u>Step 1: Create Exam</u></b></h4>
                    <br>
                    @include('btn.proficiency_exams.partials.create_proficiency_exam')
                </div>
                <div class="grid grid_1">
                </div>
                <div class="grid grid_8">
                    <h4><b style="color:darkred"><u>Step 2: Send Exams</u></b></h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Exam</th>
                                <th>Created</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($user->tutor_tests as $tutor_test)
                                <tr>
                                    <td>
                                        <b>{{$tutor_test->proficiency_exam->name}}</b> <br>
                                        <small><a href="{{route('view.tutor.test', $tutor_test->ref_id)}}">{{$tutor_test->questions->count()}} Questions 
                                            @if ($tutor_test->view_solutions())
                                                (View Solutions) 
                                            @endif
                                            </a>
                                        </small>
                                    </td>
                                    <td>
                                        {{$tutor_test->created_at->diffForHumans()}}             
                                    </td>
                                    <td><a href="{{route('select.tutors', $tutor_test->ref_id)}}"><img src="{{asset('img/education_theme/icons/icon-email-2-grey.svg')}}" style="height:20px"> Send To Tutors</a></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">No Recent Exams</td>
                                </tr>
                            @endforelse
                            
                        </tbody>
                    </table>

                    <br>
                    <h4><b style="color:darkblue"><u>Step 3: Review Tutor Results</u></b></h4><a name="tutor_proficiency_results"></a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Exam</th>
                                <th>Tutor</th>
                                <th>Tutor's Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($user->tutor_tests as $tutor_test)
                                @foreach ($tutor_test->tutor_candidates as $tutor_candidate)
                                    <tr>
                                        <td>{{$tutor_test->proficiency_exam->name}}</td>
                                        <td>{{$tutor_candidate->full_name}}
                                        <br>
                                        <small>
                                            {{$tutor_candidate->email}}<br>
                                            <b>Deadline:</b> {{$tutor_candidate->deadline->format('m/d/Y h:i A')}}
                                        </small>
                                        
                                        </td>
                                        <td>
                                            <center>
                                            @if ($tutor_candidate->submitted)
                                                <span style="color: @if ($tutor_candidate->grade() < $tutor_candidate->grade_cutoff)
                                                        darkred
                                                    @else
                                                        darkgreen
                                                    @endif"><b>
                                                        {{$tutor_candidate->grade()}}%
                                                    </b>
                                                </span>
                                                <br>
                                                <small><a href="{{route('view.tutor.results', ['tutor_test_ref_id' => $tutor_test->ref_id, 'tutor_candidate_ref_id' => $tutor_candidate->ref_id])}}">Click For Details</a></small>
                                                <br>
                                            @else
                                                --
                                                <Br>
                                                <small>
                                                    @if ($tutor_candidate->paid())
                                                        Tutor Started Exam
                                                        <br>
                                                    @else
                                                        Tutor Hasn't Started Exam
                                        {!! Form::open(['method' => 'POST', 'route' => ['resend.tutor.test', 'tutor_test_ref_id' => $tutor_test->ref_id, 'tutor_candidate_ref_id' => $tutor_candidate->ref_id], 'id' => "cand-$tutor_candidate->ref_id"]) !!}
                                            
                                            {!! Form::button("Resend Exam To $tutor_candidate->first_name", ['class' => 'btn btn-xs btn-info', 'onclick' => 'form.submit()']) !!}
                                            
                                        {!! Form::close() !!}
                                                    @endif    
                                                </small>
                                                
                                            @endif
                                                <small>

                                                <b>Passing Grade:</b> {{$tutor_candidate->grade_cutoff}}%
                                                </small>
                                            </center>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            
                            
                        </tbody>
                    </table>
                </div>
                
                
            @endif
        </div>
    </div>
</div>

@endsection
