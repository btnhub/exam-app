<?php

use Illuminate\Database\Seeder;

class SubscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscriptions')->truncate();

        $faker = Faker\Factory::create();

        DB::table('subscriptions')->insert(
	            [
			        'ref_id' => strtoupper(str_random(12)),
			        'name' => "Semester",
			        'description' => "Semester long access to exams",
			        'duration' => 16,
			        'price' => 75,
			        'sale_price' => 45,
			        'active' => 1,
			        'on_sale' => 0,
			        'created_at' => $faker->dateTimeThisMonth,
			        'updated_at' => $faker->dateTimeThisMonth,
	            ]);

        DB::table('subscriptions')->insert(
	            [
			        'ref_id' => strtoupper(str_random(12)),
			        'name' => "Quarter",
			        'description' => "Quarter Term long access to exams",
			        'duration' => 12,
			        'price' => 60,
			        'sale_price' => 40,
			        'active' => 1,
			        'on_sale' => 0,
			        'created_at' => $faker->dateTimeThisMonth,
			        'updated_at' => $faker->dateTimeThisMonth,
	            ]);

        DB::table('subscriptions')->insert(
	            [
			        'ref_id' => strtoupper(str_random(12)),
			        'name' => "Month",
			        'description' => "Month long access to exams",
			        'duration' => 4,
			        'price' => 40,
			        'sale_price' => 30,
			        'active' => 1,
			        'on_sale' => 0,
			        'created_at' => $faker->dateTimeThisMonth,
			        'updated_at' => $faker->dateTimeThisMonth,
	            ]);

        DB::table('subscriptions')->insert(
	            [
			        'ref_id' => strtoupper(str_random(12)),
			        'name' => "Crunch Time",
			        'description' => "2 Week access to exams",
			        'duration' => 2,
			        'price' => 30,
			        'sale_price' => 25,
			        'active' => 1,
			        'on_sale' => 0,
			        'created_at' => $faker->dateTimeThisMonth,
			        'updated_at' => $faker->dateTimeThisMonth,
	            ]);

    }
}
