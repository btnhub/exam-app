<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseProficiencyExamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_proficiency_exam', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')
                ->foreign('course_id')
                ->references('id')
                ->on('courses')
                ->onDelete('cascade');
            $table->integer('proficiency_exam_id')
                ->foreign('proficiency_exam_id')
                ->references('id')
                ->on('proficiency_exams')
                ->onDelete('cascade');
            $table->smallInteger('specific_exam_number')
                  ->unsigned()
                  ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_proficiency_exam');
    }
}
