<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorTestPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_test_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->unique();
            $table->integer('tutor_candidate_id')
                    ->foreign('tutor_candidate_id')
                    ->references('id')
                    ->on('tutor_candidates')
                    ->onDelete('cascade');
            $table->string('stripe_customer_id')->nullable();
            $table->string('transaction_id')->unique();
            $table->decimal('amount', 6, 2)->unsigned();
            $table->boolean('captured')->default(0);
            $table->boolean('paid')->default(0);
            $table->timestamp('completed_at')->nullable();
            $table->boolean('refunded')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_test_payments');
    }
}
