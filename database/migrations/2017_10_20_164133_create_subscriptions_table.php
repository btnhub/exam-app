<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->nullable()->unique();
            $table->string('name');
            $table->string('description')->nullable();
            $table->smallInteger('duration')->unsigned()->comment('weeks');
            $table->decimal('price', 10,2)->unsigned()->nullable();
            $table->decimal('sale_price', 10,2)->unsigned()->nullable();
            $table->boolean('active')->default(1);
            $table->boolean('on_sale')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
