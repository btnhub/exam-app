<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->unique();
            $table->integer('user_id')
                    ->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade')
                    ->nullable();
            $table->string('stripe_customer_id')->nullable();
            $table->string('transaction_id')->unique();
            $table->text('details')->nullable();
            $table->decimal('amount', 10, 2)->unsigned();
            $table->decimal('discount_amount', 10,2)->unsigned()->nullable();
            $table->decimal('credit_applied', 10,2)->unsigned()->nullable();
            $table->decimal('total_amount', 10, 2)->unsigned();
            $table->string('coupon_id')->nullable();
            $table->boolean('captured')->default(0);
            $table->boolean('paid')->default(0);
            $table->timestamp('completed_at')->nullable();
            $table->boolean('refunded')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
