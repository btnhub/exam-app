<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProficiencyExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proficiency_exams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->nullable()->unique();
            $table->string('name');
            $table->string('department')->nullable();
            $table->boolean('available')->default(0)->nullable();
            $table->decimal('discount')->default(0)->nullable();
            $table->string('description')->nullable();
            $table->text('detailed_description')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proficiency_exams');
    }
}
