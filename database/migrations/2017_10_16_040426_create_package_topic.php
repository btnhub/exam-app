<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageTopic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_topic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id')
                ->foreign('package_id')
                ->references('id')
                ->on('packages')
                ->onDelete('cascade');
            $table->integer('topic_id')
                ->foreign('topic_id')
                ->references('id')
                ->on('topics')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_topic');
    }
}
