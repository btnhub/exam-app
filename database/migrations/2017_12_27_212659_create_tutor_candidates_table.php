<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->comment('tutor')
                ->nullable();
            $table->integer('tutor_test_id')
                ->foreign('tutor_test_id')
                ->references('id')
                ->on('tutor_tests')
                ->onDelete('cascade');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('email');
            $table->dateTime('deadline')->nullable();
            $table->tinyInteger('grade_cutoff')->nullable();
            $table->boolean('submitted')->default(0);
            $table->dateTime('submission_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_candidates');
    }
}
