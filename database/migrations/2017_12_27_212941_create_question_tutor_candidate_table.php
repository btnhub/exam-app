<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTutorCandidateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_tutor_candidate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutor_candidate_id')->unsigned();
            $table->integer('question_id')->unsigned();
            $table->integer('selected_answer_id')->unsigned()->nullable();
            $table->integer('question_time')->unsigned()->comment('in seconds')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_tutor_candidate');
    }
}
