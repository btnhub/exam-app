<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTutorTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_tutor_test', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutor_test_id')
                ->foreign('tutor_test_id')
                ->references('id')
                ->on('tutor_tests')
                ->onDelete('cascade');
            $table->integer('question_id')
                ->foreign('question_id')
                ->references('id')
                ->on('questions')
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_tutor_test');
    }
}
