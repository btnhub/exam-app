<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentQuestionPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_question', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assessment_id')
                ->foreign('assessment_id')
                ->references('id')
                ->on('assessments')
                ->onDelete('cascade');
            $table->integer('question_id')
                ->foreign('question_id')
                ->references('id')
                ->on('questions')
                ->onDelete('cascade');
            $table->integer('selected_answer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_question');
    }
}
