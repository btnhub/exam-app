<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('layouts.education_theme.homepage');
});

Route::get('/emails', function () {
	$tutor_test = App\TutorTest::first();
	$tutor_candidate = App\TutorCandidate::first();
   return view('btn.emails.notify_tutor_candidates', compact('tutor_test', 'tutor_candidate'));
});

Auth::routes();

//DO NOT CHANGE ROUTE URL NAME /home
Route::get('/home', 'UsersController@myAccount')
		->name('home')
		->middleware('auth');

Route::get('/cart', [
		'as' => 'cart.index',
		'uses' => 'CartController@index'
	]);

Route::get('/subscriptions/select-packages', [
		'as' => 'purchase.select.packages',
		'uses' => 'SubscriptionsController@selectPackages'
	]);

Route::post('/subscriptions/select-packages/{package_ref_id}', [
	'as' => 'add.to.cart',
	'uses' => 'CartController@addToCart'
]);

Route::delete('/cart/empty', [
	'as' =>'empty.cart', 
	'uses' => 'CartController@emptyCart',
]);

Route::delete('/cart/remove-item/{row_id}', [
	'as' =>'remove.item', 
	'uses' => 'CartController@removeItem',
]);

Route::middleware(['auth', 'role:admin'])->group(function () {
	
	Route::get('/users', [
		'as' => 'users.index',
		'uses' => 'UsersController@index'
	]);

	Route::get('/clone-users/{user_id}', [
		'as' => 'clone.user',
		'uses' => 'UsersController@cloneUser'
	]);

	Route::get('/select-exam', [
		'as' => 'select-exam',
		'uses' => 'ExamsController@selectExam'
	]);

	Route::get('/courses/{course_id}/add-topics', [
		'as' => 'add.course.topics',
		'uses' => 'CoursesController@addCourseTopics'
	]);

	Route::post('/courses/{course_id}/add-topics', [
		'as' => 'save.course.topics',
		'uses' => 'CoursesController@saveCourseTopics'
	]);

	Route::get('/take-exam/{exam_id}', [
		'as' => 'take-exam',
		'uses' => 'ExamsController@takeExam'
	]);

	Route::post('/grade-exam/{exam_id}', [
		'as' => 'grade-exam',
		'uses' => 'ExamsController@gradeExam'
	]);

	Route::get('/view-questions/{course_id}', [
		'as' => 'view.questions',
		'uses' => 'QuestionsController@viewQuestions'
	]);

	Route::post('/add-topics/{course_id}', [
		'as' => 'add.topics',
		'uses' => 'QuestionsController@addTopics'
	]);


	Route::get('/update-exam/reviewed-completed/{exam_id}/{option}/{value}', [
		'as' => 'update.completed.reviewed',
		'uses' => 'ExamsController@updateCompletedOrReviewed'
	]);

	Route::get('/subscriptions', [
		'as' => 'subscriptions.index',
		'uses' => 'SubscriptionsController@index'
	]);

	Route::post('/subscriptions/{subscription_id}/create-free-subscription', [
		'as' => 'create.free.subscription',
		'uses' => 'SubscriptionsController@createFreeSubscription'
	]);	
});

Route::middleware(['auth', 'role:admin|staff'])->group(function () {
	Route::get('/staff/exam-upload-progress', [
		'as' => 'exam.upload.progress',
		'uses' => 'ExamsController@examUploadProgress'
	]);

	Route::get('/load-exam/{exam_id}', [
		'as' => 'load-exam',
		'uses' => 'ExamsController@loadExam'
	]);

	Route::post('/save-exam', [
		'as' => 'save.exam',
		'uses' => 'ExamsController@saveExam'
	]);

	Route::get('/show-question/{ref_id}', [
		'as' => 'show-question',
		'uses' => 'QuestionsController@showQuestion'
	]);

	Route::post('/update-question/{ref_id}', [
		'as' => 'update.question',
		'uses' => 'QuestionsController@updateQuestion'
	]);

	Route::get('/create-question/{exam_id}', [
		'as' => 'create.question',
		'uses' => 'QuestionsController@createQuestion'
	]);

	Route::post('/save-question/{exam_id}', [
		'as' => 'save.question',
		'uses' => 'QuestionsController@saveQuestion'
	]);
});


Route::middleware(['auth'])->group(function () {
	//To Purchase Subscription, an account must be made
	Route::get('/subscriptions/review', [
		'as' => 'review.subscriptions',
		'uses' => 'SubscriptionsController@reviewSubscriptions'
	]);	

	Route::post('/subscriptions/purchase', [
		'as' => 'purchase.subscriptions',
		'uses' => 'SubscriptionsController@purchaseSubscriptions'
	]);

	Route::get('/edit-account/{user_ref_id}', [
		'as' => 'edit.account',
		'uses' => 'UsersController@editAccount'
	]);

	Route::post('/edit-account/{user_ref_id}', [
		'as' => 'update.account',
		'uses' => 'UsersController@updateAccount'
	]);	
});

Route::middleware(['auth', 'role:admin|subscriber|free|mogi-student'])->group(function () {

	Route::get('/take-assessment/view-topics/{package_ref_id}', [
		'as' => 'view.topics',
		'uses' => 'AssessmentsController@viewTopics'
	]);

	Route::post('/take-assessment/{package_ref_id}', [
		'as' => 'take.assessment',
		'uses' => 'AssessmentsController@takeAssessment'
	]);

	Route::get('/take-assessment/res/{assessment_ref_id}', [
		'as' => 'resume.assessment',
		'uses' => 'AssessmentsController@resumeAssessment'
	]);

	Route::post('/pause-assessment/{package_ref_id}', [
		'as' => 'pause.assessment',
		'uses' => 'AssessmentsController@pauseAssessment'
	]);

	Route::post('/grade-assessment/{package_ref_id}', [
		'as' => 'grade.assessment',
		'uses' => 'AssessmentsController@gradeAssessment'
	]);

	Route::get('/view-results/{assessment_ref_id}', [
		'as' => 'view.results',
		'uses' => 'AssessmentsController@viewResults'
	]);

	Route::post('/report-issue/{question_ref_id}/{assessment_ref_id}', [
		'as' => 'report.issue',
		'uses' => 'AssessmentsController@reportIssue'
	]);

});


//Proficiency Exams
Route::group(['prefix' => 'proficiency-exams'], function(){
	Route::get('/', [
		'as' => 'proficiency.exams.info',
		'uses' => 'ProficiencyExamsController@proficiencyExamsInfo'
		]);

	Route::middleware(['auth', 'role:admin'])->group(function () {
		Route::get('/list', [
			'as' => 'proficiency.exams.list',
			'uses' => 'ProficiencyExamsController@index'
			]);
		
		Route::post('/store', [
			'as' => 'proficiency.store',
			'uses' => 'ProficiencyExamsController@store'
			]);

		Route::post('add-courses/{prof_exam_id}', [
			'as' => 'add.proficiency.courses',
			'uses' => 'ProficiencyExamsController@addProficiencyCourses'
			]);
	});

	Route::middleware(['auth'])->group(function () {
		Route::post('create-tutor-test', [
			'as' => 'create.tutor.test',
			'uses' => 'TutorTestsController@createTutorTest'
			]);

		Route::get('/view-test/{tutor_test_ref_id}', [
			'as' => 'view.tutor.test',
			'uses' => 'TutorTestsController@viewTutorTest'
			]);

		Route::get('/email-tutors/{tutor_test_ref_id}', [
			'as' => 'select.tutors',
			'uses' => 'TutorTestsController@selectTutors'
			]);

		Route::post('email-exams/{tutor_test_ref_id}', [
			'as' => 'email.tutor.test',
			'uses' => 'TutorTestsController@emailTutorTest'
			]);
		Route::post('resend-email/{tutor_test_ref_id}/{tutor_candidate_ref_id}', [
			'as' => 'resend.tutor.test',
			'uses' => 'TutorTestsController@resendTutorEmail'
			]);

		Route::get('/view-performance/{tutor_test_ref_id}/{tutor_candidate_ref_id}', [
			'as' => 'view.tutor.results',
			'uses' => 'TutorTestsController@viewTutorResults'
			]);

		Route::get('/tutors/take-test/{tutor_test_ref_id}/{tutor_candidate_ref_id}', [
			'as' => 'take.tutor.test',
			'uses' => 'TutorTestsController@takeTutorTest'
			]);
		
		Route::post('/tutors/subscription/{tutor_test_ref_id}/{tutor_candidate_ref_id}', [
			'as' => 'purchase.tutor.test',
			'uses' => 'TutorTestsController@purchaseTutorTest'
			]);

		Route::post('/tutors/grade-test/{tutor_test_ref_id}/{tutor_candidate_ref_id}', [
			'as' => 'grade.tutor.test',
			'uses' => 'TutorTestsController@gradeTutorTest'
			]);
	});
});
